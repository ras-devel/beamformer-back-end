########!/opt/local/bin/python2.7

#print('Im Printing!!!')

import corr, time, struct, sys, logging, socket, math, operator
from numpy import genfromtxt
import numpy as np

boffile     = 'flag39_2017_Feb_23_1721.bof'#Luke replace boffile name
paf0        = 10*(2**24)+17*(2**16)+16*(2**8)+39
flag3_0     = 10*(2**24)+17*(2**16)+16*(2**8)+208
flag3_1     = 10*(2**24)+17*(2**16)+16*(2**8)+209
flag3_2     = 10*(2**24)+17*(2**16)+16*(2**8)+210
flag3_3     = 10*(2**24)+17*(2**16)+16*(2**8)+211
#west        = 10*(2**24)+17*(2**16)+0*(2**8)+35 # west ip address , need to direct the unwanted packets somewhere   
#tofu        = 10*(2**24)+17*(2**16)+0*(2**8)+36 # ip address of tofu for the correct mac address
#south       = 10*(2**24)+17*(2**16)+0*(2**8)+33 
blackhole   = 10*(2**24)+17*(2**16)+16*(2**8)+200

source_port = 60000                             #the physical port on the roach board side  which it will be sending from
                    #luke why 60,000???
#From Ray's Dealer Player code
def _ip_string_to_int(ip):
    """_ip_string_to_int(ip)

    Takes an IP address in string representation and returns an integer
    representation::

      iip = _ip_string_to_int('10.17.0.51')
      print(hex(iip))
      0x0A110040

    """
    try:
        rval = sum(map(lambda x, y: x << y,
                       [int(p) for p in ip.split('.')], [24, 16, 8, 0]))
    except (TypeError, AttributeError):
        rval = None

    return rval

def _hostname_to_ip(hostname):
    """_hostname_to_ip(hostname)

    Takes a hostname string and returns an IP address string::

      ip = _hostname_to_ip('vegasr2-1')
      print(ip)
      10.17.0.64

    """
    try:
        rval = socket.gethostbyaddr(hostname)[2][0]
    except (TypeError, socket.gaierror, socket.herror):
        rval = None

    return rval

macs = {
    '10.17.16.39' : 0x000F5308458C, # paf0
    '10.17.16.208': 0x7CFE90B92DF0, # flag3_0 
    '10.17.16.209': 0x7CFE90B92BD0, # flag3_1 
    '10.17.16.210': 0x7CFE90B92BD1, # flag3_2
    '10.17.16.211': 0x7CFE90B92DF1, # flag3_3
#    '10.17.0.35': 0x000F530C668C, # west
#    '10.17.0.33': 0x0002C952FDCB, # south
#    '10.17.0.36': 0x000F530CFDB8, # tofu
    '10.17.16.200': 0x0202b1ac401e, # blackhole mac address and fake ip; Note the switch is configured to drop packets sent to this mac
}

hpc_macs = {}
hpc_macs_list = [0xffffffffffff] * 256

for hostname, mac in macs.iteritems():
    key = _ip_string_to_int(_hostname_to_ip(hostname)) & 0xFF
    hpc_macs[key] = mac
    hpc_macs_list[key] = mac


def treat_data_sbs(j,k):
    if(j==0):
            fur67 = open("usb_data_re01.txt","r")
            fui67 = open("usb_data_im01.txt","r")
            flr67 = open("lsb_data_re01.txt","r")
            fli67 = open("lsb_data_im01.txt","r")
    elif j==2:
            fur67 = open("usb_data_re23.txt","r")
            fui67 = open("usb_data_im23.txt","r")
            flr67 = open("lsb_data_re23.txt","r")
            fli67 = open("lsb_data_im23.txt","r")
    elif j==4:
            fur67 = open("usb_data_re45.txt","r")
            fui67 = open("usb_data_im45.txt","r")
            flr67 = open("lsb_data_re45.txt","r")
            fli67 = open("lsb_data_im45.txt","r")
    elif j==6:
            fur67 = open("usb_data_re67.txt","r")
            fui67 = open("usb_data_im67.txt","r")
            flr67 = open("lsb_data_re67.txt","r")
            fli67 = open("lsb_data_im67.txt","r")
    else:
        print 'wrong j value in treat_data_sbs \n'

    usb_r67 = np.array(fur67.read().split('\n'))
    #if(j==0):
    #    if(k==0):
            #print ('contents of usb_data_re01.txt\n')
            #print (usb_r67[0:255])
            #print ('check it \n')
    usb_i67 = np.array(fui67.read().split('\n'))
    lsb_r67 = np.array(flr67.read().split('\n'))
    lsb_i67 = np.array(fli67.read().split('\n'))

    i=0
    usb67_mag = usb_r67
    lsb67_mag = lsb_r67
    while i<513:
        usb_r67[i] = float(usb_r67[i])
        usb_i67[i] = float(usb_i67[i])
        usb67_mag[i]=float((float(usb_r67[i])*float(usb_r67[i])+float(usb_i67[i])*float(usb_i67[i])))
        lsb67_mag[i]=float((float(lsb_r67[i])*float(lsb_r67[i])+float(lsb_i67[i])*float(lsb_i67[i])))          
        #usb67_mag[i]=float(np.sqrt(float(usb_r67[i])*float(usb_r67[i])+float(usb_i67[i])*float(usb_i67[i])))
        #lsb67_mag[i]=float(np.sqrt(float(lsb_r67[i])*float(lsb_r67[i])+float(lsb_i67[i])*float(lsb_i67[i])))          
        i=i+1

    #usb6_mag=map(float,(usb67_mag[0:256]))
    #usb7_mag=map(float,(usb67_mag[256:512]))
    #lsb6_mag=map(float,(lsb67_mag[0:256]))
    #lsb7_mag=map(float,(lsb67_mag[256:512]))


    ############luke
    usb6_mag1=map(float,(usb67_mag[0:256]))
    usb7_mag1=map(float,(usb67_mag[256:512]))
    lsb6_mag1=map(float,(lsb67_mag[0:256]))
    lsb7_mag1=map(float,(lsb67_mag[256:512]))

    return usb6_mag1, lsb6_mag1, usb7_mag1, lsb7_mag1

def get_data_sbs(k,j):



    #for k in range(4):
        #lsb_raw_re = fpga.read('lsb_re_'+str(k)+str(k+1)+'_bram',2**12,0)
        #lsb_raw_re = fpga.read('lsb_re_01_1',2**12,0)
        if(k==6):
            lsb_raw_re = lsb_raw_re = fpga.read('lsb_re_'+str(k)+str(k+1)+'_2',2**12,0)
        else:
            lsb_raw_re = fpga.read('lsb_re_'+str(k)+str(k+1)+'_1',2**12,0)
        lsb_re = struct.unpack('>4096b',lsb_raw_re)
        #print(lsb_re)
        
        #lsb_raw_im = fpga.read('lsb_im_'+str(k)+str(k+1)+'_bram',2**12,0)
        #lsb_raw_im = fpga.read('lsb_im_01_1',2**12,0)
        if(k==6):
            lsb_raw_im = fpga.read('lsb_im_'+str(k)+str(k+1)+'_2',2**12,0)
        else:
            lsb_raw_im = fpga.read('lsb_im_'+str(k)+str(k+1)+'_1',2**12,0)
        lsb_im = struct.unpack('>4096b',lsb_raw_im)
        
        #usb_raw_re = fpga.read('usb_re_'+str(k)+str(k+1)+'_bram',2**12,0) 
        #usb_raw_re = fpga.read('usb_re_01_1',2**12,0)
        if(k==6):
            usb_raw_re = fpga.read('usb_re_'+str(k)+str(k+1)+'_2',2**12,0)
        else:
            usb_raw_re = fpga.read('usb_re_'+str(k)+str(k+1)+'_1',2**12,0)
        usb_re = struct.unpack('>4096b',usb_raw_re)
        
        #usb_raw_im = fpga.read('usb_im_'+str(k)+str(k+1)+'_bram',2**12,0)
        #usb_raw_im = fpga.read('usb_im_01_1',2**12,0)
        if(k==6):
            usb_raw_im = fpga.read('usb_im_'+str(k)+str(k+1)+'_2',2**12,0)
        else:
            usb_raw_im = fpga.read('usb_im_'+str(k)+str(k+1)+'_1',2**12,0)
        usb_im = struct.unpack('>4096b',usb_raw_im)
       
        usb_r = np.array(usb_re)#/128.0
        #if (j==0):
            #print usb_re
            #print (len(usb_r))
            #print ('first contents of usb_data_re01.txt\n')
            #print(j)
            #print (usb_re[0:255])
            #print ('check it \n')
        usb_i = np.array(usb_im)#/128.0
        lsb_r = np.array(lsb_re)#/128.0
        lsb_i = np.array(lsb_im)#/128.0
        
        m=0
        with open('lsb_data_re'+str(k)+str(k+1)+'.txt','w') as f:
           for m in range(len(lsb_r)):
               the_str = ""
               the_str += str(lsb_r[m]) + "\n"
               f.write(the_str)
        m=0
        with open('lsb_data_im'+str(k)+str(k+1)+'.txt','w') as f:
           for m in range(len(lsb_i)):
               the_str = ""
               the_str += str(lsb_i[m]) + "\n"
               f.write(the_str)

        m=0
        with open('usb_data_re'+str(k)+str(k+1)+'.txt','w') as f:
           for m in range(len(usb_r)):
               the_str = ""
               the_str += str(usb_r[m]) + "\n"
               f.write(the_str)
        m=0
        with open('usb_data_im'+str(k)+str(k+1)+'.txt','w') as f:
           for m in range(len(usb_i)):
               the_str = ""
               the_str += str(usb_i[m]) + "\n"
               f.write(the_str)
        
        return usb_r, usb_i, lsb_r, lsb_i

def get_data_iq(rcv_nr):
    data = fpga.read('final_data_'+str(rcv_nr),2**16*2,0)         #read data from specific receiver number
    d_0=struct.unpack('>65536H',data) #< for little endian B for (8 bits) now made H for 16 bits
    d_0i = []
    d_0q = []
    for n in range(65536):
        d_0i.append(d_0[n]>>8)   #get the top 8 bits of final_data_rcv_nr 
        d_0q.append(d_0[n]&0xff) #get the bottom 8 bits of final_data_rcv_nr

    m=0
    with open('data_' +str(rcv_nr) +'i.txt','w') as f:
       for m in range(len(d_0i)):
           the_str = ""
           the_str += str(d_0i[m]) + "\n"
           f.write(the_str)
    m=0
    with open('data_' +str(rcv_nr) +'q.txt','w') as f1:
       for m in range(len(d_0q)):
           the_str = ""
           the_str += str(d_0q[m]) + "\n"
           f1.write(the_str)

#this function should do a byte swap on the channel specified. 
#As a first implementation ch should be represented in hex
def swap_byte(fpga,ch):
    fpga.write_int('rxslide',ch)   
    fpga.write_int('rxslide',0x00)

    #fpga.write_int('rxslide',ch)   
    #time.sleep(0.5)
    #if (hex(fpga.read_int('msb2_loc')) == 0x80):
    #       fpga.write_int('rxslide',0x00)          
    #       return;
    #time.sleep(1)
    #fpga.write_int('rxslide',0x00)
    #time.sleep(1)
    #while(hex(fpga.read_int('msb2_loc')) != 0x80):
    #    fpga.write_int('rxslide',ch) 
    #    time.sleep(0.5)
    #    if (hex(fpga.read_int('msb2_loc')) == 0x80):
    #       fpga.write_int('rxslide',0x00)          
    #       break;
    #    time.sleep(1)
    #    fpga.write_int('rxslide',0x00)
    #    time.sleep(1)    

def exit_fail():
    print 'FAILURE DETECTED. Log entries:\n',lh.printMessages()
    try:
        fpga.stop()
    except: pass
    raise
    exit()



def exit_clean():
    try:
        for f in fpgas: f.stop()
    except: pass
    exit()


if __name__ == '__main__':
    from optparse import OptionParser

    p = OptionParser()
    p.set_usage('f_engine_config_lo.py <ROACH_HOSTNAME_or_IP> [options]')
    p.set_description(__doc__)
    p.add_option('-n'  , '--noprogram'         , dest='noprogram'       , action='store_true'                      , help='Don\'t reprogram the roach.')  
    p.add_option('-c'  , '--set_coefficients'  , dest='set_coef'        , type = 'int'       , default=0           , help='Set the sideband separating coefficients.')  
    p.add_option('-k'  , '--set_coeffs_ones'   , dest='set_coef_ones'   , type = 'int'       , default=0           , help='Set the SBS coeffs to 1.')
    p.add_option('-l'  , '--lo'                , dest='lo'              , type = 'str'       , default='1450'      , help='Set the LO for sbs coefficients, default is 1450 MHz')  
    p.add_option('-r'  , '--reset_transceivers', dest='reset_trans'     , type = 'int'       , default=0           , help='Reset data input transceivers.')  
    p.add_option('-i'  , '--fft_shift'         , dest='fft_shift'       , type = 'long'      , default=0xaaaaaaaa  , help='Set the fft_shift register, default is 0xaaaaaaaa')  
    p.add_option('-b'  , '--bit_lock'          , dest='auto_align_bits' , type = 'int'       , default=0           , help='Automatically bit align the system')  
    p.add_option('-B'  , '--byte_lock'         , dest='auto_align_bytes', type = 'int'       , default=0           , help='1=calc and align bytes, 2=check byte alignment')
    p.add_option('-e'  , '--byte_swap'         , dest='swap_bytes_ch'   , type = 'long'      , default=-1          , help='Swap bytes of specified channel')
    p.add_option('-a'  , '--set_arm'           , dest='arm'             , type = 'int'       , default=0           , help='ARM the subsystem')  
    p.add_option('-t'  , '--get_time_data'     , dest='time_data'       , type = 'int'       , default=0           , help='Get time domain i and q data')  
    p.add_option('-s'  , '--get_sbs_data'      , dest='sbs_data'        , type = 'int'       , default=0           , help='Get sideband separated frequency data')  
    p.add_option('-d'  , '--boffile'           , dest='bof'             , type = 'str'       , default=boffile     , help='Specify the bof file to load')  
    p.add_option('-p'  , '--packetizer'        , dest='pack'            , type = 'int'       , default=0           , help='Reset GbE cores, start GbE cores, Set f_id & x_id registers on firmware. Set destination port')  
    p.add_option('-x'  , '--xid'               , dest='xid'             , type = 'int'       , default=-1          , help='The x id number for the sideband to grab')  
    p.add_option('-f'  , '--fid'               , dest='fid'             , type = 'int'       , default=-1          , help='The f id number for the roach.')  
    p.add_option('-o'  , '--dport'             , dest='dport'           , type = 'int'       , default=-1          , help='The destination port number')  
    p.add_option('-C'  , '--dcomp'             , dest='dcomp'           , type = 'str'       , default='blackhole' , help='The destination computer (paf0, tofu)')  
    #Luke added
    #p.add_option('-q   , '--IQ_stage'          , dest='iq_stage'        , type = 'int'       , default =0          , help='select stage: 1=initial/USB, 2=check/LSB')
    
    opts, args = p.parse_args(sys.argv[1:])

    if args==[]:
       print 'Please specify a ROACH board. \nExiting.'
       exit()
    else:
       roach = args[0]

    if opts.bof != '':
       boffile = opts.bof

    if (opts.fid == -1):
       print 'Specify ROACH f_id.'
       exit()

    blade_roach = ['A', 'B', 'C', 'D', 'E']
    fid_prefix  = blade_roach[opts.fid]

    lsbre01 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre01_'+opts.lo+'.csv', delimiter = '/n')
    lsbre23 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre23_'+opts.lo+'.csv', delimiter = '/n')
    lsbre45 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre45_'+opts.lo+'.csv', delimiter = '/n')
    lsbre67 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre67_'+opts.lo+'.csv', delimiter = '/n')
                                                  
    lsbim01 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim01_'+opts.lo+'.csv', delimiter = '/n')
    lsbim23 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim23_'+opts.lo+'.csv', delimiter = '/n')
    lsbim45 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim45_'+opts.lo+'.csv', delimiter = '/n')
    lsbim67 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim67_'+opts.lo+'.csv', delimiter = '/n')
                                                  
    usbre01 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre01_'+opts.lo+'.csv', delimiter = '/n')
    usbre23 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre23_'+opts.lo+'.csv', delimiter = '/n')
    usbre45 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre45_'+opts.lo+'.csv', delimiter = '/n')
    usbre67 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre67_'+opts.lo+'.csv', delimiter = '/n')
                                                  
    usbim01 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim01_'+opts.lo+'.csv', delimiter = '/n')
    usbim23 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim23_'+opts.lo+'.csv', delimiter = '/n')
    usbim45 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim45_'+opts.lo+'.csv', delimiter = '/n')
    usbim67 = genfromtxt('../coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim67_'+opts.lo+'.csv', delimiter = '/n')

    #ones_coeffs = genfromtxt('../coefficients/paf_ones_coeffs.csv',delimiter ='/n')

    mac_base0   = (2<<40) + (2<<32) + (10<<24) + (17<<16) + (16<<8) + (4*opts.fid+230)               #give each physical port a unique id 
    mac_base1   = (2<<40) + (2<<32) + (10<<24) + (17<<16) + (16<<8) + (4*opts.fid+231) 
    mac_base2   = (2<<40) + (2<<32) + (10<<24) + (17<<16) + (16<<8) + (4*opts.fid+232) 
    mac_base3   = (2<<40) + (2<<32) + (10<<24) + (17<<16) + (16<<8) + (4*opts.fid+233) 
    
    source_ip0 = 10*(2**24)+17*(2**16)+16*(2**8)+230+(4*opts.fid) 
    source_ip1 = 10*(2**24)+17*(2**16)+16*(2**8)+231+(4*opts.fid) 
    source_ip2 = 10*(2**24)+17*(2**16)+16*(2**8)+232+(4*opts.fid) 
    source_ip3 = 10*(2**24)+17*(2**16)+16*(2**8)+233+(4*opts.fid) 

try:
    lh = corr.log_handlers.DebugLogHandler()
    logger = logging.getLogger(roach)
    logger.addHandler(lh)
    logger.setLevel(10)

    print('Connecting to server %s... '%(roach)),
    fpga = corr.katcp_wrapper.FpgaClient(roach, logger=logger)
    time.sleep(1)

    if fpga.is_connected():
       print 'ok\n'
    else:
       print 'ERROR connecting to server %s.\n'%(roach)
       exit_fail()
    
    if not opts.noprogram:
       print '------------------------'
       print 'Programming ROACH: %s'%(roach) 
       sys.stdout.flush()
       fpga.progdev(boffile)
       time.sleep(1)
       print 'done. \n'
       
    if (opts.reset_trans):
       print 'Resetting the transceivers on: %s.\n'%(roach)
       fpga.write_int('gtxrxreset_in', 0xff)
       fpga.write_int('gtxrxreset_in', 0x00)
       print 'done \n'

    if (opts.set_coef):
       print 'Setting coefficients on: %s.\n'%(roach)
       # 000000 111111 #      
       lsbre01_to_pack = np.round(lsbre01*2**29)
       lsbre01_packed  = struct.pack('>512l', *lsbre01_to_pack)   #the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
       fpga.write('lsbre01', lsbre01_packed, 0)          
       
       lsbim01_to_pack = np.round(lsbim01*2**29)
       lsbim01_packed  = struct.pack('>512l', *lsbim01_to_pack)   
       fpga.write('lsbim01', lsbim01_packed, 0) 
  
       usbre01_to_pack = np.round(usbre01*2**29)    
       usbre01_packed  = struct.pack('>512l', *usbre01_to_pack)
       fpga.write('usbre01', usbre01_packed, 0)

       usbim01_to_pack = np.round(usbim01*2**29)
       usbim01_packed  = struct.pack('>512l', *usbim01_to_pack)
       fpga.write('usbim01', usbim01_packed, 0)

       # 222222 333333 #
       lsbre23_to_pack = np.round(lsbre23*2**29)
       lsbre23_packed  = struct.pack('>512l', *lsbre23_to_pack)
       fpga.write('lsbre23', lsbre23_packed, 0)

       lsbim23_to_pack = np.round(lsbim23*2**29)
       lsbim23_packed  = struct.pack('>512l', *lsbim23_to_pack)
       fpga.write('lsbim23', lsbim23_packed, 0)

       usbre23_to_pack = np.round(usbre23*2**29)
       usbre23_packed  = struct.pack('>512l', *usbre23_to_pack)
       fpga.write('usbre23', usbre23_packed, 0)

       usbim23_to_pack = np.round(usbim23*2**29)
       usbim23_packed  = struct.pack('>512l', *usbim23_to_pack)
       fpga.write('usbim23', usbim23_packed, 0)

       # 444444 555555 #
       lsbre45_to_pack = np.round(lsbre45*2**29)
       lsbre45_packed  = struct.pack('>512l', *lsbre45_to_pack)
       fpga.write('lsbre45', lsbre45_packed, 0)

       lsbim45_to_pack = np.round(lsbim45*2**29)
       lsbim45_packed  = struct.pack('>512l',*lsbim45_to_pack)
       fpga.write('lsbim45', lsbim45_packed, 0)

       usbre45_to_pack = np.round(usbre45*2**29)
       usbre45_packed  = struct.pack('>512l',*usbre45_to_pack)
       fpga.write('usbre45', usbre45_packed, 0)

       usbim45_to_pack = np.round(usbim45*2**29)
       usbim45_packed  = struct.pack('>512l',*usbim45_to_pack)
       fpga.write('usbim45', usbim45_packed, 0)

       # 666666 777777 #
       lsbre67_to_pack = np.round(lsbre67*2**29)
       lsbre67_packed  = struct.pack('>512l',*lsbre67_to_pack)
       fpga.write('lsbre67', lsbre67_packed, 0)
 
       lsbim67_to_pack = np.round(lsbim67*2**29)
       lsbim67_packed  = struct.pack('>512l',*lsbim67_to_pack)
       fpga.write('lsbim67', lsbim67_packed, 0)

       usbre67_to_pack = np.round(usbre67*2**29)
       usbre67_packed  = struct.pack('>512l',*usbre67_to_pack)
       fpga.write('usbre67', usbre67_packed, 0)

       usbim67_to_pack = np.round(usbim67*2**29)
       usbim67_packed  = struct.pack('>512l',*usbim67_to_pack)
       fpga.write('usbim67', usbim67_packed, 0)
       print 'done. \n'

    #if (opts.set_coef_ones==1):
    #    ones_to_pack = np.round(ones_coeffs*2**29)
    #    ones_packed = struct.pack('>512l',*ones_to_pack)
    #    fpga.write('lsbre01', ones_packed, 0)
    #    fpga.write('lsbim01', ones_packed, 0)
    #    fpga.write('usbre01', ones_packed, 0)
    #    fpga.write('usbim01', ones_packed, 0)

    #    fpga.write('lsbre23', ones_packed, 0)
    #    fpga.write('lsbim23', ones_packed, 0)
    #    fpga.write('usbre23', ones_packed, 0)
    #    fpga.write('usbim23', ones_packed, 0)

    #    fpga.write('lsbre45', ones_packed, 0)
    #    fpga.write('lsbim45', ones_packed, 0)
    #    fpga.write('usbre45', ones_packed, 0)
    #    fpga.write('usbim45', ones_packed, 0)

    #    fpga.write('lsbre67', ones_packed, 0)
    #    fpga.write('lsbim67', ones_packed, 0)
    #    fpga.write('usbre67', ones_packed, 0)
    #    fpga.write('usbim67', ones_packed, 0)

    if (opts.auto_align_bits):
       print 'Aligning bits... \n'
       fpga.write_int('msb_realign',1)
       time.sleep(0.5)
       fpga.write_int('msb_realign',0)
       time.sleep(0.5)             #luke
       if(fpga.read_int('bit_locked') !=255):#luke
            print 'bit locked failed. \n'   #luke
       print 'done. \n'

    if (opts.auto_align_bytes == 8):
        final_data_0 = fpga.read('final_data_0',131072)
        final_data_0_up = struct.unpack('>65536h', final_data_0)
        #print(final_data_0_up)
        from scipy.fftpack import fft, rfft
        fft_0 = fft((final_data_0_up))

        final_data_1 = fpga.read('final_data_1',131072)
        final_data_1_up = struct.unpack('>65536h', final_data_1)
        #print(final_data_1_up)
        #from scipy.fftpack import fft
        fft_1 = fft(final_data_1_up)

        final_data_2 = fpga.read('final_data_2',131072)
        final_data_2_up = struct.unpack('>65536h', final_data_2)
        #print(final_data_2_up)
        from scipy.fftpack import fft
        fft_2 = fft(final_data_2_up)

        final_data_3 = fpga.read('final_data_3',131072)
        final_data_3_up = struct.unpack('>65536h', final_data_3)
        #print(final_data_3_up)
        from scipy.fftpack import fft
        fft_3 = fft(final_data_3_up)

        final_data_4 = fpga.read('final_data_4',131072)
        final_data_4_up = struct.unpack('>65536h', final_data_4)
        #print(final_data_4_up)
        #from scipy.fftpack import fft
        fft_4 = fft(final_data_4_up)

        final_data_5 = fpga.read('final_data_5',131072)
        final_data_5_up = struct.unpack('>65536h', final_data_5)
        #print(final_data_5_up)
        from scipy.fftpack import fft
        fft_5 = fft(final_data_5_up)

        final_data_6 = fpga.read('final_data_6',131072)
        final_data_6_up = struct.unpack('>65536h', final_data_6)
        #print(final_data_6_up)
        from scipy.fftpack import fft
        fft_6 = fft(final_data_6_up)

        final_data_7 = fpga.read('final_data_7',131072)
        final_data_7_up = struct.unpack('>65536h', final_data_7)
        #print(final_data_7_up)
        from scipy.fftpack import fft
        fft_7 = fft(final_data_7_up)

        print(sum(fft_0))
        print(sum(fft_1))
        print(sum(fft_2))
        print(sum(fft_3))
        
        x_min=0
        x_max=65536/2
        y_min=1000000
        y_max=100000000

        #import matplotlib.pyplot as plt
        #plt.subplot(8,1,1)
        #plt.plot(fft_0, color = 'red')
        ##plt.plot(fft_2, color = 'red')
        ##plt.plot((sbs0_supressed), color = 'black')
        #plt.axis([x_min, x_max, y_min, y_max])
        #plt.yscale('log')

        #plt.subplot(8,1,2)
        ##plt.plot(fft_0, color = 'blue')
        #plt.plot(fft_1, color = 'red')
        ##plt.plot((sbs0_supressed), color = 'black')
        #plt.axis([x_min, x_max, y_min, y_max])
        #plt.yscale('log')

        #plt.subplot(8,1,3)
        ##plt.plot(fft_0, color = 'blue')
        #plt.plot(fft_2, color = 'red')
        ##plt.plot((sbs0_supressed), color = 'black')
        #plt.axis([x_min, x_max, y_min, y_max])
        #plt.yscale('log')

        #plt.subplot(8,1,4)
        ##plt.plot(fft_0, color = 'blue')
        #plt.plot(fft_3, color = 'red')
        ##plt.plot((sbs0_supressed), color = 'black')
        #plt.axis([x_min, x_max, y_min, y_max])
        #plt.yscale('log')

        #plt.subplot(8,1,5)
        ##plt.plot(fft_0, color = 'blue')
        #plt.plot(fft_5, color = 'red')
        ##plt.plot((sbs0_supressed), color = 'black')
        #plt.axis([x_min, x_max, y_min, y_max])
        #plt.yscale('log')

        #plt.subplot(8,1,6)
        ##plt.plot(fft_0, color = 'blue')
        #plt.plot(fft_5, color = 'red')
        ##plt.plot((sbs0_supressed), color = 'black')
        #plt.axis([x_min, x_max, y_min, y_max])
        #plt.yscale('log')

        #plt.subplot(8,1,7)
        ##plt.plot(fft_0, color = 'blue')
        #plt.plot(fft_6, color = 'red')
        ##plt.plot((sbs0_supressed), color = 'black')
        #plt.axis([x_min, x_max, y_min, y_max])
        #plt.yscale('log')

        #plt.subplot(8,1,8)
        ##plt.plot(fft_0, color = 'blue')
        #plt.plot(fft_7, color = 'red')
        ##plt.plot((sbs0_supressed), color = 'black')
        #plt.axis([x_min, x_max, y_min, y_max])
        #plt.yscale('log')

        #plt.show()

        from scipy import fftpack
        x0x=fftpack.fft(final_data_0_up)
        x1x=fftpack.fft(final_data_1_up)
        x2x=fftpack.fft(final_data_2_up)
        x3x=fftpack.fft(final_data_3_up)
        x4x=fftpack.fft(final_data_4_up)
        x5x=fftpack.fft(final_data_5_up)
        x6x=fftpack.fft(final_data_6_up)
        x7x=fftpack.fft(final_data_7_up)

            #process_list = process_list + thread5.split()
        #plt.subplot(8,1,1)
        #plt.plot(x0x, color = 'red')
        #plt.axis([x_min, x_max, y_min, y_max])

        #plt.subplot(8,1,2)
        #plt.plot(x1x, color = 'red')
        #plt.axis([x_min, x_max, y_min, y_max])

        #plt.subplot(8,1,3)
        #plt.plot(x2x, color = 'red')
        #plt.axis([x_min, x_max, y_min, y_max])

        #plt.subplot(8,1,4)
        #plt.plot(x3x, color = 'red')
        #plt.axis([x_min, x_max, y_min, y_max])

        #plt.subplot(8,1,5)
        #plt.plot(x4x, color = 'red')
        #plt.axis([x_min, x_max, y_min, y_max])

        #plt.subplot(8,1,6)
        #plt.plot(x5x, color = 'red')
        #plt.axis([x_min, x_max, y_min, y_max])

        #plt.subplot(8,1,7)
        #plt.plot(x6x, color = 'red')
        #plt.axis([x_min, x_max, y_min, y_max])

        #plt.subplot(8,1,8)
        #plt.plot(x7x, color = 'red')
        #plt.axis([x_min, x_max, y_min, y_max])

        #plt.show()
  



    elif (opts.auto_align_bytes==1):
       print 'Aligning IQ... \n'
       
       ###############
       #Luke debug
       ###############

    
       time.sleep(0.3)
       ####################################
       ####################################
       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,0)
       usb0_mag1, lsb0_mag1, usb1_mag1, lsb1_mag1 = treat_data_sbs(0,0)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag1, lsb2_mag1, usb3_mag1, lsb3_mag1 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag1, lsb4_mag1, usb5_mag1, lsb5_mag1 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag1, lsb6_mag1, usb7_mag1, lsb7_mag1 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag2, lsb0_mag2, usb1_mag2, lsb1_mag2 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag2, lsb2_mag2, usb3_mag2, lsb3_mag2 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag2, lsb4_mag2, usb5_mag2, lsb5_mag2 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag2, lsb6_mag2, usb7_mag2, lsb7_mag2 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag3, lsb0_mag3, usb1_mag3, lsb1_mag3 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag3, lsb2_mag3, usb3_mag3, lsb3_mag3 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag3, lsb4_mag3, usb5_mag3, lsb5_mag3 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag3, lsb6_mag3, usb7_mag3, lsb7_mag3 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag4, lsb0_mag4, usb1_mag4, lsb1_mag4 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag4, lsb2_mag4, usb3_mag4, lsb3_mag4 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag4, lsb4_mag4, usb5_mag4, lsb5_mag4 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag4, lsb6_mag4, usb7_mag4, lsb7_mag4 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag5, lsb0_mag5, usb1_mag5, lsb1_mag5 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag5, lsb2_mag5, usb3_mag5, lsb3_mag5 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag5, lsb4_mag5, usb5_mag5, lsb5_mag5 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag5, lsb6_mag5, usb7_mag5, lsb7_mag5 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag6, lsb0_mag6, usb1_mag6, lsb1_mag6 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag6, lsb2_mag6, usb3_mag6, lsb3_mag6 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag6, lsb4_mag6, usb5_mag6, lsb5_mag6 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag6, lsb6_mag6, usb7_mag6, lsb7_mag6 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag7, lsb0_mag7, usb1_mag7, lsb1_mag7 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag7, lsb2_mag7, usb3_mag7, lsb3_mag7 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag7, lsb4_mag7, usb5_mag7, lsb5_mag7 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag7, lsb6_mag7, usb7_mag7, lsb7_mag7 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag8, lsb0_mag8, usb1_mag8, lsb1_mag8 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag8, lsb2_mag8, usb3_mag8, lsb3_mag8 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag8, lsb4_mag8, usb5_mag8, lsb5_mag8 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag8, lsb6_mag8, usb7_mag8, lsb7_mag8 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag9, lsb0_mag9, usb1_mag9, lsb1_mag9 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag9, lsb2_mag9, usb3_mag9, lsb3_mag9 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag9, lsb4_mag9, usb5_mag9, lsb5_mag9 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag9, lsb6_mag9, usb7_mag9, lsb7_mag9 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag10, lsb0_mag10, usb1_mag10, lsb1_mag10 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag10, lsb2_mag10, usb3_mag10, lsb3_mag10 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag10, lsb4_mag10, usb5_mag10, lsb5_mag10 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag10, lsb6_mag10, usb7_mag10, lsb7_mag10 = treat_data_sbs(6,1)

       time.sleep(0.1)
       
       usb0_mag=usb0_mag1
       lsb0_mag=lsb0_mag1
       usb1_mag=usb1_mag1
       lsb1_mag=lsb1_mag1
       usb2_mag=usb2_mag1
       lsb2_mag=lsb2_mag1
       usb3_mag=usb3_mag1
       lsb3_mag=lsb3_mag1
       usb4_mag=usb4_mag1
       lsb4_mag=lsb4_mag1
       usb5_mag=usb5_mag1
       lsb5_mag=lsb5_mag1
       usb6_mag=usb6_mag1
       lsb6_mag=lsb6_mag1
       usb7_mag=usb7_mag1
       lsb7_mag=lsb7_mag1

       ii=1
       #while ii<257:
       ########luke
       while ii<256:
            usb0_mag[ii] = (usb0_mag1[ii]+usb0_mag2[ii]+usb0_mag3[ii]+usb0_mag4[ii]+usb0_mag5[ii]+usb0_mag6[ii]+usb0_mag7[ii]+usb0_mag8[ii]+usb0_mag9[ii]+usb0_mag10[ii])/10
            lsb0_mag[ii] = (lsb0_mag1[ii]+lsb0_mag2[ii]+lsb0_mag3[ii]+lsb0_mag5[ii]+lsb0_mag5[ii]+lsb0_mag6[ii]+lsb0_mag7[ii]+lsb0_mag8[ii]+lsb0_mag9[ii]+lsb0_mag10[ii])/10
            usb1_mag[ii] = (usb1_mag1[ii]+usb1_mag2[ii]+usb1_mag3[ii]+usb1_mag4[ii]+usb1_mag5[ii]+usb1_mag6[ii]+usb1_mag7[ii]+usb1_mag8[ii]+usb1_mag9[ii]+usb1_mag10[ii])/10
            lsb1_mag[ii] = (lsb1_mag1[ii]+lsb1_mag2[ii]+lsb1_mag3[ii]+lsb1_mag5[ii]+lsb1_mag5[ii]+lsb1_mag6[ii]+lsb1_mag7[ii]+lsb1_mag8[ii]+lsb1_mag9[ii]+lsb1_mag10[ii])/10
            usb2_mag[ii] = (usb2_mag1[ii]+usb2_mag2[ii]+usb2_mag3[ii]+usb2_mag4[ii]+usb2_mag5[ii]+usb2_mag6[ii]+usb2_mag7[ii]+usb2_mag8[ii]+usb2_mag9[ii]+usb2_mag10[ii])/10
            lsb2_mag[ii] = (lsb2_mag1[ii]+lsb2_mag2[ii]+lsb2_mag3[ii]+lsb2_mag5[ii]+lsb2_mag5[ii]+lsb2_mag6[ii]+lsb2_mag7[ii]+lsb2_mag8[ii]+lsb2_mag9[ii]+lsb2_mag10[ii])/10
            usb3_mag[ii] = (usb3_mag1[ii]+usb3_mag2[ii]+usb3_mag3[ii]+usb3_mag4[ii]+usb3_mag5[ii]+usb3_mag6[ii]+usb3_mag7[ii]+usb3_mag8[ii]+usb3_mag9[ii]+usb3_mag10[ii])/10
            lsb3_mag[ii] = (lsb3_mag1[ii]+lsb3_mag2[ii]+lsb3_mag3[ii]+lsb3_mag5[ii]+lsb3_mag5[ii]+lsb3_mag6[ii]+lsb3_mag7[ii]+lsb3_mag8[ii]+lsb3_mag9[ii]+lsb3_mag10[ii])/10
            usb4_mag[ii] = (usb4_mag1[ii]+usb4_mag2[ii]+usb4_mag3[ii]+usb4_mag4[ii]+usb4_mag5[ii]+usb4_mag6[ii]+usb4_mag7[ii]+usb4_mag8[ii]+usb4_mag9[ii]+usb4_mag10[ii])/10
            lsb4_mag[ii] = (lsb4_mag1[ii]+lsb4_mag2[ii]+lsb4_mag3[ii]+lsb4_mag5[ii]+lsb4_mag5[ii]+lsb4_mag6[ii]+lsb4_mag7[ii]+lsb4_mag8[ii]+lsb4_mag9[ii]+lsb4_mag10[ii])/10
            usb5_mag[ii] = (usb5_mag1[ii]+usb5_mag2[ii]+usb5_mag3[ii]+usb5_mag4[ii]+usb5_mag5[ii]+usb5_mag6[ii]+usb5_mag7[ii]+usb5_mag8[ii]+usb5_mag9[ii]+usb5_mag10[ii])/10
            lsb5_mag[ii] = (lsb5_mag1[ii]+lsb5_mag2[ii]+lsb5_mag3[ii]+lsb5_mag5[ii]+lsb5_mag5[ii]+lsb5_mag6[ii]+lsb5_mag7[ii]+lsb5_mag8[ii]+lsb5_mag9[ii]+lsb5_mag10[ii])/10
            usb6_mag[ii] = (usb6_mag1[ii]+usb6_mag2[ii]+usb6_mag3[ii]+usb6_mag4[ii]+usb6_mag5[ii]+usb6_mag6[ii]+usb6_mag7[ii]+usb6_mag8[ii]+usb6_mag9[ii]+usb6_mag10[ii])/10
            lsb6_mag[ii] = (lsb6_mag1[ii]+lsb6_mag2[ii]+lsb6_mag3[ii]+lsb6_mag5[ii]+lsb6_mag5[ii]+lsb6_mag6[ii]+lsb6_mag7[ii]+lsb6_mag8[ii]+lsb6_mag9[ii]+lsb6_mag10[ii])/10
            usb7_mag[ii] = (usb7_mag1[ii]+usb7_mag2[ii]+usb7_mag3[ii]+usb7_mag4[ii]+usb7_mag5[ii]+usb7_mag6[ii]+usb7_mag7[ii]+usb7_mag8[ii]+usb7_mag9[ii]+usb7_mag10[ii])/10
            lsb7_mag[ii] = (lsb7_mag1[ii]+lsb7_mag2[ii]+lsb7_mag3[ii]+lsb7_mag5[ii]+lsb7_mag5[ii]+lsb7_mag6[ii]+lsb7_mag7[ii]+lsb7_mag8[ii]+lsb7_mag9[ii]+lsb7_mag10[ii])/10
            ii=ii+1

       #mag0_rat_a=usb0_mag[166]/lsb0_mag[166]
       #mag1_rat_a=usb1_mag[166]/lsb1_mag[166]
       #mag2_rat_a=usb2_mag[166]/lsb2_mag[166]
       #mag3_rat_a=usb3_mag[166]/lsb3_mag[166]
       #mag4_rat_a=usb4_mag[166]/lsb4_mag[166]
       #mag5_rat_a=usb5_mag[166]/lsb5_mag[166]
       #mag6_rat_a=usb6_mag[166]/lsb6_mag[166]
       #mag7_rat_a=usb7_mag[166]/lsb7_mag[166]


       #mag0_rat_a=usb0_mag[33]/lsb0_mag[32]
       #mag1_rat_a=usb1_mag[32]/lsb1_mag[32]
       #mag2_rat_a=usb2_mag[32]/lsb2_mag[32]
       #mag3_rat_a=usb3_mag[32]/lsb3_mag[32]
       #mag4_rat_a=usb4_mag[32]/lsb4_mag[32]
       #mag5_rat_a=usb5_mag[32]/lsb5_mag[32]
       #mag6_rat_a=usb6_mag[32]/lsb6_mag[32]
       #mag7_rat_a=usb7_mag[32]/lsb7_mag[32]

       indexx=67
       indexfh=123
       #34=10MHz
       #100=30MHz
       #199=60MHz
       #51=15MHz

       mag0_rat_a=(usb0_mag[indexx])/(lsb0_mag[indexx])
       mag1_rat_a=(usb1_mag[indexx])/(lsb1_mag[indexx])
       mag2_rat_a=(usb2_mag[indexx])/(lsb2_mag[indexx])
       mag3_rat_a=(usb3_mag[indexx])/(lsb3_mag[indexx])
       mag4_rat_a=(usb4_mag[indexx])/(lsb4_mag[indexx])
       mag5_rat_a=(usb5_mag[indexx])/(lsb5_mag[indexx])
       mag6_rat_a=(usb6_mag[indexx])/(lsb6_mag[indexx])
       mag7_rat_a=(usb7_mag[indexx])/(lsb7_mag[indexx])

       print('print sideband sums')
       print(' ')
       print(sum(usb0_mag)-usb0_mag[0]-usb0_mag[1]-usb0_mag[2]-usb0_mag[3])
       print(sum(lsb0_mag)-lsb0_mag[0]-lsb0_mag[1]-lsb0_mag[2]-lsb0_mag[3])
       print(' ')
       print(sum(usb1_mag)-usb1_mag[0]-usb1_mag[1]-usb1_mag[2]-usb1_mag[3])
       print(sum(lsb1_mag)-lsb1_mag[0]-lsb1_mag[1]-lsb1_mag[2]-lsb1_mag[3])
       print(' ')
       print(sum(usb2_mag)-usb2_mag[0]-usb2_mag[1]-usb2_mag[2]-usb2_mag[3])
       print(sum(lsb2_mag)-lsb2_mag[0]-lsb2_mag[1]-lsb2_mag[2]-lsb2_mag[3])
       print(' ')
       print(sum(usb3_mag)-usb3_mag[0]-usb3_mag[1]-usb3_mag[2]-usb3_mag[3])
       print(sum(lsb3_mag)-lsb3_mag[0]-lsb3_mag[1]-lsb3_mag[2]-lsb3_mag[3])
       print(' ')
       print(sum(usb4_mag)-usb4_mag[0]-usb4_mag[1]-usb4_mag[2]-usb4_mag[3])
       print(sum(lsb4_mag)-lsb4_mag[0]-lsb4_mag[1]-lsb4_mag[2]-lsb4_mag[3])
       print(' ')
       print(sum(usb5_mag)-usb5_mag[0]-usb5_mag[1]-usb5_mag[2]-usb5_mag[3])
       print(sum(lsb5_mag)-lsb5_mag[0]-lsb5_mag[1]-lsb5_mag[2]-lsb5_mag[3])
       print(' ')
       print(sum(usb6_mag)-usb6_mag[0]-usb6_mag[1]-usb6_mag[2]-usb6_mag[3])
       print(sum(lsb6_mag)-lsb6_mag[0]-lsb6_mag[1]-lsb6_mag[2]-lsb6_mag[3])
       print(' ')
       print(sum(usb7_mag)-usb7_mag[0]-usb7_mag[1]-usb7_mag[2]-usb7_mag[3])
       print(sum(lsb7_mag)-lsb7_mag[0]-lsb7_mag[1]-lsb7_mag[2]-lsb7_mag[3])
       print(' ')
       print(' ')


        
       print('testing 1st harmonic')

       print(' ')
       print('chan 0')
       print(usb0_mag[indexfh])
       print(lsb0_mag[indexfh])

       print(' ')
       print('chan 1')
       print(usb1_mag[indexfh])
       print(lsb1_mag[indexfh])

       print(' ')
       print('chan 2')
       print(usb2_mag[indexfh])
       print(lsb2_mag[indexfh])

       print(' ')
       print('chan 3')
       print(usb3_mag[indexfh])
       print(lsb3_mag[indexfh])

       print(' ')
       print('chan 4')
       print(usb4_mag[indexfh])
       print(lsb4_mag[indexfh])

       print(' ')
       print('chan 5')
       print(usb5_mag[indexfh])
       print(lsb5_mag[indexfh])

       print(' ')
       print('chan 6')
       print(usb6_mag[indexfh])
       print(lsb6_mag[indexfh])

       print(' ')
       print('chan 7')
       print(usb7_mag[indexfh])
       print(lsb7_mag[indexfh])

       print(' ')
       print(' ')

       print('chan 0')
       print(usb0_mag[indexx])
       print(lsb0_mag[indexx])
       print('chan 1')
       print(usb1_mag[indexx])
       print(lsb1_mag[indexx])
       print('chan 2')
       print(usb2_mag[indexx])
       print(lsb2_mag[indexx])
       print('chan 3')
       print(usb3_mag[indexx])
       print(lsb3_mag[indexx])
       print('chan 4')
       print(usb4_mag[indexx])
       print(lsb4_mag[indexx])
       print('chan 5')
       print(usb5_mag[indexx])
       print(lsb5_mag[indexx])
       print('chan 6')
       print(usb6_mag[indexx])
       print(lsb6_mag[indexx])
       print('chan 7')
       print(usb7_mag[indexx])
       print(lsb7_mag[indexx])

       print(usb0_mag)
       print(lsb0_mag)

       print(' ')
       print(' ')

       print(usb1_mag)
       print(lsb1_mag)

       print(' ')
       print(' ')

       print(usb3_mag)
       print(lsb3_mag)

       print(' ')
       print(' ')

       print(usb6_mag)
       print(lsb6_mag)

       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)





       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,0)
       usb0_mag1, lsb0_mag1, usb1_mag1, lsb1_mag1 = treat_data_sbs(0,0)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag1, lsb2_mag1, usb3_mag1, lsb3_mag1 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag1, lsb4_mag1, usb5_mag1, lsb5_mag1 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag1, lsb6_mag1, usb7_mag1, lsb7_mag1 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag2, lsb0_mag2, usb1_mag2, lsb1_mag2 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag2, lsb2_mag2, usb3_mag2, lsb3_mag2 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag2, lsb4_mag2, usb5_mag2, lsb5_mag2 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag2, lsb6_mag2, usb7_mag2, lsb7_mag2 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag3, lsb0_mag3, usb1_mag3, lsb1_mag3 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag3, lsb2_mag3, usb3_mag3, lsb3_mag3 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag3, lsb4_mag3, usb5_mag3, lsb5_mag3 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag3, lsb6_mag3, usb7_mag3, lsb7_mag3 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag4, lsb0_mag4, usb1_mag4, lsb1_mag4 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag4, lsb2_mag4, usb3_mag4, lsb3_mag4 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag4, lsb4_mag4, usb5_mag4, lsb5_mag4 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag4, lsb6_mag4, usb7_mag4, lsb7_mag4 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag5, lsb0_mag5, usb1_mag5, lsb1_mag5 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag5, lsb2_mag5, usb3_mag5, lsb3_mag5 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag5, lsb4_mag5, usb5_mag5, lsb5_mag5 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag5, lsb6_mag5, usb7_mag5, lsb7_mag5 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag6, lsb0_mag6, usb1_mag6, lsb1_mag6 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag6, lsb2_mag6, usb3_mag6, lsb3_mag6 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag6, lsb4_mag6, usb5_mag6, lsb5_mag6 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag6, lsb6_mag6, usb7_mag6, lsb7_mag6 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag7, lsb0_mag7, usb1_mag7, lsb1_mag7 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag7, lsb2_mag7, usb3_mag7, lsb3_mag7 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag7, lsb4_mag7, usb5_mag7, lsb5_mag7 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag7, lsb6_mag7, usb7_mag7, lsb7_mag7 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag8, lsb0_mag8, usb1_mag8, lsb1_mag8 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag8, lsb2_mag8, usb3_mag8, lsb3_mag8 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag8, lsb4_mag8, usb5_mag8, lsb5_mag8 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag8, lsb6_mag8, usb7_mag8, lsb7_mag8 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag9, lsb0_mag9, usb1_mag9, lsb1_mag9 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag9, lsb2_mag9, usb3_mag9, lsb3_mag9 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag9, lsb4_mag9, usb5_mag9, lsb5_mag9 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag9, lsb6_mag9, usb7_mag9, lsb7_mag9 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag10, lsb0_mag10, usb1_mag10, lsb1_mag10 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag10, lsb2_mag10, usb3_mag10, lsb3_mag10 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag10, lsb4_mag10, usb5_mag10, lsb5_mag10 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag10, lsb6_mag10, usb7_mag10, lsb7_mag10 = treat_data_sbs(6,1)

       time.sleep(0.1)
       
       usb0_mag=usb0_mag1
       lsb0_mag=lsb0_mag1
       usb1_mag=usb1_mag1
       lsb1_mag=lsb1_mag1
       usb2_mag=usb2_mag1
       lsb2_mag=lsb2_mag1
       usb3_mag=usb3_mag1
       lsb3_mag=lsb3_mag1
       usb4_mag=usb4_mag1
       lsb4_mag=lsb4_mag1
       usb5_mag=usb5_mag1
       lsb5_mag=lsb5_mag1
       usb6_mag=usb6_mag1
       lsb6_mag=lsb6_mag1
       usb7_mag=usb7_mag1
       lsb7_mag=lsb7_mag1

       ii=1
       #while ii<257:
       ########luke
       while ii<256:
            usb0_mag[ii] = usb0_mag1[ii]+usb0_mag2[ii]+usb0_mag3[ii]+usb0_mag4[ii]+usb0_mag5[ii]+usb0_mag6[ii]+usb0_mag7[ii]+usb0_mag8[ii]+usb0_mag9[ii]+usb0_mag10[ii]
            lsb0_mag[ii] = lsb0_mag1[ii]+lsb0_mag2[ii]+lsb0_mag3[ii]+lsb0_mag5[ii]+lsb0_mag5[ii]+lsb0_mag6[ii]+lsb0_mag7[ii]+lsb0_mag8[ii]+lsb0_mag9[ii]+lsb0_mag10[ii]
            usb1_mag[ii] = usb1_mag1[ii]+usb1_mag2[ii]+usb1_mag3[ii]+usb1_mag4[ii]+usb1_mag5[ii]+usb1_mag6[ii]+usb1_mag7[ii]+usb1_mag8[ii]+usb1_mag9[ii]+usb1_mag10[ii]
            lsb1_mag[ii] = lsb1_mag1[ii]+lsb1_mag2[ii]+lsb1_mag3[ii]+lsb1_mag5[ii]+lsb1_mag5[ii]+lsb1_mag6[ii]+lsb1_mag7[ii]+lsb1_mag8[ii]+lsb1_mag9[ii]+lsb1_mag10[ii]
            usb2_mag[ii] = usb2_mag1[ii]+usb2_mag2[ii]+usb2_mag3[ii]+usb2_mag4[ii]+usb2_mag5[ii]+usb2_mag6[ii]+usb2_mag7[ii]+usb2_mag8[ii]+usb2_mag9[ii]+usb2_mag10[ii]
            lsb2_mag[ii] = lsb2_mag1[ii]+lsb2_mag2[ii]+lsb2_mag3[ii]+lsb2_mag5[ii]+lsb2_mag5[ii]+lsb2_mag6[ii]+lsb2_mag7[ii]+lsb2_mag8[ii]+lsb2_mag9[ii]+lsb2_mag10[ii]
            usb3_mag[ii] = usb3_mag1[ii]+usb3_mag2[ii]+usb3_mag3[ii]+usb3_mag4[ii]+usb3_mag5[ii]+usb3_mag6[ii]+usb3_mag7[ii]+usb3_mag8[ii]+usb3_mag9[ii]+usb3_mag10[ii]
            lsb3_mag[ii] = lsb3_mag1[ii]+lsb3_mag2[ii]+lsb3_mag3[ii]+lsb3_mag5[ii]+lsb3_mag5[ii]+lsb3_mag6[ii]+lsb3_mag7[ii]+lsb3_mag8[ii]+lsb3_mag9[ii]+lsb3_mag10[ii]
            usb4_mag[ii] = usb4_mag1[ii]+usb4_mag2[ii]+usb4_mag3[ii]+usb4_mag4[ii]+usb4_mag5[ii]+usb4_mag6[ii]+usb4_mag7[ii]+usb4_mag8[ii]+usb4_mag9[ii]+usb4_mag10[ii]
            lsb4_mag[ii] = lsb4_mag1[ii]+lsb4_mag2[ii]+lsb4_mag3[ii]+lsb4_mag5[ii]+lsb4_mag5[ii]+lsb4_mag6[ii]+lsb4_mag7[ii]+lsb4_mag8[ii]+lsb4_mag9[ii]+lsb4_mag10[ii]
            usb5_mag[ii] = usb5_mag1[ii]+usb5_mag2[ii]+usb5_mag3[ii]+usb5_mag4[ii]+usb5_mag5[ii]+usb5_mag6[ii]+usb5_mag7[ii]+usb5_mag8[ii]+usb5_mag9[ii]+usb5_mag10[ii]
            lsb5_mag[ii] = lsb5_mag1[ii]+lsb5_mag2[ii]+lsb5_mag3[ii]+lsb5_mag5[ii]+lsb5_mag5[ii]+lsb5_mag6[ii]+lsb5_mag7[ii]+lsb5_mag8[ii]+lsb5_mag9[ii]+lsb5_mag10[ii]
            usb6_mag[ii] = usb6_mag1[ii]+usb6_mag2[ii]+usb6_mag3[ii]+usb6_mag4[ii]+usb6_mag5[ii]+usb6_mag6[ii]+usb6_mag7[ii]+usb6_mag8[ii]+usb6_mag9[ii]+usb6_mag10[ii]
            lsb6_mag[ii] = lsb6_mag1[ii]+lsb6_mag2[ii]+lsb6_mag3[ii]+lsb6_mag5[ii]+lsb6_mag5[ii]+lsb6_mag6[ii]+lsb6_mag7[ii]+lsb6_mag8[ii]+lsb6_mag9[ii]+lsb6_mag10[ii]
            usb7_mag[ii] = usb7_mag1[ii]+usb7_mag2[ii]+usb7_mag3[ii]+usb7_mag4[ii]+usb7_mag5[ii]+usb7_mag6[ii]+usb7_mag7[ii]+usb7_mag8[ii]+usb7_mag9[ii]+usb7_mag10[ii]
            lsb7_mag[ii] = lsb7_mag1[ii]+lsb7_mag2[ii]+lsb7_mag3[ii]+lsb7_mag5[ii]+lsb7_mag5[ii]+lsb7_mag6[ii]+lsb7_mag7[ii]+lsb7_mag8[ii]+lsb7_mag9[ii]+lsb7_mag10[ii]
            ii=ii+1

       #mag0_rat_b=usb0_mag[166]/lsb0_mag[166]
       #mag1_rat_b=usb1_mag[166]/lsb1_mag[166]
       #mag2_rat_b=usb2_mag[166]/lsb2_mag[166]
       #mag3_rat_b=usb3_mag[166]/lsb3_mag[166]
       #mag4_rat_b=usb4_mag[166]/lsb4_mag[166]
       #mag5_rat_b=usb5_mag[166]/lsb5_mag[166]
       #mag6_rat_b=usb6_mag[166]/lsb6_mag[166]
       #mag7_rat_b=usb7_mag[166]/lsb7_mag[166]

       #mag0_rat_b=usb0_mag[32]/lsb0_mag[32]
       #mag1_rat_b=usb1_mag[32]/lsb1_mag[32]
       #mag2_rat_b=usb2_mag[32]/lsb2_mag[32]
       #mag3_rat_b=usb3_mag[32]/lsb3_mag[32]
       #mag4_rat_b=usb4_mag[32]/lsb4_mag[32]
       #mag5_rat_b=usb5_mag[32]/lsb5_mag[32]
       #mag6_rat_b=usb6_mag[32]/lsb6_mag[32]
       #mag7_rat_b=usb7_mag[32]/lsb7_mag[32]

       mag0_rat_b=(usb0_mag[indexx])/(lsb0_mag[indexx])
       mag1_rat_b=(usb1_mag[indexx])/(lsb1_mag[indexx])
       mag2_rat_b=(usb2_mag[indexx])/(lsb2_mag[indexx])
       mag3_rat_b=(usb3_mag[indexx])/(lsb3_mag[indexx])
       mag4_rat_b=(usb4_mag[indexx])/(lsb4_mag[indexx])
       mag5_rat_b=(usb5_mag[indexx])/(lsb5_mag[indexx])
       mag6_rat_b=(usb6_mag[indexx])/(lsb6_mag[indexx])
       mag7_rat_b=(usb7_mag[indexx])/(lsb7_mag[indexx])


       print ('mag0_rat_a')
       print mag0_rat_a
       print ('mag0_rat_b')
       print mag0_rat_b

       print ('mag1_rat_a')
       print mag1_rat_a
       print ('mag1_rat_b')
       print mag1_rat_b

       print ('mag2_rat_a')
       print mag2_rat_a
       print ('mag2_rat_b')
       print mag2_rat_b

       print ('mag3_rat_a')
       print mag3_rat_a
       print ('mag3_rat_b')
       print mag3_rat_b

       print ('mag4_rat_a')
       print mag4_rat_a
       print ('mag4_rat_b')
       print mag4_rat_b

       print ('mag5_rat_a')
       print mag5_rat_a
       print ('mag5_rat_b')
       print mag5_rat_b

       print ('mag6_rat_a')
       print mag6_rat_a
       print ('mag6_rat_b')
       print mag6_rat_b

       print ('mag7_rat_a')
       print mag7_rat_a
       print ('mag7_rat_b')
       print mag7_rat_b

       #print(usb3_mag)
       #print(lsb3_mag)

       print('chan 0')
       print(usb0_mag[indexx])
       print(lsb0_mag[indexx])
       print('chan 1')
       print(usb1_mag[indexx])
       print(lsb1_mag[indexx])
       print('chan 2')
       print(usb2_mag[indexx])
       print(lsb2_mag[indexx])
       print('chan 3')
       print(usb3_mag[indexx])
       print(lsb3_mag[indexx])
       print('chan 4')
       print(usb4_mag[indexx])
       print(lsb4_mag[indexx])
       print('chan 5')
       print(usb5_mag[indexx])
       print(lsb5_mag[indexx])
       print('chan 6')
       print(usb6_mag[indexx])
       print(lsb6_mag[indexx])
       print('chan 7')
       print(usb7_mag[indexx])
       print(lsb7_mag[indexx])

       if(mag0_rat_b<mag0_rat_a):
            ##flyp dem bites on A0!!!
            print 're-aligning 0 \n'
            fpga.write_int('rxslide',1)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',1)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',1)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',1)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',1)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',1)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',1)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',1)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)


       if(mag1_rat_b<mag1_rat_a):
            ##flyp dem bites on A1!!!
            print 're-aligning 1 \n'
            fpga.write_int('rxslide',2)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',2)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',2)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',2)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',2)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',2)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',2)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',2)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)


       if(mag2_rat_b<mag2_rat_a):
            ##flyp dem bites on A2!!!
            print 're-aligning 2 \n'
            fpga.write_int('rxslide',4)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',4)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',4)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',4)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',4)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',4)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',4)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',4)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)


       if(mag3_rat_b<mag3_rat_a):
            ##flyp dem bites on A3!!!
            print 're-aligning 3 \n'
            fpga.write_int('rxslide',8)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',8)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',8)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',8)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',8)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',8)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',8)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',8)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)


       if(mag4_rat_b<mag4_rat_a):
            ##flyp dem bites on A4!!!
            print 're-aligning 4 \n'
            fpga.write_int('rxslide',16)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',16)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',16)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',16)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',16)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',16)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',16)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',16)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)


       if(mag5_rat_b<mag5_rat_a):
            ##flyp dem bites on A5!!!
            print 're-aligning 5 \n'
            fpga.write_int('rxslide',32)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',32)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',32)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',32)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',32)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',32)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',32)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',32)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)


       if(mag6_rat_b<mag6_rat_a):
            ##flyp dem bites on A6!!!
            print 're-aligning 6 \n'
            fpga.write_int('rxslide',64)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',64)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',64)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',64)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',64)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',64)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',64)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',64)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)


       if(mag7_rat_b<mag7_rat_a):
            ##flyp dem bites on A7!!!
            print 're-aligning 7 \n'
            fpga.write_int('rxslide',128)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',128)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',128)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',128)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',128)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',128)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',128)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)
            time.sleep(0.1)
            fpga.write_int('rxslide',128)
            time.sleep(0.1)
            fpga.write_int('rxslide',0)

       print 'done aligning bytes \n'


    elif (opts.auto_align_bytes == 2):
       print 'Checking IQ Alignment... \n'
       ####################################

       time.sleep(0.1)
       ####################################
       ####################################
       tsleep=0#0.001

       loop_acc = 0

       usb0_mag1 = [256]
       usb1_mag1 = [256]
       usb2_mag1 = [256]
       usb3_mag1 = [256]
       usb4_mag1 = [256]
       usb5_mag1 = [256]
       usb6_mag1 = [256]
       usb7_mag1 = [256]

       lsb0_mag1 = [256]
       lsb1_mag1 = [256]
       lsb2_mag1 = [256]
       lsb3_mag1 = [256]
       lsb4_mag1 = [256]
       lsb5_mag1 = [256]
       lsb6_mag1 = [256]
       lsb7_mag1 = [256]

       start = time.time()

       while (loop_acc < 10):
            loop_acc = loop_acc+1
            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(0,0)
            usb0_mag_temp, lsb0_mag_temp, usb1_mag_temp, lsb1_mag_temp = treat_data_sbs(0,0)
            
            time.sleep(tsleep)

            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(2,1)
            usb2_mag_temp, lsb2_mag_temp, usb3_mag_temp, lsb3_mag_temp = treat_data_sbs(2,1)

            time.sleep(tsleep)

            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(4,1)
            usb4_mag_temp, lsb4_mag_temp, usb5_mag_temp, lsb5_mag_temp = treat_data_sbs(4,1)

            time.sleep(tsleep)

            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(6,1)
            usb6_mag_temp, lsb6_mag_temp, usb7_mag_temp, lsb7_mag_temp = treat_data_sbs(6,1)

            time.sleep(tsleep)

            usb0_mag1 = (np.asarray(usb0_mag1)+np.asarray(usb0_mag_temp))
            usb1_mag1 = (np.asarray(usb1_mag1)+np.asarray(usb1_mag_temp))
            usb2_mag1 = (np.asarray(usb2_mag1)+np.asarray(usb2_mag_temp))
            usb3_mag1 = (np.asarray(usb3_mag1)+np.asarray(usb3_mag_temp))
            usb4_mag1 = (np.asarray(usb4_mag1)+np.asarray(usb4_mag_temp))
            usb5_mag1 = (np.asarray(usb5_mag1)+np.asarray(usb5_mag_temp))
            usb6_mag1 = (np.asarray(usb6_mag1)+np.asarray(usb6_mag_temp))
            usb7_mag1 = (np.asarray(usb7_mag1)+np.asarray(usb7_mag_temp))

            lsb0_mag1 = (np.asarray(lsb0_mag1)+np.asarray(lsb0_mag_temp))
            lsb1_mag1 = (np.asarray(lsb1_mag1)+np.asarray(lsb1_mag_temp))
            lsb2_mag1 = (np.asarray(lsb2_mag1)+np.asarray(lsb2_mag_temp))
            lsb3_mag1 = (np.asarray(lsb3_mag1)+np.asarray(lsb3_mag_temp))
            lsb4_mag1 = (np.asarray(lsb4_mag1)+np.asarray(lsb4_mag_temp))
            lsb5_mag1 = (np.asarray(lsb5_mag1)+np.asarray(lsb5_mag_temp))
            lsb6_mag1 = (np.asarray(lsb6_mag1)+np.asarray(lsb6_mag_temp))
            lsb7_mag1 = (np.asarray(lsb7_mag1)+np.asarray(lsb7_mag_temp))


       end = time.time()
       #print(' ')
       #print('loop runtime in minutes=')
       #print((end-start)/60)
       #print(' ')

       usb0_mag = []
       usb1_mag = []
       usb2_mag = []
       usb3_mag = []
       usb4_mag = []
       usb5_mag = []
       usb6_mag = []
       usb7_mag = []

       lsb0_mag = []
       lsb1_mag = []
       lsb2_mag = []
       lsb3_mag = []
       lsb4_mag = []
       lsb5_mag = []
       lsb6_mag = []
       lsb7_mag = []       

       for x in usb0_mag1:
            usb0_mag.append(x/loop_acc)

       for x in lsb0_mag1:
            lsb0_mag.append(x/loop_acc)

       for x in usb1_mag1:
            usb1_mag.append(x/loop_acc)

       for x in lsb1_mag1:
            lsb1_mag.append(x/loop_acc)

       for x in usb2_mag1:
            usb2_mag.append(x/loop_acc)

       for x in lsb2_mag1:
            lsb2_mag.append(x/loop_acc)

       for x in usb3_mag1:
            usb3_mag.append(x/loop_acc)

       for x in lsb3_mag1:
            lsb3_mag.append(x/loop_acc)

       for x in usb4_mag1:
            usb4_mag.append(x/loop_acc)

       for x in lsb4_mag1:
            lsb4_mag.append(x/loop_acc)

       for x in usb5_mag1:
            usb5_mag.append(x/loop_acc)

       for x in lsb5_mag1:
            lsb5_mag.append(x/loop_acc)

       for x in usb6_mag1:
            usb6_mag.append(x/loop_acc)

       for x in lsb6_mag1:
            lsb6_mag.append(x/loop_acc)

       for x in usb7_mag1:
            usb7_mag.append(x/loop_acc)

       for x in lsb7_mag1:
            lsb7_mag.append(x/loop_acc)

       usb0_flt_mag=0
       lsb0_flt_mag=0
       usb1_flt_mag=0
       lsb1_flt_mag=0
       usb2_flt_mag=0
       lsb2_flt_mag=0
       usb3_flt_mag=0
       lsb3_flt_mag=0
       usb4_flt_mag=0
       lsb4_flt_mag=0
       usb5_flt_mag=0
       lsb5_flt_mag=0
       usb6_flt_mag=0
       lsb6_flt_mag=0
       usb7_flt_mag=0
       lsb7_flt_mag=0

       iii=5
       while iii<38:
            usb0_flt_mag = usb0_flt_mag + usb0_mag[iii]
            lsb0_flt_mag = lsb0_flt_mag + lsb0_mag[iii]

            usb1_flt_mag = usb1_flt_mag + usb1_mag[iii]
            lsb1_flt_mag = lsb1_flt_mag + lsb1_mag[iii]

            usb2_flt_mag = usb2_flt_mag + usb2_mag[iii]
            lsb2_flt_mag = lsb2_flt_mag + lsb2_mag[iii]

            usb3_flt_mag = usb3_flt_mag + usb3_mag[iii]
            lsb3_flt_mag = lsb3_flt_mag + lsb3_mag[iii]

            usb4_flt_mag = usb4_flt_mag + usb4_mag[iii]
            lsb4_flt_mag = lsb4_flt_mag + lsb4_mag[iii]

            usb5_flt_mag = usb5_flt_mag + usb5_mag[iii]
            lsb5_flt_mag = lsb5_flt_mag + lsb5_mag[iii]

            usb6_flt_mag = usb6_flt_mag + usb6_mag[iii]
            lsb6_flt_mag = lsb6_flt_mag + lsb6_mag[iii]

            usb7_flt_mag = usb7_flt_mag + usb7_mag[iii]
            lsb7_flt_mag = lsb7_flt_mag + lsb7_mag[iii]

            iii = iii + 1

       #print('printing noise sums')
       #print(usb0_flt_mag)
       #print(lsb0_flt_mag)
       #print(' ')
       #print(usb1_flt_mag)
       #print(lsb1_flt_mag)
       #print(' ')
       #print(usb2_flt_mag)
       #print(lsb2_flt_mag)
       #print(' ')
       #print(usb3_flt_mag)
       #print(lsb3_flt_mag)
       #print(' ')
       #print(usb4_flt_mag)
       #print(lsb4_flt_mag)
       #print(' ')
       #print(usb5_flt_mag)
       #print(lsb5_flt_mag)
       #print(' ')
       #print(usb6_flt_mag)
       #print(lsb6_flt_mag)
       #print(' ')
       #print(usb7_flt_mag)
       #print(lsb7_flt_mag)
       #print(' ')

       x_max=512
       x_min=0
       y_max=600
       y_max1=6000
       y_min=0#100

       indexx=67
       indexfh=133
       #34=10MHz
       #100=30MHz
       #199=60MHz
       #51=15MHz

       sbs0_supressed = (np.asarray(usb0_mag)-np.asarray(lsb0_mag))
       sbs1_supressed = (np.asarray(usb1_mag)-np.asarray(lsb1_mag))
       sbs2_supressed = (np.asarray(usb2_mag)-np.asarray(lsb2_mag))
       sbs3_supressed = (np.asarray(usb3_mag)-np.asarray(lsb3_mag))
       sbs4_supressed = (np.asarray(usb4_mag)-np.asarray(lsb4_mag))
       sbs5_supressed = (np.asarray(usb5_mag)-np.asarray(lsb5_mag))
       sbs6_supressed = (np.asarray(usb6_mag)-np.asarray(lsb6_mag))
       sbs7_supressed = (np.asarray(usb7_mag)-np.asarray(lsb7_mag))

       #print(sum(sbs0_supressed)-sbs0_supressed[0]-sbs0_supressed[1])
       #print(sum(sbs1_supressed)-sbs1_supressed[0]-sbs1_supressed[1])
       #print(sum(sbs2_supressed)-sbs2_supressed[0]-sbs2_supressed[1])
       #print(sum(sbs3_supressed)-sbs3_supressed[0]-sbs3_supressed[1])
       #print(sum(sbs4_supressed)-sbs4_supressed[0]-sbs4_supressed[1])
       #print(sum(sbs5_supressed)-sbs5_supressed[0]-sbs5_supressed[1])
       #print(sum(sbs6_supressed)-sbs6_supressed[0]-sbs6_supressed[1])
       #print(sum(sbs7_supressed)-sbs7_supressed[0]-sbs7_supressed[1])

       usb0_filter_mag=0
       lsb0_filter_mag=0
       usb1_filter_mag=0
       lsb1_filter_mag=0
       usb2_filter_mag=0
       lsb2_filter_mag=0
       usb3_filter_mag=0
       lsb3_filter_mag=0
       usb4_filter_mag=0
       lsb4_filter_mag=0
       usb5_filter_mag=0
       lsb5_filter_mag=0
       usb6_filter_mag=0
       lsb6_filter_mag=0
       usb7_filter_mag=0
       lsb7_filter_mag=0

       suppressed0_filter_mag=0
       suppressed1_filter_mag=0
       suppressed2_filter_mag=0
       suppressed3_filter_mag=0
       suppressed4_filter_mag=0
       suppressed5_filter_mag=0
       suppressed6_filter_mag=0
       suppressed7_filter_mag=0


       loop_acc=110#75
       while (loop_acc<160):#125):
            loop_acc=loop_acc+1
            suppressed0_filter_mag = suppressed0_filter_mag + sbs0_supressed[loop_acc]
            suppressed1_filter_mag = suppressed1_filter_mag + sbs1_supressed[loop_acc]
            suppressed2_filter_mag = suppressed2_filter_mag + sbs2_supressed[loop_acc]
            suppressed3_filter_mag = suppressed3_filter_mag + sbs3_supressed[loop_acc]
            suppressed4_filter_mag = suppressed4_filter_mag + sbs4_supressed[loop_acc]
            suppressed5_filter_mag = suppressed5_filter_mag + sbs5_supressed[loop_acc]
            suppressed6_filter_mag = suppressed6_filter_mag + sbs6_supressed[loop_acc]
            suppressed7_filter_mag = suppressed7_filter_mag + sbs7_supressed[loop_acc]

            #usb0_filter_mag = usb0_filter_mag + usb0_mag[loop_acc]
            #lsb0_filter_mag = lsb0_filter_mag + lsb0_mag[loop_acc]
            #usb1_filter_mag = usb1_filter_mag + usb1_mag[loop_acc]
            #lsb1_filter_mag = lsb1_filter_mag + lsb1_mag[loop_acc]
            #usb2_filter_mag = usb2_filter_mag + usb2_mag[loop_acc]
            #lsb2_filter_mag = lsb2_filter_mag + lsb2_mag[loop_acc]
            #usb3_filter_mag = usb3_filter_mag + usb3_mag[loop_acc]
            #lsb3_filter_mag = lsb3_filter_mag + lsb3_mag[loop_acc]
            #usb4_filter_mag = usb4_filter_mag + usb4_mag[loop_acc]
            #lsb4_filter_mag = lsb4_filter_mag + lsb4_mag[loop_acc]
            #usb5_filter_mag = usb5_filter_mag + usb5_mag[loop_acc]
            #lsb5_filter_mag = lsb5_filter_mag + lsb5_mag[loop_acc]
            #usb6_filter_mag = usb6_filter_mag + usb6_mag[loop_acc]
            #lsb6_filter_mag = lsb6_filter_mag + lsb6_mag[loop_acc]
            #usb7_filter_mag = usb7_filter_mag + usb7_mag[loop_acc]
            #lsb7_filter_mag = lsb7_filter_mag + lsb7_mag[loop_acc]

       print(suppressed0_filter_mag)
       print(suppressed1_filter_mag)
       print(suppressed2_filter_mag)
       print(suppressed3_filter_mag)
       print(suppressed4_filter_mag)
       print(suppressed5_filter_mag)
       print(suppressed6_filter_mag)
       print(suppressed7_filter_mag)



       fpga_byte_flip = 255#0


       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)

       basey_base = 10

       lsb0_mag.reverse()
       lsb1_mag.reverse()
       lsb2_mag.reverse()
       lsb3_mag.reverse()
       lsb4_mag.reverse()
       lsb5_mag.reverse()
       lsb6_mag.reverse()
       lsb7_mag.reverse()

       #import matplotlib.pyplot as plt
       #plt.subplot(8,1,1)
       #plt.plot(lsb0_mag+usb0_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,2)
       #plt.plot(lsb1_mag+usb1_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,3)
       #plt.plot(lsb2_mag+usb2_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,4)
       #plt.plot(lsb3_mag+usb3_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,5)
       #plt.plot(lsb4_mag+usb4_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,6)
       #plt.plot(lsb5_mag+usb5_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,7)
       #plt.plot(lsb6_mag+usb6_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,8)
       #plt.plot(lsb7_mag+usb7_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.show()

       #plt.subplot(8,1,1)
       #plt.plot(lsb0_mag+usb0_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,2)
       #plt.plot(lsb1_mag+usb1_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,3)
       #plt.plot(lsb2_mag+usb2_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,4)
       #plt.plot(lsb3_mag+usb3_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,5)
       #plt.plot(lsb4_mag+usb4_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,6)
       #plt.plot(lsb5_mag+usb5_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,7)
       #plt.plot(lsb6_mag+usb6_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,8)
       #plt.plot(lsb7_mag+usb7_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.show()

       #Plot the "after" results

       tsleep=0#0.001

       loop_acc = 0

       usb0_mag1 = [256]
       usb1_mag1 = [256]
       usb2_mag1 = [256]
       usb3_mag1 = [256]
       usb4_mag1 = [256]
       usb5_mag1 = [256]
       usb6_mag1 = [256]
       usb7_mag1 = [256]

       lsb0_mag1 = [256]
       lsb1_mag1 = [256]
       lsb2_mag1 = [256]
       lsb3_mag1 = [256]
       lsb4_mag1 = [256]
       lsb5_mag1 = [256]
       lsb6_mag1 = [256]
       lsb7_mag1 = [256]

       start = time.time()

       while (loop_acc < 10):
            loop_acc = loop_acc+1
            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(0,0)
            usb0_mag_temp, lsb0_mag_temp, usb1_mag_temp, lsb1_mag_temp = treat_data_sbs(0,0)
            
            time.sleep(tsleep)

            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(2,1)
            usb2_mag_temp, lsb2_mag_temp, usb3_mag_temp, lsb3_mag_temp = treat_data_sbs(2,1)

            time.sleep(tsleep)

            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(4,1)
            usb4_mag_temp, lsb4_mag_temp, usb5_mag_temp, lsb5_mag_temp = treat_data_sbs(4,1)

            time.sleep(tsleep)

            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(6,1)
            usb6_mag_temp, lsb6_mag_temp, usb7_mag_temp, lsb7_mag_temp = treat_data_sbs(6,1)

            time.sleep(tsleep)

            usb0_mag1 = (np.asarray(usb0_mag1)+np.asarray(usb0_mag_temp))
            usb1_mag1 = (np.asarray(usb1_mag1)+np.asarray(usb1_mag_temp))
            usb2_mag1 = (np.asarray(usb2_mag1)+np.asarray(usb2_mag_temp))
            usb3_mag1 = (np.asarray(usb3_mag1)+np.asarray(usb3_mag_temp))
            usb4_mag1 = (np.asarray(usb4_mag1)+np.asarray(usb4_mag_temp))
            usb5_mag1 = (np.asarray(usb5_mag1)+np.asarray(usb5_mag_temp))
            usb6_mag1 = (np.asarray(usb6_mag1)+np.asarray(usb6_mag_temp))
            usb7_mag1 = (np.asarray(usb7_mag1)+np.asarray(usb7_mag_temp))

            lsb0_mag1 = (np.asarray(lsb0_mag1)+np.asarray(lsb0_mag_temp))
            lsb1_mag1 = (np.asarray(lsb1_mag1)+np.asarray(lsb1_mag_temp))
            lsb2_mag1 = (np.asarray(lsb2_mag1)+np.asarray(lsb2_mag_temp))
            lsb3_mag1 = (np.asarray(lsb3_mag1)+np.asarray(lsb3_mag_temp))
            lsb4_mag1 = (np.asarray(lsb4_mag1)+np.asarray(lsb4_mag_temp))
            lsb5_mag1 = (np.asarray(lsb5_mag1)+np.asarray(lsb5_mag_temp))
            lsb6_mag1 = (np.asarray(lsb6_mag1)+np.asarray(lsb6_mag_temp))
            lsb7_mag1 = (np.asarray(lsb7_mag1)+np.asarray(lsb7_mag_temp))


       end = time.time()
   
       usb0_mag = []
       usb1_mag = []
       usb2_mag = []
       usb3_mag = []
       usb4_mag = []
       usb5_mag = []
       usb6_mag = []
       usb7_mag = []

       lsb0_mag = []
       lsb1_mag = []
       lsb2_mag = []
       lsb3_mag = []
       lsb4_mag = []
       lsb5_mag = []
       lsb6_mag = []
       lsb7_mag = []       

       for x in usb0_mag1:
            usb0_mag.append(x/loop_acc)

       for x in lsb0_mag1:
            lsb0_mag.append(x/loop_acc)

       for x in usb1_mag1:
            usb1_mag.append(x/loop_acc)

       for x in lsb1_mag1:
            lsb1_mag.append(x/loop_acc)

       for x in usb2_mag1:
            usb2_mag.append(x/loop_acc)

       for x in lsb2_mag1:
            lsb2_mag.append(x/loop_acc)

       for x in usb3_mag1:
            usb3_mag.append(x/loop_acc)

       for x in lsb3_mag1:
            lsb3_mag.append(x/loop_acc)

       for x in usb4_mag1:
            usb4_mag.append(x/loop_acc)

       for x in lsb4_mag1:
            lsb4_mag.append(x/loop_acc)

       for x in usb5_mag1:
            usb5_mag.append(x/loop_acc)

       for x in lsb5_mag1:
            lsb5_mag.append(x/loop_acc)

       for x in usb6_mag1:
            usb6_mag.append(x/loop_acc)

       for x in lsb6_mag1:
            lsb6_mag.append(x/loop_acc)

       for x in usb7_mag1:
            usb7_mag.append(x/loop_acc)

       for x in lsb7_mag1:
            lsb7_mag.append(x/loop_acc)

       usb0_flt_mag=0
       lsb0_flt_mag=0
       usb1_flt_mag=0
       lsb1_flt_mag=0
       usb2_flt_mag=0
       lsb2_flt_mag=0
       usb3_flt_mag=0
       lsb3_flt_mag=0
       usb4_flt_mag=0
       lsb4_flt_mag=0
       usb5_flt_mag=0
       lsb5_flt_mag=0
       usb6_flt_mag=0
       lsb6_flt_mag=0
       usb7_flt_mag=0
       lsb7_flt_mag=0

       iii=5
       while iii<38:
            usb0_flt_mag = usb0_flt_mag + usb0_mag[iii]
            lsb0_flt_mag = lsb0_flt_mag + lsb0_mag[iii]

            usb1_flt_mag = usb1_flt_mag + usb1_mag[iii]
            lsb1_flt_mag = lsb1_flt_mag + lsb1_mag[iii]

            usb2_flt_mag = usb2_flt_mag + usb2_mag[iii]
            lsb2_flt_mag = lsb2_flt_mag + lsb2_mag[iii]

            usb3_flt_mag = usb3_flt_mag + usb3_mag[iii]
            lsb3_flt_mag = lsb3_flt_mag + lsb3_mag[iii]

            usb4_flt_mag = usb4_flt_mag + usb4_mag[iii]
            lsb4_flt_mag = lsb4_flt_mag + lsb4_mag[iii]

            usb5_flt_mag = usb5_flt_mag + usb5_mag[iii]
            lsb5_flt_mag = lsb5_flt_mag + lsb5_mag[iii]

            usb6_flt_mag = usb6_flt_mag + usb6_mag[iii]
            lsb6_flt_mag = lsb6_flt_mag + lsb6_mag[iii]

            usb7_flt_mag = usb7_flt_mag + usb7_mag[iii]
            lsb7_flt_mag = lsb7_flt_mag + lsb7_mag[iii]

            iii = iii + 1

       x_max=512
       x_min=0
       y_max=600#6000#500
       y_max1=5000
       y_min=0#100

       indexx=67
       indexfh=133


       sbs0_supressed = (np.asarray(usb0_mag)-np.asarray(lsb0_mag))
       sbs1_supressed = (np.asarray(usb1_mag)-np.asarray(lsb1_mag))
       sbs2_supressed = (np.asarray(usb2_mag)-np.asarray(lsb2_mag))
       sbs3_supressed = (np.asarray(usb3_mag)-np.asarray(lsb3_mag))
       sbs4_supressed = (np.asarray(usb4_mag)-np.asarray(lsb4_mag))
       sbs5_supressed = (np.asarray(usb5_mag)-np.asarray(lsb5_mag))
       sbs6_supressed = (np.asarray(usb6_mag)-np.asarray(lsb6_mag))
       sbs7_supressed = (np.asarray(usb7_mag)-np.asarray(lsb7_mag))

       usb0_filter_mag=0
       lsb0_filter_mag=0
       usb1_filter_mag=0
       lsb1_filter_mag=0
       usb2_filter_mag=0
       lsb2_filter_mag=0
       usb3_filter_mag=0
       lsb3_filter_mag=0
       usb4_filter_mag=0
       lsb4_filter_mag=0
       usb5_filter_mag=0
       lsb5_filter_mag=0
       usb6_filter_mag=0
       lsb6_filter_mag=0
       usb7_filter_mag=0
       lsb7_filter_mag=0

       suppressed0_filter_mag_after=0
       suppressed1_filter_mag_after=0
       suppressed2_filter_mag_after=0
       suppressed3_filter_mag_after=0
       suppressed4_filter_mag_after=0
       suppressed5_filter_mag_after=0
       suppressed6_filter_mag_after=0
       suppressed7_filter_mag_after=0


       loop_acc=75
       while (loop_acc<125):
            loop_acc=loop_acc+1
            suppressed0_filter_mag_after = suppressed0_filter_mag_after + sbs0_supressed[loop_acc]
            suppressed1_filter_mag_after = suppressed1_filter_mag_after + sbs1_supressed[loop_acc]
            suppressed2_filter_mag_after = suppressed2_filter_mag_after + sbs2_supressed[loop_acc]
            suppressed3_filter_mag_after = suppressed3_filter_mag_after + sbs3_supressed[loop_acc]
            suppressed4_filter_mag_after = suppressed4_filter_mag_after + sbs4_supressed[loop_acc]
            suppressed5_filter_mag_after = suppressed5_filter_mag_after + sbs5_supressed[loop_acc]
            suppressed6_filter_mag_after = suppressed6_filter_mag_after + sbs6_supressed[loop_acc]
            suppressed7_filter_mag_after = suppressed7_filter_mag_after + sbs7_supressed[loop_acc]

       print('before/after channel 0')
       print(suppressed0_filter_mag)
       print(suppressed0_filter_mag_after)

       print('before/after channel 1')
       print(suppressed1_filter_mag)
       print(suppressed1_filter_mag_after)

       print('before/after channel 2')
       print(suppressed2_filter_mag)
       print(suppressed2_filter_mag_after)

       print('before/after channel 3')
       print(suppressed3_filter_mag)
       print(suppressed3_filter_mag_after)

       print('before/after channel 4')
       print(suppressed4_filter_mag)
       print(suppressed4_filter_mag_after)

       print('before/after channel 5')
       print(suppressed5_filter_mag)
       print(suppressed5_filter_mag_after)

       print('before/after channel 6')
       print(suppressed6_filter_mag)
       print(suppressed6_filter_mag_after)

       print('before/after channel 7')
       print(suppressed7_filter_mag)
       print(suppressed7_filter_mag_after)






       suppression_threshold=5000
       fpga_byte_flip=0
       if (suppressed0_filter_mag>suppressed0_filter_mag_after):
            fpga_byte_flip = fpga_byte_flip+1
            print('chan 0 is unaligned')
            print(' ')

       if (suppressed1_filter_mag>suppressed1_filter_mag_after):
            fpga_byte_flip = fpga_byte_flip+2
            print('chan 1 is unaligned')
            print(' ')

       if (suppressed2_filter_mag>suppressed2_filter_mag_after):
            fpga_byte_flip = fpga_byte_flip+4
            print('chan 2 is unaligned')
            print(' ')

       if (suppressed3_filter_mag>suppressed3_filter_mag_after):
            fpga_byte_flip = fpga_byte_flip+8
            print('chan 3 is unaligned')
            print(' ')

       if (suppressed4_filter_mag>suppressed4_filter_mag_after):
            fpga_byte_flip = fpga_byte_flip+16
            print('chan 4 is unaligned')
            print(' ')

       if (suppressed5_filter_mag>suppressed5_filter_mag_after):
            fpga_byte_flip = fpga_byte_flip+32
            print('chan 5 is unaligned')
            print(' ')

       if (suppressed6_filter_mag>suppressed6_filter_mag_after):
            fpga_byte_flip = fpga_byte_flip+64
            print('chan 6 is unaligned')
            print(' ')

       if (suppressed7_filter_mag>suppressed7_filter_mag_after):
            fpga_byte_flip = fpga_byte_flip+128
            print('chan 7 is unaligned')
            print(' ')

       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)
       fpga.write_int('rxslide', fpga_byte_flip)
       fpga.write_int('rxslide', 0)


       basey_base = 10

       lsb0_mag.reverse()
       lsb1_mag.reverse()
       lsb2_mag.reverse()
       lsb3_mag.reverse()
       lsb4_mag.reverse()
       lsb5_mag.reverse()
       lsb6_mag.reverse()
       lsb7_mag.reverse()


       tsleep=0#0.001

       loop_acc = 0

       usb0_mag1 = [256]
       usb1_mag1 = [256]
       usb2_mag1 = [256]
       usb3_mag1 = [256]
       usb4_mag1 = [256]
       usb5_mag1 = [256]
       usb6_mag1 = [256]
       usb7_mag1 = [256]

       lsb0_mag1 = [256]
       lsb1_mag1 = [256]
       lsb2_mag1 = [256]
       lsb3_mag1 = [256]
       lsb4_mag1 = [256]
       lsb5_mag1 = [256]
       lsb6_mag1 = [256]
       lsb7_mag1 = [256]

       start = time.time()

       while (loop_acc < 10):
            loop_acc = loop_acc+1
            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(0,0)
            usb0_mag_temp, lsb0_mag_temp, usb1_mag_temp, lsb1_mag_temp = treat_data_sbs(0,0)
            
            time.sleep(tsleep)

            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(2,1)
            usb2_mag_temp, lsb2_mag_temp, usb3_mag_temp, lsb3_mag_temp = treat_data_sbs(2,1)

            time.sleep(tsleep)

            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(4,1)
            usb4_mag_temp, lsb4_mag_temp, usb5_mag_temp, lsb5_mag_temp = treat_data_sbs(4,1)

            time.sleep(tsleep)

            usb_r_temp, usb_i_temp, lsb_r_temp, lsb_i_temp=get_data_sbs(6,1)
            usb6_mag_temp, lsb6_mag_temp, usb7_mag_temp, lsb7_mag_temp = treat_data_sbs(6,1)

            time.sleep(tsleep)

            usb0_mag1 = (np.asarray(usb0_mag1)+np.asarray(usb0_mag_temp))
            usb1_mag1 = (np.asarray(usb1_mag1)+np.asarray(usb1_mag_temp))
            usb2_mag1 = (np.asarray(usb2_mag1)+np.asarray(usb2_mag_temp))
            usb3_mag1 = (np.asarray(usb3_mag1)+np.asarray(usb3_mag_temp))
            usb4_mag1 = (np.asarray(usb4_mag1)+np.asarray(usb4_mag_temp))
            usb5_mag1 = (np.asarray(usb5_mag1)+np.asarray(usb5_mag_temp))
            usb6_mag1 = (np.asarray(usb6_mag1)+np.asarray(usb6_mag_temp))
            usb7_mag1 = (np.asarray(usb7_mag1)+np.asarray(usb7_mag_temp))

            lsb0_mag1 = (np.asarray(lsb0_mag1)+np.asarray(lsb0_mag_temp))
            lsb1_mag1 = (np.asarray(lsb1_mag1)+np.asarray(lsb1_mag_temp))
            lsb2_mag1 = (np.asarray(lsb2_mag1)+np.asarray(lsb2_mag_temp))
            lsb3_mag1 = (np.asarray(lsb3_mag1)+np.asarray(lsb3_mag_temp))
            lsb4_mag1 = (np.asarray(lsb4_mag1)+np.asarray(lsb4_mag_temp))
            lsb5_mag1 = (np.asarray(lsb5_mag1)+np.asarray(lsb5_mag_temp))
            lsb6_mag1 = (np.asarray(lsb6_mag1)+np.asarray(lsb6_mag_temp))
            lsb7_mag1 = (np.asarray(lsb7_mag1)+np.asarray(lsb7_mag_temp))


       end = time.time()
   
       usb0_mag = []
       usb1_mag = []
       usb2_mag = []
       usb3_mag = []
       usb4_mag = []
       usb5_mag = []
       usb6_mag = []
       usb7_mag = []

       lsb0_mag = []
       lsb1_mag = []
       lsb2_mag = []
       lsb3_mag = []
       lsb4_mag = []
       lsb5_mag = []
       lsb6_mag = []
       lsb7_mag = []       

       for x in usb0_mag1:
            usb0_mag.append(x/loop_acc)

       for x in lsb0_mag1:
            lsb0_mag.append(x/loop_acc)

       for x in usb1_mag1:
            usb1_mag.append(x/loop_acc)

       for x in lsb1_mag1:
            lsb1_mag.append(x/loop_acc)

       for x in usb2_mag1:
            usb2_mag.append(x/loop_acc)

       for x in lsb2_mag1:
            lsb2_mag.append(x/loop_acc)

       for x in usb3_mag1:
            usb3_mag.append(x/loop_acc)

       for x in lsb3_mag1:
            lsb3_mag.append(x/loop_acc)

       for x in usb4_mag1:
            usb4_mag.append(x/loop_acc)

       for x in lsb4_mag1:
            lsb4_mag.append(x/loop_acc)

       for x in usb5_mag1:
            usb5_mag.append(x/loop_acc)

       for x in lsb5_mag1:
            lsb5_mag.append(x/loop_acc)

       for x in usb6_mag1:
            usb6_mag.append(x/loop_acc)

       for x in lsb6_mag1:
            lsb6_mag.append(x/loop_acc)

       for x in usb7_mag1:
            usb7_mag.append(x/loop_acc)

       for x in lsb7_mag1:
            lsb7_mag.append(x/loop_acc)

       usb0_flt_mag=0
       lsb0_flt_mag=0
       usb1_flt_mag=0
       lsb1_flt_mag=0
       usb2_flt_mag=0
       lsb2_flt_mag=0
       usb3_flt_mag=0
       lsb3_flt_mag=0
       usb4_flt_mag=0
       lsb4_flt_mag=0
       usb5_flt_mag=0
       lsb5_flt_mag=0
       usb6_flt_mag=0
       lsb6_flt_mag=0
       usb7_flt_mag=0
       lsb7_flt_mag=0

       iii=5
       while iii<38:
            usb0_flt_mag = usb0_flt_mag + usb0_mag[iii]
            lsb0_flt_mag = lsb0_flt_mag + lsb0_mag[iii]

            usb1_flt_mag = usb1_flt_mag + usb1_mag[iii]
            lsb1_flt_mag = lsb1_flt_mag + lsb1_mag[iii]

            usb2_flt_mag = usb2_flt_mag + usb2_mag[iii]
            lsb2_flt_mag = lsb2_flt_mag + lsb2_mag[iii]

            usb3_flt_mag = usb3_flt_mag + usb3_mag[iii]
            lsb3_flt_mag = lsb3_flt_mag + lsb3_mag[iii]

            usb4_flt_mag = usb4_flt_mag + usb4_mag[iii]
            lsb4_flt_mag = lsb4_flt_mag + lsb4_mag[iii]

            usb5_flt_mag = usb5_flt_mag + usb5_mag[iii]
            lsb5_flt_mag = lsb5_flt_mag + lsb5_mag[iii]

            usb6_flt_mag = usb6_flt_mag + usb6_mag[iii]
            lsb6_flt_mag = lsb6_flt_mag + lsb6_mag[iii]

            usb7_flt_mag = usb7_flt_mag + usb7_mag[iii]
            lsb7_flt_mag = lsb7_flt_mag + lsb7_mag[iii]

            iii = iii + 1

       x_max=512
       x_min=0
       y_max=600#6000#500
       y_max1=5000
       y_min=0#100

       indexx=67
       indexfh=133

        
       lsb0_mag.reverse()
       lsb1_mag.reverse()
       lsb2_mag.reverse()
       lsb3_mag.reverse()
       lsb4_mag.reverse()
       lsb5_mag.reverse()
       lsb6_mag.reverse()
       lsb7_mag.reverse()


       #import matplotlib.pyplot as plt
       #plt.subplot(8,1,1)
       #plt.plot(lsb0_mag+usb0_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,2)
       #plt.plot(lsb1_mag+usb1_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,3)
       #plt.plot(lsb2_mag+usb2_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,4)
       #plt.plot(lsb3_mag+usb3_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,5)
       #plt.plot(lsb4_mag+usb4_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,6)
       #plt.plot(lsb5_mag+usb5_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,7)
       #plt.plot(lsb6_mag+usb6_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.subplot(8,1,8)
       #plt.plot(lsb7_mag+usb7_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max])

       #plt.show()



       #plt.subplot(8,1,1)
       #plt.plot(lsb0_mag+usb0_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,2)
       #plt.plot(lsb1_mag+usb1_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,3)
       #plt.plot(lsb2_mag+usb2_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,4)
       #plt.plot(lsb3_mag+usb3_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,5)
       #plt.plot(lsb4_mag+usb4_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,6)
       #plt.plot(lsb5_mag+usb5_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,7)
       #plt.plot(lsb6_mag+usb6_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.subplot(8,1,8)
       #plt.plot(lsb7_mag+usb7_mag, color = 'black')
       #plt.axis([x_min, x_max, y_min, y_max1])

       #plt.show()
       
       #line ends here
        



    if (opts.swap_bytes_ch != -1):
       print 'Swapping bytes on specified channel/s'
      
       #while(hex(fpga.read_int('msb2_loc')) != 0x80):
       #   print 'in while'
       #   swap_byte(fpga,opts.swap_bytes_ch)      
       #print 'done'
 
    print 'Writing fft_shift ..... \n'
    fpga.write_int('fft_shift',opts.fft_shift)    #Don't know where this number comes from but its like this in the tut3 example as well 
    print 'done \n'
    
    print 'Writing quantization gain .... \n'
    #fpga.write_int('quant_gain',0x0000000a)
    fpga.write_int('quant_gain',0x00000014)
    print 'done \n'




##########################
#######################
#########luke

    if (opts.pack):
       #resets the GbE cores
       print 'Resetting the GbE cores on: %s' %(roach) 
       fpga.write_int('part2_gbe_rst_core',0)
       time.sleep(1)
       fpga.write_int('part2_gbe_rst_core',1)
       time.sleep(1)
       fpga.write_int('part2_gbe_rst_core',0)
       time.sleep(1)
       print 'done. \n'
       
      # Configure 10 GbE cores
       print 'Configuring 10 GbE interfaces on: %s.\n'%(roach) 
       fpga.config_10gbe_core('part2_gbe0', mac_base0, source_ip0, source_port, hpc_macs_list)
       fpga.config_10gbe_core('part2_gbe1', mac_base1, source_ip1, source_port, hpc_macs_list)
       fpga.config_10gbe_core('part2_gbe2', mac_base2, source_ip2, source_port, hpc_macs_list)
       fpga.config_10gbe_core('part2_gbe3', mac_base3, source_ip3, source_port, hpc_macs_list)
       print 'done. \n'
       print '------------------------'

       #set f_id
       print 'Write f engine fid... '  
       fpga.write_int('part2_f_id', opts.fid)
       time.sleep(0.5)
       print 'done. \n'
       
       #set x_id's
       print 'Write x engine id... '
       for i in range(20):
          fpga.write_int('part2_x_id'+str(i),i)
       time.sleep(0.5)
       print 'done. \n'
      
       #set destination port 
       print 'Writing destination port number: %d' %opts.dport
       fpga.write_int('part2_x_port', opts.dport)
       time.sleep(0.5)
       print 'done \n'
      
    #Set the destination IP addresses 
    if (opts.xid != -1):
       for i in range(20): 
          fpga.write_int('part2_x_ip'+str(i), blackhole)

       # The following 11 lines of code can be upgraded to case statements
       if  (opts.dcomp=='paf0' ):
          print 'Send subbands %d to paf0 and subsequent 4 xids to flag3' %opts.xid    
          fpga.write_int('part2_x_ip'+str(opts.xid)  ,paf0   )
          fpga.write_int('part2_x_ip'+str(opts.xid+1),flag3_0)
          fpga.write_int('part2_x_ip'+str(opts.xid+2),flag3_1)
          fpga.write_int('part2_x_ip'+str(opts.xid+3),flag3_2)
          fpga.write_int('part2_x_ip'+str(opts.xid+4),flag3_3)
       elif(opts.dcomp=='south'):
          print 'Send subbands %d to south' %opts.xid    
          fpga.write_int('part2_x_ip'+str(opts.xid),south)
       elif(opts.dcomp=='west' ):
          print 'Send subbands %d to west' %opts.xid    
          fpga.write_int('part2_x_ip'+str(opts.xid),west )
       elif(opts.dcomp=='tofu' ):
          print 'Send subbands %d to tofu' %opts.xid    
          fpga.write_int('part2_x_ip'+str(opts.xid),tofu )
       else :
          print("Unknown destination computer therefore sending packets to the BlAcKhOlE\n");
          fpga.write_int('part2_x_ip'+str(opts.xid),blackhole)

       print '\n done'


    if (opts.arm):
       print 'Sending sync pulse \n'
       fpga.write_int('ARM',1)
       fpga.write_int('ARM',0)
       print 'done ... \n'

    #if (opts.sbs_data):
    #   print 'Enabling output snapshots blocks \n'
    #   for k in range(4):
    #      fpga.write_int('lsb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
    #      fpga.write_int('lsb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,0)
    #      fpga.write_int('usb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
    #      fpga.write_int('usb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
# 
#       for k in range(4):
#          fpga.write_int('lsb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,1) 
#          fpga.write_int('lsb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,1)
#          fpga.write_int('usb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,1) 
#          fpga.write_int('usb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,1) ##

       #for k in range(4):
       #   fpga.write_int('lsb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
       #   fpga.write_int('lsb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,0)
       #   fpga.write_int('usb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
       #   fpga.write_int('usb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
       
       #time.sleep(0.5)
       #get_data_sbs(0)
       #get_data_sbs(2)
       #get_data_sbs(4)
       #get_data_sbs(6)
       #print 'done \n'
    #luke added

#Luke commented out
    if (opts.time_data):
       print 'Reseting time domain brams'
       fpga.write_int('bram_we' ,0)
       fpga.write_int('bram_rst',1)
       fpga.write_int('bram_rst',0)
       fpga.write_int('bram_we' ,1)
       time.sleep(2)
       for i in range(8):
         get_data_iq(i)
       print 'done \n'

except KeyboardInterrupt:
  exit_clean()
except:
  exit_fail()

exit_clean()
