#! /usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
from time import sleep


# List of tuples

try:
    from corr import katcp_wrapper as kcp

    fpgas = {
        "A": kcp.FpgaClient("flagr2-1", 7147),
        "B": kcp.FpgaClient("flagr2-2", 7147),
        "C": kcp.FpgaClient("flagr2-3", 7147),
        "D": kcp.FpgaClient("flagr2-4", 7147),
        "E": kcp.FpgaClient("flagr2-5", 7147),
    }
except Exception:
    print("Warning: Could not import corr!")
    fpgas = {
        "A": None,
        "B": None,
        "C": None,
        "D": None,
        "E": None,
    }

channel_to_slide_val = {1: 1, 2: 2, 3: 4, 4: 8, 5: 16, 6: 32, 7: 64, 8: 128}


def parse_channel_coords(string):
    if len(string) != 2:
        raise ValueError("Invalid command given: {}".format(string))

    roach = string[0].upper()
    if not "A" <= roach <= "E":
        raise ValueError("Invalid roach id: {}; should be A-E".format(roach))
    channel = int(string[1])
    if not 1 <= channel <= 8:
        raise ValueError("Invalid channel id: {}; should be 1-8".format(channel))
    return (roach, channel)


def get_channels_to_fix():
    channels_to_fix = []
    while True:
        response = raw_input("Input another roach/channel (e.g. A1), or 'stop' to exit")
        if response == "stop":
            break
        try:
            channel_coords = parse_channel_coords(response)
        except ValueError as error:
            print("Failed to parse; try again: {}".format(error))
        else:
            channels_to_fix.append(channel_coords)
    return channels_to_fix

def zero_all_channel_delays(commit=False):
    for roach_id, roach in fpgas.items():
        print("Processing roach {}".format(roach_id))
        for channel in range(8):
            if commit:
                roach.write_int("{}iq_dly_reg".format(channel), 0)
            else:
                print('{}write_int("iq_dly_reg", 0)'.format(channel))

def do_byte_lock(channels_to_fix, commit=False):
    for __ in range(8):
        for roach, channel in channels_to_fix:
            print("Processing {}{}...".format(roach, channel))
            if commit:
                fpgas[roach].write_int("rxslide", channel_to_slide_val[channel])
                sleep(0.1)
                fpgas[roach].write_int("rxslide", 0)
            else:
                print(
                    "DRY RUN: write_int('rxslide', {})".format(
                        channel_to_slide_val[channel]
                    )
                )
                print("sleep(0.1)")
                print("DRY RUN: write_int('rxslide', 0)")
            print("...done")


def main():
    args = parse_args()

    if args.zero_delays:
        zero_all_channel_delays(args.commit)
        return 

    channels_to_fix = [parse_channel_coords(coord_str) for coord_str in args.coords]
    print("Got channel coords: {}".format(channels_to_fix))

    do_byte_lock(channels_to_fix, args.commit)


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "coords",
        nargs="*",
        help="Roach/channel coords, in the format A1. Enter any number of coords.",
    )
    parser.add_argument("-z", "--zero-delays", action="store_true")
    parser.add_argument("--commit", action="store_true")
    return parser.parse_args()


if __name__ == "__main__":
    main()
