import time, struct

from corr import katcp_wrapper

from helper_functions import _ip_string_to_int
from helper_functions import get_data_sbs, treat_data_sbs

from numpy import genfromtxt
import numpy as np

config_debug = True
lo = '1450'

hasConfigured = False

with open('RC.txt', 'r') as f:
    for val in f:
        global hasConfigured
        num = int(val)
        print "RD: config num ", num
        if num == 0:
            hasConfigured = False
        else:
            hasConfigured = True
        print "RD: prgm=", hasConfigured


class RoachDoctor():

    def __init__(self, roach_host_list, dibas_info):

        # init class variables
        self.roaches = [] # fpga client to roaches
        self.ip_addrs = []
        self.blackhole = None
        self.mac_list = [0xFFFFFFFFFFFF]*256 # init a list of 256 locations

        self.bof_file    = dibas_info['bof_file']
        self.backend     = dibas_info['backend']
        self.source_port = dibas_info['source_port']
        self.dest_port   = dibas_info['dest_port']
        self.dest_comp   = dibas_info['dest_comp']
        
        self.bb_lock_data_dir = "./f_engine_config/bb_lock_data/"

        # set up the ARP table
        for hpc in self.dest_comp:
            addrs = dibas_info[hpc]['10GbE_interfaces']
            for ip in sorted(addrs.keys()):

                # set the blackhole ip if present, otherwise log valid ip destinations.
                # I could think of another way to implement this... not sure I like looking just for a blackhole hpc....maybe a defulat option in dibas?
                if hpc == "blackhole":
                    self.blackhole = ip
                else:
                    self.ip_addrs.append(ip)

                mac = addrs[ip]
                loc = _ip_string_to_int(ip) & 0xFF
                self.mac_list[loc] = mac
        self.ip_addrs = sorted(self.ip_addrs)

        # add a fpga client for the roaches
        for idx in range(0, len(roach_host_list)):
            roach_name = roach_host_list[idx]

            roach = katcp_wrapper.FpgaClient(roach_name)
            time.sleep(.2)
            if roach.is_connected():
                print("ConfigFE: Adding %s" % roach_name)
                self.roaches.append(roach)
            else:
                print("ConfigFE: Could not connect to %s" % roach_name)

    def configure(self, bit_lock=False, byte_lock=False, byte_lock_check=False, fft_shift=False, quan_gain=False):

        global hasConfigured

        for i in range(0, len(self.roaches)):

            roach = self.roaches[i]
            fid = i

            if config_debug:
                print ""
                print "ConfigFE: *** Configuring %s ***" % roach.host
                print "ConfigFE: FID %d..." % fid

            if not hasConfigured:
                self.prog_dev(fpga=roach)
                self.net_config(fpga=roach, fid=fid)
                self.set_coeff(fpga=roach, fid=fid, bankPrefixList=['A', 'B', 'C', 'D', 'E'])
                self.set_safety(fpga=roach)

            if bit_lock:
                self.bit_lock(fpga=roach)
            if byte_lock:
                self.byte_lock(fpga=roach, check=byte_lock_check)
            if fft_shift:
                self.write_fft_shift(fpga=roach, shift=fft_shift)
            if quan_gain:
                self.write_quantization_gain(fpga=roach)

        hasConfigured = True

    def get_roaches(self):
        return self.roaches

    def prog_dev(self, fpga):
        if config_debug:
            print "ConfigFE: Issuing Progdev..."
            print "ConfigFE:     " + str(self.bof_file)
        fpga.progdev(str(self.bof_file))
        time.sleep(.5)

    def net_config(self, fpga, fid):
        # resets the GbE cores
        if config_debug:
            print "ConfigFE: Resetting the GbE cores..."
        fpga.write_int('part1_gbe_rst_core', 0)
        time.sleep(.3)
        fpga.write_int('part1_gbe_rst_core', 1)
        time.sleep(.3)
        fpga.write_int('part1_gbe_rst_core', 0)
        time.sleep(.3)

        # Configure 10 GbE cores
        if config_debug:
            print "ConfigFE: Configuring 10 GbE interfaces..."
        # give each physical port a unique id
        baseMac = (2 << 40) + (2 << 32) + (10 << 24) + (17 << 16) + (16 << 8)
        mac_base0 = baseMac + (4 * fid + 230)
        mac_base1 = baseMac + (4 * fid + 231)
        mac_base2 = baseMac + (4 * fid + 232)
        mac_base3 = baseMac + (4 * fid + 233)

        baseIp = 10 * (2 ** 24) + 17 * (2 ** 16) + 16 * (2 ** 8)
        source_ip0 = baseIp + 230 + (4 * fid)
        source_ip1 = baseIp + 231 + (4 * fid)
        source_ip2 = baseIp + 232 + (4 * fid)
        source_ip3 = baseIp + 233 + (4 * fid)

        fpga.config_10gbe_core('part1_gbe0', mac_base0, source_ip0, self.source_port, self.mac_list)
        fpga.config_10gbe_core('part1_gbe1', mac_base1, source_ip1, self.source_port, self.mac_list)
        fpga.config_10gbe_core('part1_gbe2', mac_base2, source_ip2, self.source_port, self.mac_list)
        fpga.config_10gbe_core('part1_gbe3', mac_base3, source_ip3, self.source_port, self.mac_list)

        # set fid
        if config_debug:
            print 'ConfigFE: Writing fid...'
        fpga.write_int('part1_f_id', fid)
        time.sleep(0.3)

        # set xid
        if config_debug:
            print 'ConfigFE: Writing xid...'

        for i in range(20):
            fpga.write_int('part1_x_id' + str(i), i)
        time.sleep(0.3)

        # set dest port
        if config_debug:
            print "ConfigFE: Writing destination port as: %d" % self.dest_port
        fpga.write_int('part1_x_port', self.dest_port)
        time.sleep(0.3)

        # set dest IP addrs
        if config_debug:
            print "ConfigFE: Writing destination IP addrs..."

        # If present, set all to blackhole ip's
        if self.blackhole:
            for i in range(20):
                fpga.write_int('part1_x_ip' + str(i), _ip_string_to_int(self.blackhole))

        # fill in for hpc's present
        for i in range(0, len(self.ip_addrs)):
            register = 'part1_x_ip' + str(i)
            split_addr = self.ip_addrs[i].split('.')
            dest_addr = 10*(2**24)+17*(2**16)+16*(2**8)+int(split_addr[-1])
            fpga.write_int(register, dest_addr)

    def set_coeff(self, fpga, fid, bankPrefixList):
        if config_debug:
            print "ConfigFE: Setting coefficients..."

        basePath = './config/coefficients/'
        bankPrefix = bankPrefixList[fid]
       
        #lsbre0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre0_' + lo + '.csv', delimiter='/n')
        #lsbre1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre1_' + lo + '.csv', delimiter='/n')
        #lsbre2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre2_' + lo + '.csv', delimiter='/n')
        #lsbre3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre3_' + lo + '.csv', delimiter='/n')
        #lsbre4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre4_' + lo + '.csv', delimiter='/n')
        #lsbre5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre5_' + lo + '.csv', delimiter='/n')
        #lsbre6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre6_' + lo + '.csv', delimiter='/n')
        #lsbre7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre7_' + lo + '.csv', delimiter='/n')

        #lsbim0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim0_' + lo + '.csv', delimiter='/n')
        #lsbim1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim1_' + lo + '.csv', delimiter='/n')
        #lsbim2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim2_' + lo + '.csv', delimiter='/n')
        #lsbim3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim3_' + lo + '.csv', delimiter='/n')
        #lsbim4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim4_' + lo + '.csv', delimiter='/n')
        #lsbim5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim5_' + lo + '.csv', delimiter='/n')
        #lsbim6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim6_' + lo + '.csv', delimiter='/n')
        #lsbim7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim7_' + lo + '.csv', delimiter='/n')

        #usbre0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre0_' + lo + '.csv', delimiter='/n')
        #usbre1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre1_' + lo + '.csv', delimiter='/n')
        #usbre2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre2_' + lo + '.csv', delimiter='/n')
        #usbre3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre3_' + lo + '.csv', delimiter='/n')
        #usbre4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre4_' + lo + '.csv', delimiter='/n')
        #usbre5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre5_' + lo + '.csv', delimiter='/n')
        #usbre6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre6_' + lo + '.csv', delimiter='/n')
        #usbre7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre7_' + lo + '.csv', delimiter='/n')

        #usbim0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim0_' + lo + '.csv', delimiter='/n')
        #usbim1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim1_' + lo + '.csv', delimiter='/n')
        #usbim2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim2_' + lo + '.csv', delimiter='/n')
        #usbim3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim3_' + lo + '.csv', delimiter='/n')
        #usbim4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim4_' + lo + '.csv', delimiter='/n')
        #usbim5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim5_' + lo + '.csv', delimiter='/n')
        #usbim6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim6_' + lo + '.csv', delimiter='/n')
        #usbim7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim7_' + lo + '.csv', delimiter='/n')
        
	#lsbre01 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre01_' + lo + '.csv', delimiter = '/n')
	#lsbre23 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre23_' + lo + '.csv', delimiter = '/n')
	#lsbre45 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre45_' + lo + '.csv', delimiter = '/n')
	#lsbre67 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre67_' + lo + '.csv', delimiter = '/n')
		                                  
	#lsbim01 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim01_' + lo + '.csv', delimiter = '/n')
	#lsbim23 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim23_' + lo + '.csv', delimiter = '/n')
	#lsbim45 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim45_' + lo + '.csv', delimiter = '/n')
	#lsbim67 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim67_' + lo + '.csv', delimiter = '/n')
		                                  
	#usbre01 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre01_' + lo + '.csv', delimiter = '/n')
	#usbre23 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre23_' + lo + '.csv', delimiter = '/n')
	#usbre45 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre45_' + lo + '.csv', delimiter = '/n')
	#usbre67 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre67_' + lo + '.csv', delimiter = '/n')
		                                  
	#usbim01 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim01_' + lo + '.csv', delimiter = '/n')
	#usbim23 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim23_' + lo + '.csv', delimiter = '/n')
	#usbim45 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim45_' + lo + '.csv', delimiter = '/n')
	#usbim67 = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim67_' + lo + '.csv', delimiter = '/n')



	bj_lsbrefft0x0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre0_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft0x1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre1_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft0x2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre2_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft0x3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre3_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft0x4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre4_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft0x5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre5_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft0x6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre6_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft0x7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre7_' + lo + '.csv', delimiter='/n')
		                                                             
	bj_lsbimfft0x0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim0_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft0x1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim1_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft0x2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim2_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft0x3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim3_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft0x4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim4_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft0x5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim5_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft0x6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim6_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft0x7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim7_' + lo + '.csv', delimiter='/n')
		                                                             
	bj_usbrefft0x0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre0_' + lo + '.csv', delimiter='/n')
	bj_usbrefft0x1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre1_' + lo + '.csv', delimiter='/n')
	bj_usbrefft0x2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre2_' + lo + '.csv', delimiter='/n')
	bj_usbrefft0x3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre3_' + lo + '.csv', delimiter='/n')
	bj_usbrefft0x4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre4_' + lo + '.csv', delimiter='/n')
	bj_usbrefft0x5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre5_' + lo + '.csv', delimiter='/n')
	bj_usbrefft0x6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre6_' + lo + '.csv', delimiter='/n')
	bj_usbrefft0x7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre7_' + lo + '.csv', delimiter='/n')
		                                                             
	bj_usbimfft0x0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim0_' + lo + '.csv', delimiter='/n')
	bj_usbimfft0x1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim1_' + lo + '.csv', delimiter='/n')
	bj_usbimfft0x2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim2_' + lo + '.csv', delimiter='/n')
	bj_usbimfft0x3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim3_' + lo + '.csv', delimiter='/n')
	bj_usbimfft0x4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim4_' + lo + '.csv', delimiter='/n')
	bj_usbimfft0x5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim5_' + lo + '.csv', delimiter='/n')
	bj_usbimfft0x6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim6_' + lo + '.csv', delimiter='/n')
	bj_usbimfft0x7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim7_' + lo + '.csv', delimiter='/n')

	bj_lsbrefft1x0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre0_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft1x1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre1_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft1x2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre2_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft1x3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre3_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft1x4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre4_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft1x5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre5_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft1x6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre6_' + lo + '.csv', delimiter='/n')
	bj_lsbrefft1x7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbre7_' + lo + '.csv', delimiter='/n')
		                                                             
	bj_lsbimfft1x0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim0_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft1x1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim1_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft1x2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim2_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft1x3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim3_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft1x4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim4_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft1x5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim5_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft1x6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim6_' + lo + '.csv', delimiter='/n')
	bj_lsbimfft1x7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_lsbim7_' + lo + '.csv', delimiter='/n')
		                                                             
	bj_usbrefft1x0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre0_' + lo + '.csv', delimiter='/n')
	bj_usbrefft1x1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre1_' + lo + '.csv', delimiter='/n')
	bj_usbrefft1x2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre2_' + lo + '.csv', delimiter='/n')
	bj_usbrefft1x3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre3_' + lo + '.csv', delimiter='/n')
	bj_usbrefft1x4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre4_' + lo + '.csv', delimiter='/n')
	bj_usbrefft1x5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre5_' + lo + '.csv', delimiter='/n')
	bj_usbrefft1x6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre6_' + lo + '.csv', delimiter='/n')
	bj_usbrefft1x7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbre7_' + lo + '.csv', delimiter='/n')
		                                                             
	bj_usbimfft1x0 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim0_' + lo + '.csv', delimiter='/n')
	bj_usbimfft1x1 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim1_' + lo + '.csv', delimiter='/n')
	bj_usbimfft1x2 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim2_' + lo + '.csv', delimiter='/n')
	bj_usbimfft1x3 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim3_' + lo + '.csv', delimiter='/n')
	bj_usbimfft1x4 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim4_' + lo + '.csv', delimiter='/n')
	bj_usbimfft1x5 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim5_' + lo + '.csv', delimiter='/n')
	bj_usbimfft1x6 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim6_' + lo + '.csv', delimiter='/n')
	bj_usbimfft1x7 = genfromtxt(basePath + lo + '/BJ_' + bankPrefix + '_256_usbim7_' + lo + '.csv', delimiter='/n')


	#lsbre01_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre01_' + lo + '_adj.csv', delimiter = '/n')
	#lsbre23_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre23_' + lo + '_adj.csv', delimiter = '/n')
	#lsbre45_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre45_' + lo + '_adj.csv', delimiter = '/n')
	#lsbre67_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre67_' + lo + '_adj.csv', delimiter = '/n')
		                                  
	#lsbim01_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim01_' + lo + '_adj.csv', delimiter = '/n')
	#lsbim23_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim23_' + lo + '_adj.csv', delimiter = '/n')
	#lsbim45_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim45_' + lo + '_adj.csv', delimiter = '/n')
	#lsbim67_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim67_' + lo + '_adj.csv', delimiter = '/n')
		                                  
	#usbre01_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre01_' + lo + '_adj.csv', delimiter = '/n')
	#usbre23_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre23_' + lo + '_adj.csv', delimiter = '/n')
	#usbre45_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre45_' + lo + '_adj.csv', delimiter = '/n')
	#usbre67_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre67_' + lo + '_adj.csv', delimiter = '/n')
		                                  
	#usbim01_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim01_' + lo + '_adj.csv', delimiter = '/n')
	#usbim23_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim23_' + lo + '_adj.csv', delimiter = '/n')
	#usbim45_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim45_' + lo + '_adj.csv', delimiter = '/n')
	#usbim67_adj = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim67_' + lo + '_adj.csv', delimiter = '/n')



	#lsbre01_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre01_' + lo + '_adj_repl.csv', delimiter = '/n')
	#lsbre23_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre23_' + lo + '_adj_repl.csv', delimiter = '/n')
	#lsbre45_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre45_' + lo + '_adj_repl.csv', delimiter = '/n')
	#lsbre67_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbre67_' + lo + '_adj_repl.csv', delimiter = '/n')
		                                  
	#lsbim01_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim01_' + lo + '_adj_repl.csv', delimiter = '/n')
	#lsbim23_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim23_' + lo + '_adj_repl.csv', delimiter = '/n')
	#lsbim45_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim45_' + lo + '_adj_repl.csv', delimiter = '/n')
	#lsbim67_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_lsbim67_' + lo + '_adj_repl.csv', delimiter = '/n')
                                                  
	#usbre01_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre01_' + lo + '_adj_repl.csv', delimiter = '/n')
	#usbre23_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre23_' + lo + '_adj_repl.csv', delimiter = '/n')
	#usbre45_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre45_' + lo + '_adj_repl.csv', delimiter = '/n')
    	#usbre67_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbre67_' + lo + '_adj_repl.csv', delimiter = '/n')
                                                  
    	#usbim01_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim01_' + lo + '_adj_repl.csv', delimiter = '/n')
    	#usbim23_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim23_' + lo + '_adj_repl.csv', delimiter = '/n')
    	#usbim45_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim45_' + lo + '_adj_repl.csv', delimiter = '/n')
    	#usbim67_adj_repl = genfromtxt(basePath + lo + '/' + bankPrefix + '_512_usbim67_' + lo + '_adj_repl.csv', delimiter = '/n')



	# 000000000000000000000 #      
	lsbrefft0x0_to_pack = np.round( bj_lsbrefft0x0*2**29)
	lsbrefft0x0_packed  = struct.pack('>512l', *lsbrefft0x0_to_pack)   #the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
	fpga.write('lsbre0_0', lsbrefft0x0_packed, 0)  

	lsbimfft0x0_to_pack = np.round( bj_lsbimfft0x0*2**29)
	lsbimfft0x0_packed  = struct.pack('>512l', *lsbimfft0x0_to_pack)
	fpga.write('lsbim0_0', lsbimfft0x0_packed, 0)

	usbrefft0x0_to_pack = np.round( bj_usbrefft0x0*2**29)
	usbrefft0x0_packed  = struct.pack('>512l', *usbrefft0x0_to_pack)
	fpga.write('usbre0_0', usbrefft0x0_packed, 0)

	usbimfft0x0_to_pack = np.round( bj_usbimfft0x0*2**29)
	usbimfft0x0_packed  = struct.pack('>512l', *usbimfft0x0_to_pack)
	fpga.write('usbim0_0', usbimfft0x0_packed, 0)

	# 11111111111111111111111 #      
	lsbrefft0x1_to_pack = np.round( bj_lsbrefft0x1*2**29)
	lsbrefft0x1_packed  = struct.pack('>512l', *lsbrefft0x1_to_pack)
	fpga.write('lsbre1_0', lsbrefft0x1_packed, 0)  

	lsbimfft0x1_to_pack = np.round( bj_lsbimfft0x1*2**29)
	lsbimfft0x1_packed  = struct.pack('>512l', *lsbimfft0x1_to_pack)
	fpga.write('lsbim1_0', lsbimfft0x1_packed, 0)

	usbrefft0x1_to_pack = np.round( bj_usbrefft0x1*2**29)
	usbrefft0x1_packed  = struct.pack('>512l', *usbrefft0x1_to_pack)
	fpga.write('usbre1_0', usbrefft0x1_packed, 0)

	usbimfft0x1_to_pack = np.round( bj_usbimfft0x1*2**29)
	usbimfft0x1_packed  = struct.pack('>512l', *usbimfft0x1_to_pack)
	fpga.write('usbim1_0', usbimfft0x1_packed, 0)

	# 2222222222222222222222 #      
	lsbrefft0x2_to_pack = np.round( bj_lsbrefft0x2*2**29)
	lsbrefft0x2_packed  = struct.pack('>512l', *lsbrefft0x2_to_pack)
	fpga.write('lsbre2_0', lsbrefft0x2_packed, 0)  

	lsbimfft0x2_to_pack = np.round( bj_lsbimfft0x2*2**29)
	lsbimfft0x2_packed  = struct.pack('>512l', *lsbimfft0x2_to_pack)
	fpga.write('lsbim2_0', lsbimfft0x2_packed, 0)

	usbrefft0x2_to_pack = np.round( bj_usbrefft0x2*2**29)
	usbrefft0x2_packed  = struct.pack('>512l', *usbrefft0x2_to_pack)
	fpga.write('usbre2_0', usbrefft0x2_packed, 0)

	usbimfft0x2_to_pack = np.round( bj_usbimfft0x2*2**29)
	usbimfft0x2_packed  = struct.pack('>512l', *usbimfft0x2_to_pack)
	fpga.write('usbim2_0', usbimfft0x2_packed, 0)

	# 3333333333333333333333 #      
	lsbrefft0x3_to_pack = np.round( bj_lsbrefft0x3*2**29)
	lsbrefft0x3_packed  = struct.pack('>512l', *lsbrefft0x3_to_pack)
	fpga.write('lsbre3_0', lsbrefft0x3_packed, 0)  

	lsbimfft0x3_to_pack = np.round( bj_lsbimfft0x3*2**29)
	lsbimfft0x3_packed  = struct.pack('>512l', *lsbimfft0x3_to_pack)
	fpga.write('lsbim3_0', lsbimfft0x3_packed, 0)

	usbrefft0x3_to_pack = np.round( bj_usbrefft0x3*2**29)
	usbrefft0x3_packed  = struct.pack('>512l', *usbrefft0x3_to_pack)
	fpga.write('usbre3_0', usbrefft0x3_packed, 0)

	usbimfft0x3_to_pack = np.round( bj_usbimfft0x3*2**29)
	usbimfft0x3_packed  = struct.pack('>512l', *usbimfft0x3_to_pack)
	fpga.write('usbim3_0', usbimfft0x3_packed, 0)

	# 4444444444444444444444 #      
	lsbrefft0x4_to_pack = np.round( bj_lsbrefft0x4*2**29)
	lsbrefft0x4_packed  = struct.pack('>512l', *lsbrefft0x4_to_pack)
	fpga.write('lsbre4_0', lsbrefft0x4_packed, 0)  

	lsbimfft0x4_to_pack = np.round( bj_lsbimfft0x4*2**29)
	lsbimfft0x4_packed  = struct.pack('>512l', *lsbimfft0x4_to_pack)
	fpga.write('lsbim4_0', lsbimfft0x4_packed, 0)

	usbrefft0x4_to_pack = np.round( bj_usbrefft0x4*2**29)
	usbrefft0x4_packed  = struct.pack('>512l', *usbrefft0x4_to_pack)
	fpga.write('usbre4_0', usbrefft0x4_packed, 0)

	usbimfft0x4_to_pack = np.round( bj_usbimfft0x4*2**29)
	usbimfft0x4_packed  = struct.pack('>512l', *usbimfft0x4_to_pack)
	fpga.write('usbim4_0', usbimfft0x4_packed, 0)

	# 5555555555555555555555 #      
	lsbrefft0x5_to_pack = np.round( bj_lsbrefft0x5*2**29)
	lsbrefft0x5_packed  = struct.pack('>512l', *lsbrefft0x5_to_pack)
	fpga.write('lsbre5_0', lsbrefft0x5_packed, 0)  

	lsbimfft0x5_to_pack = np.round( bj_lsbimfft0x5*2**29)
	lsbimfft0x5_packed  = struct.pack('>512l', *lsbimfft0x5_to_pack)
	fpga.write('lsbim5_0', lsbimfft0x5_packed, 0)

	usbrefft0x5_to_pack = np.round( bj_usbrefft0x5*2**29)
	usbrefft0x5_packed  = struct.pack('>512l', *usbrefft0x5_to_pack)
	fpga.write('usbre5_0', usbrefft0x5_packed, 0)

	usbimfft0x5_to_pack = np.round( bj_usbimfft0x5*2**29)
	usbimfft0x5_packed  = struct.pack('>512l', *usbimfft0x5_to_pack)
	fpga.write('usbim5_0', usbimfft0x5_packed, 0)

	# 6666666666666666666666 #      
	lsbrefft0x6_to_pack = np.round( bj_lsbrefft0x6*2**29)
	lsbrefft0x6_packed  = struct.pack('>512l', *lsbrefft0x6_to_pack)
	fpga.write('lsbre6_0', lsbrefft0x6_packed, 0)  

	lsbimfft0x6_to_pack = np.round( bj_lsbimfft0x6*2**29)
	lsbimfft0x6_packed  = struct.pack('>512l', *lsbimfft0x6_to_pack)
	fpga.write('lsbim6_0', lsbimfft0x6_packed, 0)

	usbrefft0x6_to_pack = np.round( bj_usbrefft0x6*2**29)
	usbrefft0x6_packed  = struct.pack('>512l', *usbrefft0x6_to_pack)
	fpga.write('usbre6_0', usbrefft0x6_packed, 0)

	usbimfft0x6_to_pack = np.round( bj_usbimfft0x6*2**29)
	usbimfft0x6_packed  = struct.pack('>512l', *usbimfft0x6_to_pack)
	fpga.write('usbim6_0', usbimfft0x6_packed, 0)

	# 7777777777777777777777 #      
	lsbrefft0x7_to_pack = np.round( bj_lsbrefft0x7*2**29)
	lsbrefft0x7_packed  = struct.pack('>512l', *lsbrefft0x7_to_pack)
	fpga.write('lsbre7_0', lsbrefft0x7_packed, 0)  

	lsbimfft0x7_to_pack = np.round( bj_lsbimfft0x7*2**29)
	lsbimfft0x7_packed  = struct.pack('>512l', *lsbimfft0x7_to_pack)
	fpga.write('lsbim7_0', lsbimfft0x7_packed, 0)

	usbrefft0x7_to_pack = np.round( bj_usbrefft0x7*2**29)
	usbrefft0x7_packed  = struct.pack('>512l', *usbrefft0x7_to_pack)
	fpga.write('usbre7_0', usbrefft0x7_packed, 0)

	usbimfft0x7_to_pack = np.round( bj_usbimfft0x7*2**29)
	usbimfft0x7_packed  = struct.pack('>512l', *usbimfft0x7_to_pack)
	fpga.write('usbim7_0', usbimfft0x7_packed, 0)

	# 000000000000000000000 #      
	lsbrefft1x0_to_pack = np.round( bj_lsbrefft1x0*2**29)
	lsbrefft1x0_packed  = struct.pack('>512l', *lsbrefft1x0_to_pack)   #the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
	fpga.write('lsbre0_1', lsbrefft1x0_packed, 0)  

	lsbimfft1x0_to_pack = np.round( bj_lsbimfft1x0*2**29)
	lsbimfft1x0_packed  = struct.pack('>512l', *lsbimfft1x0_to_pack)
	fpga.write('lsbim0_1', lsbimfft1x0_packed, 0)

	usbrefft1x0_to_pack = np.round( bj_usbrefft1x0*2**29)
	usbrefft1x0_packed  = struct.pack('>512l', *usbrefft1x0_to_pack)
	fpga.write('usbre0_1', usbrefft1x0_packed, 0)

	usbimfft1x0_to_pack = np.round( bj_usbimfft1x0*2**29)
	usbimfft1x0_packed  = struct.pack('>512l', *usbimfft1x0_to_pack)
	fpga.write('usbim0_1', usbimfft1x0_packed, 0)

	# 11111111111111111111111 #      
	lsbrefft1x1_to_pack = np.round( bj_lsbrefft1x1*2**29)
	lsbrefft1x1_packed  = struct.pack('>512l', *lsbrefft1x1_to_pack)
	fpga.write('lsbre1_1', lsbrefft1x1_packed, 0)  

	lsbimfft1x1_to_pack = np.round( bj_lsbimfft1x1*2**29)
	lsbimfft1x1_packed  = struct.pack('>512l', *lsbimfft1x1_to_pack)
	fpga.write('lsbim1_1', lsbimfft1x1_packed, 0)

	usbrefft1x1_to_pack = np.round( bj_usbrefft1x1*2**29)
	usbrefft1x1_packed  = struct.pack('>512l', *usbrefft1x1_to_pack)
	fpga.write('usbre1_1', usbrefft1x1_packed, 0)

	usbimfft1x1_to_pack = np.round( bj_usbimfft1x1*2**29)
	usbimfft1x1_packed  = struct.pack('>512l', *usbimfft1x1_to_pack)
	fpga.write('usbim1_1', usbimfft1x1_packed, 0)

	# 2222222222222222222222 #      
	lsbrefft1x2_to_pack = np.round( bj_lsbrefft1x2*2**29)
	lsbrefft1x2_packed  = struct.pack('>512l', *lsbrefft1x2_to_pack)
	fpga.write('lsbre2_1', lsbrefft1x2_packed, 0)  

	lsbimfft1x2_to_pack = np.round( bj_lsbimfft1x2*2**29)
	lsbimfft1x2_packed  = struct.pack('>512l', *lsbimfft1x2_to_pack)
	fpga.write('lsbim2_1', lsbimfft1x2_packed, 0)

	usbrefft1x2_to_pack = np.round( bj_usbrefft1x2*2**29)
	usbrefft1x2_packed  = struct.pack('>512l', *usbrefft1x2_to_pack)
	fpga.write('usbre2_1', usbrefft1x2_packed, 0)

	usbimfft1x2_to_pack = np.round( bj_usbimfft1x2*2**29)
	usbimfft1x2_packed  = struct.pack('>512l', *usbimfft1x2_to_pack)
	fpga.write('usbim2_1', usbimfft1x2_packed, 0)

	# 3333333333333333333333 #      
	lsbrefft1x3_to_pack = np.round( bj_lsbrefft1x3*2**29)
	lsbrefft1x3_packed  = struct.pack('>512l', *lsbrefft1x3_to_pack)
	fpga.write('lsbre3_1', lsbrefft1x3_packed, 0)  

	lsbimfft1x3_to_pack = np.round( bj_lsbimfft1x3*2**29)
	lsbimfft1x3_packed  = struct.pack('>512l', *lsbimfft1x3_to_pack)
	fpga.write('lsbim3_1', lsbimfft1x3_packed, 0)

	usbrefft1x3_to_pack = np.round( bj_usbrefft1x3*2**29)
	usbrefft1x3_packed  = struct.pack('>512l', *usbrefft1x3_to_pack)
	fpga.write('usbre3_1', usbrefft1x3_packed, 0)

	usbimfft1x3_to_pack = np.round( bj_usbimfft1x3*2**29)
	usbimfft1x3_packed  = struct.pack('>512l', *usbimfft1x3_to_pack)
	fpga.write('usbim3_1', usbimfft1x3_packed, 0)

	# 4444444444444444444444 #      
	lsbrefft1x4_to_pack = np.round( bj_lsbrefft1x4*2**29)
	lsbrefft1x4_packed  = struct.pack('>512l', *lsbrefft1x4_to_pack)
	fpga.write('lsbre4_1', lsbrefft1x4_packed, 0)  

	lsbimfft1x4_to_pack = np.round( bj_lsbimfft1x4*2**29)
	lsbimfft1x4_packed  = struct.pack('>512l', *lsbimfft1x4_to_pack)
	fpga.write('lsbim4_1', lsbimfft1x4_packed, 0)

	usbrefft1x4_to_pack = np.round( bj_usbrefft1x4*2**29)
	usbrefft1x4_packed  = struct.pack('>512l', *usbrefft1x4_to_pack)
	fpga.write('usbre4_1', usbrefft1x4_packed, 0)

	usbimfft1x4_to_pack = np.round( bj_usbimfft1x4*2**29)
	usbimfft1x4_packed  = struct.pack('>512l', *usbimfft1x4_to_pack)
	fpga.write('usbim4_1', usbimfft1x4_packed, 0)

	# 5555555555555555555555 #      
	lsbrefft1x5_to_pack = np.round( bj_lsbrefft1x5*2**29)
	lsbrefft1x5_packed  = struct.pack('>512l', *lsbrefft1x5_to_pack)
	fpga.write('lsbre5_1', lsbrefft1x5_packed, 0)  

	lsbimfft1x5_to_pack = np.round( bj_lsbimfft1x5*2**29)
	lsbimfft1x5_packed  = struct.pack('>512l', *lsbimfft1x5_to_pack)
	fpga.write('lsbim5_1', lsbimfft1x5_packed, 0)

	usbrefft1x5_to_pack = np.round( bj_usbrefft1x5*2**29)
	usbrefft1x5_packed  = struct.pack('>512l', *usbrefft1x5_to_pack)
	fpga.write('usbre5_1', usbrefft1x5_packed, 0)

	usbimfft1x5_to_pack = np.round( bj_usbimfft1x5*2**29)
	usbimfft1x5_packed  = struct.pack('>512l', *usbimfft1x5_to_pack)
	fpga.write('usbim5_1', usbimfft1x5_packed, 0)

	# 6666666666666666666666 #      
	lsbrefft1x6_to_pack = np.round( bj_lsbrefft1x6*2**29)
	lsbrefft1x6_packed  = struct.pack('>512l', *lsbrefft1x6_to_pack)
	fpga.write('lsbre6_1', lsbrefft1x6_packed, 0)  

	lsbimfft1x6_to_pack = np.round( bj_lsbimfft1x6*2**29)
	lsbimfft1x6_packed  = struct.pack('>512l', *lsbimfft1x6_to_pack)
	fpga.write('lsbim6_1', lsbimfft1x6_packed, 0)

	usbrefft1x6_to_pack = np.round( bj_usbrefft1x6*2**29)
	usbrefft1x6_packed  = struct.pack('>512l', *usbrefft1x6_to_pack)
	fpga.write('usbre6_1', usbrefft1x6_packed, 0)

	usbimfft1x6_to_pack = np.round( bj_usbimfft1x6*2**29)
	usbimfft1x6_packed  = struct.pack('>512l', *usbimfft1x6_to_pack)
	fpga.write('usbim6_1', usbimfft1x6_packed, 0)

	# 7777777777777777777777 #      
	lsbrefft1x7_to_pack = np.round( bj_lsbrefft1x7*2**29)
	lsbrefft1x7_packed  = struct.pack('>512l', *lsbrefft1x7_to_pack)
	fpga.write('lsbre7_1', lsbrefft1x7_packed, 0)  

	lsbimfft1x7_to_pack = np.round( bj_lsbimfft1x7*2**29)
	lsbimfft1x7_packed  = struct.pack('>512l', *lsbimfft1x7_to_pack)
	fpga.write('lsbim7_1', lsbimfft1x7_packed, 0)

	usbrefft1x7_to_pack = np.round( bj_usbrefft1x7*2**29)
	usbrefft1x7_packed  = struct.pack('>512l', *usbrefft1x7_to_pack)
	fpga.write('usbre7_1', usbrefft1x7_packed, 0)

	usbimfft1x7_to_pack = np.round( bj_usbimfft1x7*2**29)
	usbimfft1x7_packed  = struct.pack('>512l', *usbimfft1x7_to_pack)
	fpga.write('usbim7_1', usbimfft1x7_packed, 0)


        ## 000000 #
        #lsbre0_to_pack = np.round(lsbre0 * 2 ** 29)
        #lsbre0_packed = struct.pack('>256l', *lsbre0_to_pack)  # the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
        #fpga.write('lsbre0', lsbre0_packed, 0)

        #lsbim0_to_pack = np.round(lsbim0 * 2 ** 29)
        #lsbim0_packed = struct.pack('>256l', *lsbim0_to_pack)
        #fpga.write('lsbim0', lsbim0_packed, 0)

        #usbre0_to_pack = np.round(usbre0 * 2 ** 29)
        #usbre0_packed = struct.pack('>256l', *usbre0_to_pack)
        #fpga.write('usbre0', usbre0_packed, 0)

        #usbim0_to_pack = np.round(usbim0 * 2 ** 29)
        #usbim0_packed = struct.pack('>256l', *usbim0_to_pack)
        #fpga.write('usbim0', usbim0_packed, 0)

        ## 111111 #
        #lsbre1_to_pack = np.round(lsbre1 * 2 ** 29)
        #lsbre1_packed = struct.pack('>256l', *lsbre1_to_pack)
        #fpga.write('lsbre1', lsbre1_packed, 0)

        #lsbim1_to_pack = np.round(lsbim1 * 2 ** 29)
        #lsbim1_packed = struct.pack('>256l', *lsbim1_to_pack)
        #fpga.write('lsbim1', lsbim1_packed, 0)

        #usbre1_to_pack = np.round(usbre1 * 2 ** 29)
        #usbre1_packed = struct.pack('>256l', *usbre1_to_pack)
        #fpga.write('usbre1', usbre1_packed, 0)

        #usbim1_to_pack = np.round(usbim1 * 2 ** 29)
        #usbim1_packed = struct.pack('>256l', *usbim1_to_pack)
        #fpga.write('usbim1', usbim1_packed, 0)

        ## 222222 #
        #lsbre2_to_pack = np.round(lsbre2 * 2 ** 29)
        #lsbre2_packed = struct.pack('>256l', *lsbre2_to_pack)
        #fpga.write('lsbre2', lsbre2_packed, 0)

        #lsbim2_to_pack = np.round(lsbim2 * 2 ** 29)
        #lsbim2_packed = struct.pack('>256l', *lsbim2_to_pack)
        #fpga.write('lsbim2', lsbim2_packed, 0)

        #usbre2_to_pack = np.round(usbre2 * 2 ** 29)
        #usbre2_packed = struct.pack('>256l', *usbre2_to_pack)
        #fpga.write('usbre2', usbre2_packed, 0)

        #usbim2_to_pack = np.round(usbim2 * 2 ** 29)
        #usbim2_packed = struct.pack('>256l', *usbim2_to_pack)
        #fpga.write('usbim2', usbim2_packed, 0)

        ## 333333 #
        #lsbre3_to_pack = np.round(lsbre3 * 2 ** 29)
        #lsbre3_packed = struct.pack('>256l', *lsbre3_to_pack)
        #fpga.write('lsbre3', lsbre3_packed, 0)

        #lsbim3_to_pack = np.round(lsbim3 * 2 ** 29)
        #lsbim3_packed = struct.pack('>256l', *lsbim3_to_pack)
        #fpga.write('lsbim3', lsbim3_packed, 0)

        #usbre3_to_pack = np.round(usbre3 * 2 ** 29)
        #usbre3_packed = struct.pack('>256l', *usbre3_to_pack)
        #fpga.write('usbre3', usbre3_packed, 0)

        #usbim3_to_pack = np.round(usbim3 * 2 ** 29)
        #usbim3_packed = struct.pack('>256l', *usbim3_to_pack)
        #fpga.write('usbim3', usbim3_packed, 0)

        ## 444444 #
        #lsbre4_to_pack = np.round(lsbre4 * 2 ** 29)
        #lsbre4_packed = struct.pack('>256l', *lsbre4_to_pack)
        #fpga.write('lsbre4', lsbre4_packed, 0)

        #lsbim4_to_pack = np.round(lsbim4 * 2 ** 29)
        #lsbim4_packed = struct.pack('>256l', *lsbim4_to_pack)
        #fpga.write('lsbim4', lsbim4_packed, 0)

        #usbre4_to_pack = np.round(usbre4 * 2 ** 29)
        #usbre4_packed = struct.pack('>256l', *usbre4_to_pack)
        #fpga.write('usbre4', usbre4_packed, 0)

        #usbim4_to_pack = np.round(usbim4 * 2 ** 29)
        #usbim4_packed = struct.pack('>256l', *usbim4_to_pack)
        #fpga.write('usbim4', usbim4_packed, 0)

        ## 555555 #
        #lsbre5_to_pack = np.round(lsbre5 * 2 ** 29)
        #lsbre5_packed = struct.pack('>256l', *lsbre5_to_pack)
        #fpga.write('lsbre5', lsbre5_packed, 0)

        #lsbim5_to_pack = np.round(lsbim5 * 2 ** 29)
        #lsbim5_packed = struct.pack('>256l', *lsbim5_to_pack)
        #fpga.write('lsbim5', lsbim5_packed, 0)

        #usbre5_to_pack = np.round(usbre5 * 2 ** 29)
        #usbre5_packed = struct.pack('>256l', *usbre5_to_pack)
        #fpga.write('usbre5', usbre5_packed, 0)

        #usbim5_to_pack = np.round(usbim5 * 2 ** 29)
        #usbim5_packed = struct.pack('>256l', *usbim5_to_pack)
        #fpga.write('usbim5', usbim5_packed, 0)

        ## 666666 #
        #lsbre6_to_pack = np.round(lsbre6 * 2 ** 29)
        #lsbre6_packed = struct.pack('>256l', *lsbre6_to_pack)
        #fpga.write('lsbre6', lsbre6_packed, 0)

        #lsbim6_to_pack = np.round(lsbim6 * 2 ** 29)
        #lsbim6_packed = struct.pack('>256l', *lsbim6_to_pack)
        #fpga.write('lsbim6', lsbim6_packed, 0)

        #usbre6_to_pack = np.round(usbre6 * 2 ** 29)
        #usbre6_packed = struct.pack('>256l', *usbre6_to_pack)
        #fpga.write('usbre6', usbre6_packed, 0)

        #usbim6_to_pack = np.round(usbim6 * 2 ** 29)
        #usbim6_packed = struct.pack('>256l', *usbim6_to_pack)
        #fpga.write('usbim6', usbim6_packed, 0)

        ## 777777 #
        #lsbre7_to_pack = np.round(lsbre7 * 2 ** 29)
        #lsbre7_packed = struct.pack('>256l', *lsbre7_to_pack)
        #fpga.write('lsbre7', lsbre7_packed, 0)

        #lsbim7_to_pack = np.round(lsbim7 * 2 ** 29)
        #lsbim7_packed = struct.pack('>256l', *lsbim7_to_pack)
        #fpga.write('lsbim7', lsbim7_packed, 0)

        #usbre7_to_pack = np.round(usbre7 * 2 ** 29)
        #usbre7_packed = struct.pack('>256l', *usbre7_to_pack)
        #fpga.write('usbre7', usbre7_packed, 0)

        #usbim7_to_pack = np.round(usbim7 * 2 ** 29)
        #usbim7_packed = struct.pack('>256l', *usbim7_to_pack)
        #fpga.write('usbim7', usbim3_packed, 0)

    def set_safety(self, fpga):
        if config_debug:
            print "ConfigFE: Packet stop enabled"
        fpga.write_int('pkt_stop_en', 1)

    def write_quantization_gain(self, fpga, gain=0x00000014):
        if config_debug:
            print "ConfigFE: Writing quantization gain..."
        fpga.write_int('quant_gain', gain)

    def write_fft_shift(self, fpga, shift):
        if config_debug:
            print "ConfigFE: Writing fft_shift..."
        # fpga.write_int('fft_shift', shift)
        # Difference in register name due to change from CASPER to Xilinx fft blocks. 
        fpga.write_int('scale_sch_reg', shift)

    def bit_lock(self, fpga):
        if config_debug:
            print "ConfigFE: Aligning bits for %s" % fpga.host

        fpga.write_int('msb_realign', 1)
        time.sleep(0.5)

        fpga.write_int('msb_realign', 0)
        time.sleep(0.5)

        if (fpga.read_int('bit_locked') != 255):
            print "ConfigFE: bit locked failed."

    def byte_lock(self, fpga, check):
        if config_debug:
            print "ConfigFE: Aligning IQ for %s" % fpga.host

        if check:
            time.sleep(0.3)
            ####################################
            ####################################
            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 0)
            usb0_mag1, lsb0_mag1, usb1_mag1, lsb1_mag1 = treat_data_sbs(self.bb_lock_data_dir,  0, 0)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag1, lsb2_mag1, usb3_mag1, lsb3_mag1 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag1, lsb4_mag1, usb5_mag1, lsb5_mag1 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag1, lsb6_mag1, usb7_mag1, lsb7_mag1 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag2, lsb0_mag2, usb1_mag2, lsb1_mag2 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag2, lsb2_mag2, usb3_mag2, lsb3_mag2 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag2, lsb4_mag2, usb5_mag2, lsb5_mag2 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag2, lsb6_mag2, usb7_mag2, lsb7_mag2 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag3, lsb0_mag3, usb1_mag3, lsb1_mag3 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag3, lsb2_mag3, usb3_mag3, lsb3_mag3 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag3, lsb4_mag3, usb5_mag3, lsb5_mag3 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag3, lsb6_mag3, usb7_mag3, lsb7_mag3 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag4, lsb0_mag4, usb1_mag4, lsb1_mag4 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag4, lsb2_mag4, usb3_mag4, lsb3_mag4 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag4, lsb4_mag4, usb5_mag4, lsb5_mag4 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag4, lsb6_mag4, usb7_mag4, lsb7_mag4 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag5, lsb0_mag5, usb1_mag5, lsb1_mag5 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag5, lsb2_mag5, usb3_mag5, lsb3_mag5 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag5, lsb4_mag5, usb5_mag5, lsb5_mag5 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag5, lsb6_mag5, usb7_mag5, lsb7_mag5 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag6, lsb0_mag6, usb1_mag6, lsb1_mag6 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag6, lsb2_mag6, usb3_mag6, lsb3_mag6 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag6, lsb4_mag6, usb5_mag6, lsb5_mag6 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag6, lsb6_mag6, usb7_mag6, lsb7_mag6 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag7, lsb0_mag7, usb1_mag7, lsb1_mag7 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag7, lsb2_mag7, usb3_mag7, lsb3_mag7 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag7, lsb4_mag7, usb5_mag7, lsb5_mag7 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag7, lsb6_mag7, usb7_mag7, lsb7_mag7 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag8, lsb0_mag8, usb1_mag8, lsb1_mag8 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag8, lsb2_mag8, usb3_mag8, lsb3_mag8 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag8, lsb4_mag8, usb5_mag8, lsb5_mag8 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag8, lsb6_mag8, usb7_mag8, lsb7_mag8 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag9, lsb0_mag9, usb1_mag9, lsb1_mag9 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag9, lsb2_mag9, usb3_mag9, lsb3_mag9 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag9, lsb4_mag9, usb5_mag9, lsb5_mag9 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag9, lsb6_mag9, usb7_mag9, lsb7_mag9 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag10, lsb0_mag10, usb1_mag10, lsb1_mag10 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag10, lsb2_mag10, usb3_mag10, lsb3_mag10 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag10, lsb4_mag10, usb5_mag10, lsb5_mag10 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag10, lsb6_mag10, usb7_mag10, lsb7_mag10 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb0_mag = usb0_mag1
            lsb0_mag = lsb0_mag1
            usb1_mag = usb1_mag1
            lsb1_mag = lsb1_mag1
            usb2_mag = usb2_mag1
            lsb2_mag = lsb2_mag1
            usb3_mag = usb3_mag1
            lsb3_mag = lsb3_mag1
            usb4_mag = usb4_mag1
            lsb4_mag = lsb4_mag1
            usb5_mag = usb5_mag1
            lsb5_mag = lsb5_mag1
            usb6_mag = usb6_mag1
            lsb6_mag = lsb6_mag1
            usb7_mag = usb7_mag1
            lsb7_mag = lsb7_mag1

            ii = 1
            # while ii<257:
            ########luke
            while ii < 256:
                usb0_mag[ii] = usb0_mag1[ii] + usb0_mag2[ii] + usb0_mag3[ii] + usb0_mag4[ii] + usb0_mag5[ii] + \
                               usb0_mag6[ii] + usb0_mag7[ii] + usb0_mag8[ii] + usb0_mag9[ii] + usb0_mag10[ii]
                lsb0_mag[ii] = lsb0_mag1[ii] + lsb0_mag2[ii] + lsb0_mag3[ii] + lsb0_mag5[ii] + lsb0_mag5[ii] + \
                               lsb0_mag6[ii] + lsb0_mag7[ii] + lsb0_mag8[ii] + lsb0_mag9[ii] + lsb0_mag10[ii]
                usb1_mag[ii] = usb1_mag1[ii] + usb1_mag2[ii] + usb1_mag3[ii] + usb1_mag4[ii] + usb1_mag5[ii] + \
                               usb1_mag6[ii] + usb1_mag7[ii] + usb1_mag8[ii] + usb1_mag9[ii] + usb1_mag10[ii]
                lsb1_mag[ii] = lsb1_mag1[ii] + lsb1_mag2[ii] + lsb1_mag3[ii] + lsb1_mag5[ii] + lsb1_mag5[ii] + \
                               lsb1_mag6[ii] + lsb1_mag7[ii] + lsb1_mag8[ii] + lsb1_mag9[ii] + lsb1_mag10[ii]
                usb2_mag[ii] = usb2_mag1[ii] + usb2_mag2[ii] + usb2_mag3[ii] + usb2_mag4[ii] + usb2_mag5[ii] + \
                               usb2_mag6[ii] + usb2_mag7[ii] + usb2_mag8[ii] + usb2_mag9[ii] + usb2_mag10[ii]
                lsb2_mag[ii] = lsb2_mag1[ii] + lsb2_mag2[ii] + lsb2_mag3[ii] + lsb2_mag5[ii] + lsb2_mag5[ii] + \
                               lsb2_mag6[ii] + lsb2_mag7[ii] + lsb2_mag8[ii] + lsb2_mag9[ii] + lsb2_mag10[ii]
                usb3_mag[ii] = usb3_mag1[ii] + usb3_mag2[ii] + usb3_mag3[ii] + usb3_mag4[ii] + usb3_mag5[ii] + \
                               usb3_mag6[ii] + usb3_mag7[ii] + usb3_mag8[ii] + usb3_mag9[ii] + usb3_mag10[ii]
                lsb3_mag[ii] = lsb3_mag1[ii] + lsb3_mag2[ii] + lsb3_mag3[ii] + lsb3_mag5[ii] + lsb3_mag5[ii] + \
                               lsb3_mag6[ii] + lsb3_mag7[ii] + lsb3_mag8[ii] + lsb3_mag9[ii] + lsb3_mag10[ii]
                usb4_mag[ii] = usb4_mag1[ii] + usb4_mag2[ii] + usb4_mag3[ii] + usb4_mag4[ii] + usb4_mag5[ii] + \
                               usb4_mag6[ii] + usb4_mag7[ii] + usb4_mag8[ii] + usb4_mag9[ii] + usb4_mag10[ii]
                lsb4_mag[ii] = lsb4_mag1[ii] + lsb4_mag2[ii] + lsb4_mag3[ii] + lsb4_mag5[ii] + lsb4_mag5[ii] + \
                               lsb4_mag6[ii] + lsb4_mag7[ii] + lsb4_mag8[ii] + lsb4_mag9[ii] + lsb4_mag10[ii]
                usb5_mag[ii] = usb5_mag1[ii] + usb5_mag2[ii] + usb5_mag3[ii] + usb5_mag4[ii] + usb5_mag5[ii] + \
                               usb5_mag6[ii] + usb5_mag7[ii] + usb5_mag8[ii] + usb5_mag9[ii] + usb5_mag10[ii]
                lsb5_mag[ii] = lsb5_mag1[ii] + lsb5_mag2[ii] + lsb5_mag3[ii] + lsb5_mag5[ii] + lsb5_mag5[ii] + \
                               lsb5_mag6[ii] + lsb5_mag7[ii] + lsb5_mag8[ii] + lsb5_mag9[ii] + lsb5_mag10[ii]
                usb6_mag[ii] = usb6_mag1[ii] + usb6_mag2[ii] + usb6_mag3[ii] + usb6_mag4[ii] + usb6_mag5[ii] + \
                               usb6_mag6[ii] + usb6_mag7[ii] + usb6_mag8[ii] + usb6_mag9[ii] + usb6_mag10[ii]
                lsb6_mag[ii] = lsb6_mag1[ii] + lsb6_mag2[ii] + lsb6_mag3[ii] + lsb6_mag5[ii] + lsb6_mag5[ii] + \
                               lsb6_mag6[ii] + lsb6_mag7[ii] + lsb6_mag8[ii] + lsb6_mag9[ii] + lsb6_mag10[ii]
                usb7_mag[ii] = usb7_mag1[ii] + usb7_mag2[ii] + usb7_mag3[ii] + usb7_mag4[ii] + usb7_mag5[ii] + \
                               usb7_mag6[ii] + usb7_mag7[ii] + usb7_mag8[ii] + usb7_mag9[ii] + usb7_mag10[ii]
                lsb7_mag[ii] = lsb7_mag1[ii] + lsb7_mag2[ii] + lsb7_mag3[ii] + lsb7_mag5[ii] + lsb7_mag5[ii] + \
                               lsb7_mag6[ii] + lsb7_mag7[ii] + lsb7_mag8[ii] + lsb7_mag9[ii] + lsb7_mag10[ii]
                ii = ii + 1

            mag0_rat_a = usb0_mag[166] / lsb0_mag[166]
            mag1_rat_a = usb1_mag[166] / lsb1_mag[166]
            mag2_rat_a = usb2_mag[166] / lsb2_mag[166]
            mag3_rat_a = usb3_mag[166] / lsb3_mag[166]
            mag4_rat_a = usb4_mag[166] / lsb4_mag[166]
            mag5_rat_a = usb5_mag[166] / lsb5_mag[166]
            mag6_rat_a = usb6_mag[166] / lsb6_mag[166]
            mag7_rat_a = usb7_mag[166] / lsb7_mag[166]

            fpga.write_int('rxslide', 255)
            time.sleep(0.1)
            fpga.write_int('rxslide', 0)
            time.sleep(0.1)
            fpga.write_int('rxslide', 255)
            time.sleep(0.1)
            fpga.write_int('rxslide', 0)
            time.sleep(0.1)
            fpga.write_int('rxslide', 255)
            time.sleep(0.1)
            fpga.write_int('rxslide', 0)
            time.sleep(0.1)
            fpga.write_int('rxslide', 255)
            time.sleep(0.1)
            fpga.write_int('rxslide', 0)
            time.sleep(0.1)
            fpga.write_int('rxslide', 255)
            time.sleep(0.1)
            fpga.write_int('rxslide', 0)
            time.sleep(0.1)
            fpga.write_int('rxslide', 255)
            time.sleep(0.1)
            fpga.write_int('rxslide', 0)
            time.sleep(0.1)
            fpga.write_int('rxslide', 255)
            time.sleep(0.1)
            fpga.write_int('rxslide', 0)
            time.sleep(0.1)
            fpga.write_int('rxslide', 255)
            time.sleep(0.1)
            fpga.write_int('rxslide', 0)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 0)
            usb0_mag1, lsb0_mag1, usb1_mag1, lsb1_mag1 = treat_data_sbs(self.bb_lock_data_dir,  0, 0)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag1, lsb2_mag1, usb3_mag1, lsb3_mag1 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag1, lsb4_mag1, usb5_mag1, lsb5_mag1 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag1, lsb6_mag1, usb7_mag1, lsb7_mag1 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag2, lsb0_mag2, usb1_mag2, lsb1_mag2 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag2, lsb2_mag2, usb3_mag2, lsb3_mag2 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag2, lsb4_mag2, usb5_mag2, lsb5_mag2 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag2, lsb6_mag2, usb7_mag2, lsb7_mag2 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag3, lsb0_mag3, usb1_mag3, lsb1_mag3 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag3, lsb2_mag3, usb3_mag3, lsb3_mag3 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag3, lsb4_mag3, usb5_mag3, lsb5_mag3 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag3, lsb6_mag3, usb7_mag3, lsb7_mag3 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag4, lsb0_mag4, usb1_mag4, lsb1_mag4 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag4, lsb2_mag4, usb3_mag4, lsb3_mag4 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag4, lsb4_mag4, usb5_mag4, lsb5_mag4 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag4, lsb6_mag4, usb7_mag4, lsb7_mag4 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag5, lsb0_mag5, usb1_mag5, lsb1_mag5 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag5, lsb2_mag5, usb3_mag5, lsb3_mag5 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag5, lsb4_mag5, usb5_mag5, lsb5_mag5 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag5, lsb6_mag5, usb7_mag5, lsb7_mag5 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag6, lsb0_mag6, usb1_mag6, lsb1_mag6 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag6, lsb2_mag6, usb3_mag6, lsb3_mag6 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag6, lsb4_mag6, usb5_mag6, lsb5_mag6 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag6, lsb6_mag6, usb7_mag6, lsb7_mag6 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag7, lsb0_mag7, usb1_mag7, lsb1_mag7 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag7, lsb2_mag7, usb3_mag7, lsb3_mag7 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag7, lsb4_mag7, usb5_mag7, lsb5_mag7 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag7, lsb6_mag7, usb7_mag7, lsb7_mag7 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag8, lsb0_mag8, usb1_mag8, lsb1_mag8 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag8, lsb2_mag8, usb3_mag8, lsb3_mag8 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag8, lsb4_mag8, usb5_mag8, lsb5_mag8 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag8, lsb6_mag8, usb7_mag8, lsb7_mag8 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag9, lsb0_mag9, usb1_mag9, lsb1_mag9 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag9, lsb2_mag9, usb3_mag9, lsb3_mag9 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag9, lsb4_mag9, usb5_mag9, lsb5_mag9 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag9, lsb6_mag9, usb7_mag9, lsb7_mag9 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag10, lsb0_mag10, usb1_mag10, lsb1_mag10 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag10, lsb2_mag10, usb3_mag10, lsb3_mag10 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag10, lsb4_mag10, usb5_mag10, lsb5_mag10 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag10, lsb6_mag10, usb7_mag10, lsb7_mag10 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb0_mag = usb0_mag1
            lsb0_mag = lsb0_mag1
            usb1_mag = usb1_mag1
            lsb1_mag = lsb1_mag1
            usb2_mag = usb2_mag1
            lsb2_mag = lsb2_mag1
            usb3_mag = usb3_mag1
            lsb3_mag = lsb3_mag1
            usb4_mag = usb4_mag1
            lsb4_mag = lsb4_mag1
            usb5_mag = usb5_mag1
            lsb5_mag = lsb5_mag1
            usb6_mag = usb6_mag1
            lsb6_mag = lsb6_mag1
            usb7_mag = usb7_mag1
            lsb7_mag = lsb7_mag1

            ii = 1
            # while ii<257:
            ########luke
            while ii < 256:
                usb0_mag[ii] = usb0_mag1[ii] + usb0_mag2[ii] + usb0_mag3[ii] + usb0_mag4[ii] + usb0_mag5[ii] + \
                               usb0_mag6[ii] + usb0_mag7[ii] + usb0_mag8[ii] + usb0_mag9[ii] + usb0_mag10[ii]
                lsb0_mag[ii] = lsb0_mag1[ii] + lsb0_mag2[ii] + lsb0_mag3[ii] + lsb0_mag5[ii] + lsb0_mag5[ii] + \
                               lsb0_mag6[ii] + lsb0_mag7[ii] + lsb0_mag8[ii] + lsb0_mag9[ii] + lsb0_mag10[ii]
                usb1_mag[ii] = usb1_mag1[ii] + usb1_mag2[ii] + usb1_mag3[ii] + usb1_mag4[ii] + usb1_mag5[ii] + \
                               usb1_mag6[ii] + usb1_mag7[ii] + usb1_mag8[ii] + usb1_mag9[ii] + usb1_mag10[ii]
                lsb1_mag[ii] = lsb1_mag1[ii] + lsb1_mag2[ii] + lsb1_mag3[ii] + lsb1_mag5[ii] + lsb1_mag5[ii] + \
                               lsb1_mag6[ii] + lsb1_mag7[ii] + lsb1_mag8[ii] + lsb1_mag9[ii] + lsb1_mag10[ii]
                usb2_mag[ii] = usb2_mag1[ii] + usb2_mag2[ii] + usb2_mag3[ii] + usb2_mag4[ii] + usb2_mag5[ii] + \
                               usb2_mag6[ii] + usb2_mag7[ii] + usb2_mag8[ii] + usb2_mag9[ii] + usb2_mag10[ii]
                lsb2_mag[ii] = lsb2_mag1[ii] + lsb2_mag2[ii] + lsb2_mag3[ii] + lsb2_mag5[ii] + lsb2_mag5[ii] + \
                               lsb2_mag6[ii] + lsb2_mag7[ii] + lsb2_mag8[ii] + lsb2_mag9[ii] + lsb2_mag10[ii]
                usb3_mag[ii] = usb3_mag1[ii] + usb3_mag2[ii] + usb3_mag3[ii] + usb3_mag4[ii] + usb3_mag5[ii] + \
                               usb3_mag6[ii] + usb3_mag7[ii] + usb3_mag8[ii] + usb3_mag9[ii] + usb3_mag10[ii]
                lsb3_mag[ii] = lsb3_mag1[ii] + lsb3_mag2[ii] + lsb3_mag3[ii] + lsb3_mag5[ii] + lsb3_mag5[ii] + \
                               lsb3_mag6[ii] + lsb3_mag7[ii] + lsb3_mag8[ii] + lsb3_mag9[ii] + lsb3_mag10[ii]
                usb4_mag[ii] = usb4_mag1[ii] + usb4_mag2[ii] + usb4_mag3[ii] + usb4_mag4[ii] + usb4_mag5[ii] + \
                               usb4_mag6[ii] + usb4_mag7[ii] + usb4_mag8[ii] + usb4_mag9[ii] + usb4_mag10[ii]
                lsb4_mag[ii] = lsb4_mag1[ii] + lsb4_mag2[ii] + lsb4_mag3[ii] + lsb4_mag5[ii] + lsb4_mag5[ii] + \
                               lsb4_mag6[ii] + lsb4_mag7[ii] + lsb4_mag8[ii] + lsb4_mag9[ii] + lsb4_mag10[ii]
                usb5_mag[ii] = usb5_mag1[ii] + usb5_mag2[ii] + usb5_mag3[ii] + usb5_mag4[ii] + usb5_mag5[ii] + \
                               usb5_mag6[ii] + usb5_mag7[ii] + usb5_mag8[ii] + usb5_mag9[ii] + usb5_mag10[ii]
                lsb5_mag[ii] = lsb5_mag1[ii] + lsb5_mag2[ii] + lsb5_mag3[ii] + lsb5_mag5[ii] + lsb5_mag5[ii] + \
                               lsb5_mag6[ii] + lsb5_mag7[ii] + lsb5_mag8[ii] + lsb5_mag9[ii] + lsb5_mag10[ii]
                usb6_mag[ii] = usb6_mag1[ii] + usb6_mag2[ii] + usb6_mag3[ii] + usb6_mag4[ii] + usb6_mag5[ii] + \
                               usb6_mag6[ii] + usb6_mag7[ii] + usb6_mag8[ii] + usb6_mag9[ii] + usb6_mag10[ii]
                lsb6_mag[ii] = lsb6_mag1[ii] + lsb6_mag2[ii] + lsb6_mag3[ii] + lsb6_mag5[ii] + lsb6_mag5[ii] + \
                               lsb6_mag6[ii] + lsb6_mag7[ii] + lsb6_mag8[ii] + lsb6_mag9[ii] + lsb6_mag10[ii]
                usb7_mag[ii] = usb7_mag1[ii] + usb7_mag2[ii] + usb7_mag3[ii] + usb7_mag4[ii] + usb7_mag5[ii] + \
                               usb7_mag6[ii] + usb7_mag7[ii] + usb7_mag8[ii] + usb7_mag9[ii] + usb7_mag10[ii]
                lsb7_mag[ii] = lsb7_mag1[ii] + lsb7_mag2[ii] + lsb7_mag3[ii] + lsb7_mag5[ii] + lsb7_mag5[ii] + \
                               lsb7_mag6[ii] + lsb7_mag7[ii] + lsb7_mag8[ii] + lsb7_mag9[ii] + lsb7_mag10[ii]
                ii = ii + 1

            mag0_rat_b = usb0_mag[166] / lsb0_mag[166]
            mag1_rat_b = usb1_mag[166] / lsb1_mag[166]
            mag2_rat_b = usb2_mag[166] / lsb2_mag[166]
            mag3_rat_b = usb3_mag[166] / lsb3_mag[166]
            mag4_rat_b = usb4_mag[166] / lsb4_mag[166]
            mag5_rat_b = usb5_mag[166] / lsb5_mag[166]
            mag6_rat_b = usb6_mag[166] / lsb6_mag[166]
            mag7_rat_b = usb7_mag[166] / lsb7_mag[166]

            print ('mag0_rat_a')
            print mag0_rat_a
            print ('mag0_rat_b')
            print mag0_rat_b

            print ('mag1_rat_a')
            print mag1_rat_a
            print ('mag1_rat_b')
            print mag1_rat_b

            print ('mag2_rat_a')
            print mag2_rat_a
            print ('mag2_rat_b')
            print mag2_rat_b

            print ('mag3_rat_a')
            print mag3_rat_a
            print ('mag3_rat_b')
            print mag3_rat_b

            print ('mag4_rat_a')
            print mag4_rat_a
            print ('mag4_rat_b')
            print mag4_rat_b

            print ('mag5_rat_a')
            print mag5_rat_a
            print ('mag5_rat_b')
            print mag5_rat_b

            print ('mag6_rat_a')
            print mag6_rat_a
            print ('mag6_rat_b')
            print mag6_rat_b

            print ('mag7_rat_a')
            print mag7_rat_a
            print ('mag7_rat_b')
            print mag7_rat_b

            if (mag0_rat_b < mag0_rat_a):
                ##flyp dem bites on A0!!!
                print 're-aligning 0 \n'
                fpga.write_int('rxslide', 1)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 1)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 1)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 1)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 1)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 1)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 1)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 1)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)

            if (mag1_rat_b < mag1_rat_a):
                ##flyp dem bites on A1!!!
                print 're-aligning 1 \n'
                fpga.write_int('rxslide', 2)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 2)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 2)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 2)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 2)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 2)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 2)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 2)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)

            if (mag2_rat_b < mag2_rat_a):
                ##flyp dem bites on A2!!!
                print 're-aligning 2 \n'
                fpga.write_int('rxslide', 4)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 4)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 4)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 4)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 4)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 4)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 4)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 4)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)

            if (mag3_rat_b < mag3_rat_a):
                ##flyp dem bites on A3!!!
                print 're-aligning 3 \n'
                fpga.write_int('rxslide', 8)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 8)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 8)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 8)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 8)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 8)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 8)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 8)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)

            if (mag4_rat_b < mag4_rat_a):
                ##flyp dem bites on A4!!!
                print 're-aligning 4 \n'
                fpga.write_int('rxslide', 16)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 16)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 16)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 16)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 16)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 16)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 16)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 16)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)

            if (mag5_rat_b < mag5_rat_a):
                ##flyp dem bites on A5!!!
                print 're-aligning 5 \n'
                fpga.write_int('rxslide', 32)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 32)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 32)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 32)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 32)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 32)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 32)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 32)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)

            if (mag6_rat_b < mag6_rat_a):
                ##flyp dem bites on A6!!!
                print 're-aligning 6 \n'
                fpga.write_int('rxslide', 64)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 64)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 64)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 64)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 64)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 64)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 64)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 64)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)

            if (mag7_rat_b < mag7_rat_a):
                ##flyp dem bites on A7!!!
                print 're-aligning 7 \n'
                fpga.write_int('rxslide', 128)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 128)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 128)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 128)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 128)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 128)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 128)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)
                time.sleep(0.1)
                fpga.write_int('rxslide', 128)
                time.sleep(0.1)
                fpga.write_int('rxslide', 0)

        else:
            if config_debug:
                print "ConfigFE: Checking IQ Alignment..."

            time.sleep(0.3)
            ####################################
            ####################################
            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 0)
            usb0_mag1, lsb0_mag1, usb1_mag1, lsb1_mag1 = treat_data_sbs(self.bb_lock_data_dir,  0, 0)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag1, lsb2_mag1, usb3_mag1, lsb3_mag1 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag1, lsb4_mag1, usb5_mag1, lsb5_mag1 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag1, lsb6_mag1, usb7_mag1, lsb7_mag1 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag2, lsb0_mag2, usb1_mag2, lsb1_mag2 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag2, lsb2_mag2, usb3_mag2, lsb3_mag2 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag2, lsb4_mag2, usb5_mag2, lsb5_mag2 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag2, lsb6_mag2, usb7_mag2, lsb7_mag2 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag3, lsb0_mag3, usb1_mag3, lsb1_mag3 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag3, lsb2_mag3, usb3_mag3, lsb3_mag3 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag3, lsb4_mag3, usb5_mag3, lsb5_mag3 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag3, lsb6_mag3, usb7_mag3, lsb7_mag3 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag4, lsb0_mag4, usb1_mag4, lsb1_mag4 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag4, lsb2_mag4, usb3_mag4, lsb3_mag4 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag4, lsb4_mag4, usb5_mag4, lsb5_mag4 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag4, lsb6_mag4, usb7_mag4, lsb7_mag4 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag5, lsb0_mag5, usb1_mag5, lsb1_mag5 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag5, lsb2_mag5, usb3_mag5, lsb3_mag5 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag5, lsb4_mag5, usb5_mag5, lsb5_mag5 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag5, lsb6_mag5, usb7_mag5, lsb7_mag5 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag6, lsb0_mag6, usb1_mag6, lsb1_mag6 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag6, lsb2_mag6, usb3_mag6, lsb3_mag6 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag6, lsb4_mag6, usb5_mag6, lsb5_mag6 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag6, lsb6_mag6, usb7_mag6, lsb7_mag6 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag7, lsb0_mag7, usb1_mag7, lsb1_mag7 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag7, lsb2_mag7, usb3_mag7, lsb3_mag7 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag7, lsb4_mag7, usb5_mag7, lsb5_mag7 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag7, lsb6_mag7, usb7_mag7, lsb7_mag7 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag8, lsb0_mag8, usb1_mag8, lsb1_mag8 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag8, lsb2_mag8, usb3_mag8, lsb3_mag8 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag8, lsb4_mag8, usb5_mag8, lsb5_mag8 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag8, lsb6_mag8, usb7_mag8, lsb7_mag8 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag9, lsb0_mag9, usb1_mag9, lsb1_mag9 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag9, lsb2_mag9, usb3_mag9, lsb3_mag9 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag9, lsb4_mag9, usb5_mag9, lsb5_mag9 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag9, lsb6_mag9, usb7_mag9, lsb7_mag9 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 0, 1)
            usb0_mag10, lsb0_mag10, usb1_mag10, lsb1_mag10 = treat_data_sbs(self.bb_lock_data_dir,  0, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 2, 1)
            usb2_mag10, lsb2_mag10, usb3_mag10, lsb3_mag10 = treat_data_sbs(self.bb_lock_data_dir,  2, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 4, 1)
            usb4_mag10, lsb4_mag10, usb5_mag10, lsb5_mag10 = treat_data_sbs(self.bb_lock_data_dir,  4, 1)

            usb_r, usb_i, lsb_r, lsb_i = get_data_sbs(fpga, self.bb_lock_data_dir, 6, 1)
            usb6_mag10, lsb6_mag10, usb7_mag10, lsb7_mag10 = treat_data_sbs(self.bb_lock_data_dir,  6, 1)

            time.sleep(0.1)

            usb0_mag = usb0_mag1
            lsb0_mag = lsb0_mag1
            usb1_mag = usb1_mag1
            lsb1_mag = lsb1_mag1
            usb2_mag = usb2_mag1
            lsb2_mag = lsb2_mag1
            usb3_mag = usb3_mag1
            lsb3_mag = lsb3_mag1
            usb4_mag = usb4_mag1
            lsb4_mag = lsb4_mag1
            usb5_mag = usb5_mag1
            lsb5_mag = lsb5_mag1
            usb6_mag = usb6_mag1
            lsb6_mag = lsb6_mag1
            usb7_mag = usb7_mag1
            lsb7_mag = lsb7_mag1

            ii = 1
            # while ii<257:
            ########luke
            while ii < 256:
                usb0_mag[ii] = usb0_mag1[ii] + usb0_mag2[ii] + usb0_mag3[ii] + usb0_mag4[ii] + usb0_mag5[ii] + \
                               usb0_mag6[ii] + usb0_mag7[ii] + usb0_mag8[ii] + usb0_mag9[ii] + usb0_mag10[ii]
                lsb0_mag[ii] = lsb0_mag1[ii] + lsb0_mag2[ii] + lsb0_mag3[ii] + lsb0_mag5[ii] + lsb0_mag5[ii] + \
                               lsb0_mag6[ii] + lsb0_mag7[ii] + lsb0_mag8[ii] + lsb0_mag9[ii] + lsb0_mag10[ii]
                usb1_mag[ii] = usb1_mag1[ii] + usb1_mag2[ii] + usb1_mag3[ii] + usb1_mag4[ii] + usb1_mag5[ii] + \
                               usb1_mag6[ii] + usb1_mag7[ii] + usb1_mag8[ii] + usb1_mag9[ii] + usb1_mag10[ii]
                lsb1_mag[ii] = lsb1_mag1[ii] + lsb1_mag2[ii] + lsb1_mag3[ii] + lsb1_mag5[ii] + lsb1_mag5[ii] + \
                               lsb1_mag6[ii] + lsb1_mag7[ii] + lsb1_mag8[ii] + lsb1_mag9[ii] + lsb1_mag10[ii]
                usb2_mag[ii] = usb2_mag1[ii] + usb2_mag2[ii] + usb2_mag3[ii] + usb2_mag4[ii] + usb2_mag5[ii] + \
                               usb2_mag6[ii] + usb2_mag7[ii] + usb2_mag8[ii] + usb2_mag9[ii] + usb2_mag10[ii]
                lsb2_mag[ii] = lsb2_mag1[ii] + lsb2_mag2[ii] + lsb2_mag3[ii] + lsb2_mag5[ii] + lsb2_mag5[ii] + \
                               lsb2_mag6[ii] + lsb2_mag7[ii] + lsb2_mag8[ii] + lsb2_mag9[ii] + lsb2_mag10[ii]
                usb3_mag[ii] = usb3_mag1[ii] + usb3_mag2[ii] + usb3_mag3[ii] + usb3_mag4[ii] + usb3_mag5[ii] + \
                               usb3_mag6[ii] + usb3_mag7[ii] + usb3_mag8[ii] + usb3_mag9[ii] + usb3_mag10[ii]
                lsb3_mag[ii] = lsb3_mag1[ii] + lsb3_mag2[ii] + lsb3_mag3[ii] + lsb3_mag5[ii] + lsb3_mag5[ii] + \
                               lsb3_mag6[ii] + lsb3_mag7[ii] + lsb3_mag8[ii] + lsb3_mag9[ii] + lsb3_mag10[ii]
                usb4_mag[ii] = usb4_mag1[ii] + usb4_mag2[ii] + usb4_mag3[ii] + usb4_mag4[ii] + usb4_mag5[ii] + \
                               usb4_mag6[ii] + usb4_mag7[ii] + usb4_mag8[ii] + usb4_mag9[ii] + usb4_mag10[ii]
                lsb4_mag[ii] = lsb4_mag1[ii] + lsb4_mag2[ii] + lsb4_mag3[ii] + lsb4_mag5[ii] + lsb4_mag5[ii] + \
                               lsb4_mag6[ii] + lsb4_mag7[ii] + lsb4_mag8[ii] + lsb4_mag9[ii] + lsb4_mag10[ii]
                usb5_mag[ii] = usb5_mag1[ii] + usb5_mag2[ii] + usb5_mag3[ii] + usb5_mag4[ii] + usb5_mag5[ii] + \
                               usb5_mag6[ii] + usb5_mag7[ii] + usb5_mag8[ii] + usb5_mag9[ii] + usb5_mag10[ii]
                lsb5_mag[ii] = lsb5_mag1[ii] + lsb5_mag2[ii] + lsb5_mag3[ii] + lsb5_mag5[ii] + lsb5_mag5[ii] + \
                               lsb5_mag6[ii] + lsb5_mag7[ii] + lsb5_mag8[ii] + lsb5_mag9[ii] + lsb5_mag10[ii]
                usb6_mag[ii] = usb6_mag1[ii] + usb6_mag2[ii] + usb6_mag3[ii] + usb6_mag4[ii] + usb6_mag5[ii] + \
                               usb6_mag6[ii] + usb6_mag7[ii] + usb6_mag8[ii] + usb6_mag9[ii] + usb6_mag10[ii]
                lsb6_mag[ii] = lsb6_mag1[ii] + lsb6_mag2[ii] + lsb6_mag3[ii] + lsb6_mag5[ii] + lsb6_mag5[ii] + \
                               lsb6_mag6[ii] + lsb6_mag7[ii] + lsb6_mag8[ii] + lsb6_mag9[ii] + lsb6_mag10[ii]
                usb7_mag[ii] = usb7_mag1[ii] + usb7_mag2[ii] + usb7_mag3[ii] + usb7_mag4[ii] + usb7_mag5[ii] + \
                               usb7_mag6[ii] + usb7_mag7[ii] + usb7_mag8[ii] + usb7_mag9[ii] + usb7_mag10[ii]
                lsb7_mag[ii] = lsb7_mag1[ii] + lsb7_mag2[ii] + lsb7_mag3[ii] + lsb7_mag5[ii] + lsb7_mag5[ii] + \
                               lsb7_mag6[ii] + lsb7_mag7[ii] + lsb7_mag8[ii] + lsb7_mag9[ii] + lsb7_mag10[ii]
                ii = ii + 1

            print ('usb0_mag_sum')
            print usb0_mag[166]
            print ('lsb0_mag_sum')
            print lsb0_mag[166]

            print ('usb1_mag_sum')
            print usb1_mag[166]
            print ('lsb1_mag_sum')
            print lsb1_mag[166]

            print ('usb2_mag_sum')
            print usb2_mag[166]
            print ('lsb2_mag_sum')
            print lsb2_mag[166]

            print ('usb3_mag_sum')
            print usb3_mag[166]
            print ('lsb3_mag_sum')
            print lsb3_mag[166]

            print ('usb4_mag_sum')
            print usb4_mag[166]
            print ('lsb4_mag_sum')
            print lsb4_mag[166]

            print ('usb5_mag_sum')
            print usb5_mag[166]
            print ('lsb5_mag_sum')
            print lsb5_mag[166]

            print ('usb6_mag_sum')
            print usb6_mag[166]
            print ('lsb6_mag_sum')
            print lsb6_mag[166]

            print ('usb7_mag_sum')
            print usb7_mag[166]
            print ('lsb7_mag_sum')
            print lsb7_mag[166]

            if (usb0_mag[166] < (lsb0_mag[166] * 10)):
                ##flyp dem bites on A0!!!
                print 'Channel 0 not aligned\n'
                if (usb0_mag[166] < (lsb0_mag[166] * 3)):
                    print 'setting threshold lower \n'
                    fpga.write_int('rxslide', 1)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 1)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 1)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 1)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 1)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 1)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 1)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 1)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)

            if (usb1_mag[166] < (lsb1_mag[166] * 10)):
                ##flyp dem bites on A1!!!
                print 'Channel 1 not aligned\n'
                if (usb1_mag[166] < (lsb1_mag[166] * 3)):
                    print 'setting threshold lower \n'
                    fpga.write_int('rxslide', 2)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 2)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 2)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 2)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 2)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 2)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 2)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 2)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)

            if (usb2_mag[166] < (lsb2_mag[166] * 10)):
                ##flyp dem bites on A2!!!
                print 'Channel 2 not aligned\n'
                if (usb2_mag[166] < (lsb2_mag[166] * 3)):
                    print 'setting threshold lower \n'
                    fpga.write_int('rxslide', 4)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 4)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 4)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 4)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 4)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 4)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 4)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 4)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)

            if (usb3_mag[166] < (lsb3_mag[166] * 10)):
                ##flyp dem bites on A3!!!
                print 'Channel 3 not aligned\n'
                if (usb3_mag[166] < (lsb3_mag[166] * 3)):
                    print 'setting threshold lower \n'
                    fpga.write_int('rxslide', 8)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 8)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 8)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 8)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 8)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 8)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 8)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 8)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)

            if (usb4_mag[166] < (lsb4_mag[166] * 10)):
                ##flyp dem bites on A4!!!
                print 'Channel 4 not aligned\n'
                if (usb4_mag[166] < (lsb4_mag[166] * 3)):
                    print 'setting threshold lower \n'
                    fpga.write_int('rxslide', 16)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 16)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 16)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 16)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 16)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 16)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 16)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 16)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)

            if (usb5_mag[166] < (lsb5_mag[166] * 10)):
                ##flyp dem bites on A5!!!
                print 'Channel 5 not aligned\n'
                if (usb5_mag[166] < (lsb5_mag[166] * 3)):
                    print 'setting threshold lower \n'
                    fpga.write_int('rxslide', 32)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 32)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 32)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 32)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 32)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 32)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 32)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 32)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)

            if (usb6_mag[166] < (lsb6_mag[166] * 10)):
                ##flyp dem bites on A6!!!
                print 'Channel 6 not aligned\n'
                if (usb6_mag[166] < (lsb6_mag[166] * 3)):
                    print 'setting threshold lower \n'
                    fpga.write_int('rxslide', 64)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 64)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 64)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 64)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 64)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 64)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 64)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 64)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)

            if (usb7_mag[166] < (lsb7_mag[166] * 10)):
                ##flyp dem bites on A7!!!
                print 'Channel 7 not aligned\n'
                if (usb7_mag[166] < (lsb7_mag[166] * 3)):
                    print 'setting threshold lower \n'
                    fpga.write_int('rxslide', 164)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 164)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 164)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 164)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 164)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 164)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 164)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 164)
                    time.sleep(0.1)
                    fpga.write_int('rxslide', 0)
            
            







