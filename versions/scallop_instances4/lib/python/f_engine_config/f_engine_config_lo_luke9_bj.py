#!/opt/local/bin/python2.7

import corr, time, struct, sys, logging, socket, math, operator
from numpy import genfromtxt
import numpy as np
import matplotlib.pyplot as plt

boffile     = 'fbjdbg_new6a_2018_Nov_02_1639.bof'#Luke replace boffile name
paf0        = 10*(2**24)+17*(2**16)+16*(2**8)+39
flag3_0     = 10*(2**24)+17*(2**16)+16*(2**8)+208
flag3_1     = 10*(2**24)+17*(2**16)+16*(2**8)+209
flag3_2     = 10*(2**24)+17*(2**16)+16*(2**8)+210
flag3_3     = 10*(2**24)+17*(2**16)+16*(2**8)+211
#west        = 10*(2**24)+17*(2**16)+0*(2**8)+35 # west ip address , need to direct the unwanted packets somewhere   
#tofu        = 10*(2**24)+17*(2**16)+0*(2**8)+36 # ip address of tofu for the correct mac address
#south       = 10*(2**24)+17*(2**16)+0*(2**8)+33 
blackhole   = 10*(2**24)+17*(2**16)+16*(2**8)+200

source_port = 60000                             #the physical port on the roach board side  which it will be sending from
                    #luke why 60,000???
#luke 1_23
#import nb_util as nb
#nb.connect()


def set_attens1(dB_lvl, blade_number):
    print('setting attenuation values for balancing\n')
    nb.set_atten(blade_number*8+0,dB_lvl)
    nb.set_atten(blade_number*8+1,dB_lvl)
    nb.set_atten(blade_number*8+2,dB_lvl)
    nb.set_atten(blade_number*8+3,dB_lvl)
    nb.set_atten(blade_number*8+4,dB_lvl)
    nb.set_atten(blade_number*8+5,dB_lvl)
    nb.set_atten(blade_number*8+6,dB_lvl)
    nb.set_atten(blade_number*8+7,dB_lvl)
    print('done setting attenuation values for balancing\n')

def set_1atten(dB_lvl, chan_num, blade_number):
    nb.set_atten(blade_number*8+chan_num, dB_lvl)


def get_i_q_dat(chan_num):
    print('reading i/q data from roach for balancing\n')
    #balance_0_raw = fpga.read('final_data_'+str(chan_num),2*2**16,0)
    balance_0_raw = fpga.read('final_data_0',2*2**16,0)
    #balance_1_raw = fpga.read('final_data_1',2*2**16,0)
    #balance_2_raw = fpga.read('final_data_2',2*2**16,0)
    #balance_3_raw = fpga.read('final_data_3',2*2**16,0)
    #balance_4_raw = fpga.read('final_data_4',2*2**16,0)
    #balance_5_raw = fpga.read('final_data_5',2*2**16,0)
    #balance_6_raw = fpga.read('final_data_6',2*2**16,0)
    #balance_7_raw = fpga.read('final_data_7',2*2**16,0)
    print('done reading i/q data from roach for balancing\n')


    print('converting raw i/q data for balancing\n')
    balance_0 = struct.unpack('>65536h',balance_0_raw)
    bal_0 = np.array(balance_0)
    bal_0_i = (bal_0>>8) &0xff
    bal_0_q = (bal_0 & 0xff)
    #balance_1 = struct.unpack('>65536h',balance_1_raw)
    #bal_1 = np.array(balance_1)
    #bal_1_i = (bal_1>>8) &0xff
    #bal_1_q = (bal_1 & 0xff)
    #balance_2 = struct.unpack('>65536h',balance_2_raw)
    #bal_2 = np.array(balance_2)
    #bal_2_i = (bal_2>>8) &0xff
    #bal_2_q = (bal_2 & 0xff)
    #balance_3 = struct.unpack('>65536h',balance_3_raw)
    #bal_3 = np.array(balance_3)
    #bal_3_i = (bal_3>>8) &0xff
    #bal_3_q = (bal_3 & 0xff)
    #balance_4 = struct.unpack('>65536h',balance_4_raw)
    #bal_4 = np.array(balance_4)
    #bal_4_i = (bal_4>>8) &0xff
    #bal_4_q = (bal_4 & 0xff)
    #balance_5 = struct.unpack('>65536h',balance_5_raw)
    #bal_5 = np.array(balance_5)
    #bal_5_i = (bal_5>>8) &0xff
    #bal_5_q = (bal_5 & 0xff)
    #balance_6 = struct.unpack('>65536h',balance_6_raw)
    #bal_6 = np.array(balance_6)
    #bal_6_i = (bal_6>>8) &0xff
    #bal_6_q = (bal_6 & 0xff)
    #balance_7 = struct.unpack('>65536h',balance_7_raw)
    #bal_7 = np.array(balance_7)
    #bal_7_i = (bal_7>>8) &0xff
    #bal_7_q = (bal_7 & 0xff)
    print('done converting raw i/q data for balancing\n')

    print('starting rms-summing loop for balancing\n')
    count = 0
    rms_0_sum = 0
    #rms_1_sum = 0
    #rms_2_sum = 0
    #rms_3_sum = 0
    #rms_4_sum = 0
    #rms_5_sum = 0
    #rms_6_sum = 0
    #rms_7_sum = 0

    while (count<65535):
        rms_0_sum = rms_0_sum+(bal_0_i[count]-128)**2+(bal_0_q[count]-128)**2
    #    rms_1_sum = rms_1_sum+(bal_1_i[count]-128)**2+(bal_1_q[count]-128)**2
    #    rms_2_sum = rms_2_sum+(bal_2_i[count]-128)**2+(bal_2_q[count]-128)**2
    #    rms_3_sum = rms_3_sum+(bal_0_i[count]-128)**2+(bal_3_q[count]-128)**2
    #    rms_4_sum = rms_4_sum+(bal_0_i[count]-128)**2+(bal_4_q[count]-128)**2
    #    rms_5_sum = rms_5_sum+(bal_0_i[count]-128)**2+(bal_5_q[count]-128)**2
    #    rms_6_sum = rms_6_sum+(bal_0_i[count]-128)**2+(bal_6_q[count]-128)**2
    #    rms_7_sum = rms_7_sum+(bal_0_i[count]-128)**2+(bal_7_q[count]-128)**2
        count=count+1
    print('end rms-summing loop for balancing\n')
    
    rms_0 = math.sqrt(rms_0_sum/(65536*2))
    print rms_0
    print ('rms_0\n')
    #rms_1 = math.sqrt(rms_1_sum/(65536*2))
    #print rms_1
    #print ('rms_1\n')
    #rms_2 = math.sqrt(rms_2_sum/(65536*2))
    #print rms_2
    #print ('rms_2\n')
    #rms_3 = math.sqrt(rms_3_sum/(65536*2))
    #print rms_3
    #print ('rms_3\n')
    #rms_4 = math.sqrt(rms_4_sum/(65536*2))
    #print rms_4
    #print ('rms_4\n')
    #rms_5 = math.sqrt(rms_5_sum/(65536*2))
    #print rms_5
    #print ('rms_5\n')
    
    return rms_0




#From Ray's Dealer Player code
def _ip_string_to_int(ip):
    """_ip_string_to_int(ip)

    Takes an IP address in string representation and returns an integer
    representation::

      iip = _ip_string_to_int('10.17.0.51')
      print(hex(iip))
      0x0A110040

    """
    try:
        rval = sum(map(lambda x, y: x << y,
                       [int(p) for p in ip.split('.')], [24, 16, 8, 0]))
    except (TypeError, AttributeError):
        rval = None

    return rval

def _hostname_to_ip(hostname):
    """_hostname_to_ip(hostname)

    Takes a hostname string and returns an IP address string::

      ip = _hostname_to_ip('vegasr2-1')
      print(ip)
      10.17.0.64

    """
    try:
        rval = socket.gethostbyaddr(hostname)[2][0]
    except (TypeError, socket.gaierror, socket.herror):
        rval = None

    return rval

macs = {
    '10.17.16.39' : 0x000F5308458C, # paf0
    '10.17.16.208': 0x7CFE90B92DF0, # flag3_0 
    '10.17.16.209': 0x7CFE90B92BD0, # flag3_1 
    '10.17.16.210': 0x7CFE90B92BD1, # flag3_2
    '10.17.16.211': 0x7CFE90B92DF1, # flag3_3
#    '10.17.0.35': 0x000F530C668C, # west
#    '10.17.0.33': 0x0002C952FDCB, # south
#    '10.17.0.36': 0x000F530CFDB8, # tofu
    '10.17.16.200': 0x0202b1ac401e, # blackhole mac address and fake ip; Note the switch is configured to drop packets sent to this mac
}

hpc_macs = {}
hpc_macs_list = [0xffffffffffff] * 256

for hostname, mac in macs.iteritems():
    key = _ip_string_to_int(_hostname_to_ip(hostname)) & 0xFF
    hpc_macs[key] = mac
    hpc_macs_list[key] = mac


def treat_data_sbs(j,k):
    if(j==0):
            fur67 = open("usb_data_re01.txt","r")
            fui67 = open("usb_data_im01.txt","r")
            flr67 = open("lsb_data_re01.txt","r")
            fli67 = open("lsb_data_im01.txt","r")
    elif (j==2):
            fur67 = open("usb_data_re23.txt","r")
            fui67 = open("usb_data_im23.txt","r")
            flr67 = open("lsb_data_re23.txt","r")
            fli67 = open("lsb_data_im23.txt","r")
    elif (j==4):
            fur67 = open("usb_data_re45.txt","r")
            fui67 = open("usb_data_im45.txt","r")
            flr67 = open("lsb_data_re45.txt","r")
            fli67 = open("lsb_data_im45.txt","r")
    elif (j==6):
            fur67 = open("usb_data_re67.txt","r")
            fui67 = open("usb_data_im67.txt","r")
            flr67 = open("lsb_data_re67.txt","r")
            fli67 = open("lsb_data_im67.txt","r")
    else:
        print 'wrong j value in treat_data_sbs \n'

    usb_r67 = np.array(fur67.read().split('\n'))
    usb_i67 = np.array(fui67.read().split('\n'))
    lsb_r67 = np.array(flr67.read().split('\n'))
    lsb_i67 = np.array(fli67.read().split('\n'))

    i=0
    usb67_mag = usb_r67
    lsb67_mag = lsb_r67
    while i<512:#513:
        usb_r67[i] = float(usb_r67[i])
        usb_i67[i] = float(usb_i67[i])
        usb67_mag[i]=np.sqrt(float((float(usb_r67[i])*float(usb_r67[i])+float(usb_i67[i])*float(usb_i67[i]))))
        lsb67_mag[i]=np.sqrt(float((float(lsb_r67[i])*float(lsb_r67[i])+float(lsb_i67[i])*float(lsb_i67[i])))) 
        #usb67_mag[i]=float(np.sqrt(float(usb_r67[i])*float(usb_r67[i])+float(usb_i67[i])*float(usb_i67[i])))
        #lsb67_mag[i]=float(np.sqrt(float(lsb_r67[i])*float(lsb_r67[i])+float(lsb_i67[i])*float(lsb_i67[i])))          
        i=i+1

    #usb6_mag=map(float,(usb67_mag[0:256]))
    #usb7_mag=map(float,(usb67_mag[256:512]))
    #lsb6_mag=map(float,(lsb67_mag[0:256]))
    #lsb7_mag=map(float,(lsb67_mag[256:512]))

    #count3=0
    #fft0=map(float,(usb67_mag[0:256]))
    #while (count3<256):
    #    fft0[count3]=usb67_mag[count3]
    #    count3=count3+1
    #fft1=np.fft.rfft(fft0)
    #fft_x = np.arange(0,129,1)
    #fix, ax=plt.subplots(1,1)
    #plt.plot(fft1,fft_x)


    ############luke
    usb6_mag1=map(float,(usb67_mag[0:256]))
    usb7_mag1=map(float,(usb67_mag[256:512]))
    lsb6_mag1=map(float,(lsb67_mag[0:256]))
    lsb7_mag1=map(float,(lsb67_mag[256:512]))

    #usb6_mag1=map(float,(usb67_mag[5:251]))
    #usb7_mag1=map(float,(usb67_mag[261:507]))
    #lsb6_mag1=map(float,(lsb67_mag[5:251]))
    #lsb7_mag1=map(float,(lsb67_mag[261:507]))

    return usb6_mag1, lsb6_mag1, usb7_mag1, lsb7_mag1



def get_data_sbs(k,j):

    #for k in range(4):
        if(k==6):
            lsb_raw_re = fpga.read('lsb_re_'+str(k)+str(k+1)+'_2',2**12,0)
        else:
            lsb_raw_re = fpga.read('lsb_re_'+str(k)+str(k+1)+'_1',2**12,0)
        lsb_re = struct.unpack('>4096b',lsb_raw_re)

        if(k==6):
            lsb_raw_im = fpga.read('lsb_im_'+str(k)+str(k+1)+'_2',2**12,0)
        else:
            lsb_raw_im = fpga.read('lsb_im_'+str(k)+str(k+1)+'_1',2**12,0)
        lsb_im = struct.unpack('>4096b',lsb_raw_im)

        if(k==6):
            usb_raw_re = fpga.read('usb_re_'+str(k)+str(k+1)+'_2',2**12,0)
        else:
            usb_raw_re = fpga.read('usb_re_'+str(k)+str(k+1)+'_1',2**12,0)
        usb_re = struct.unpack('>4096b',usb_raw_re)

        if(k==6):
            usb_raw_im = fpga.read('usb_im_'+str(k)+str(k+1)+'_2',2**12,0)
        else:
            usb_raw_im = fpga.read('usb_im_'+str(k)+str(k+1)+'_1',2**12,0)
        usb_im = struct.unpack('>4096b',usb_raw_im)
       
        usb_r = np.array(usb_re)/128.0
        #if (j==0):
            #print usb_re
            #print (len(usb_r))
            #print ('first contents of usb_data_re01.txt\n')
            #print(j)
            #print (usb_re[0:255])
            #print ('check it \n')
        usb_i = np.array(usb_im)/128.0
        lsb_r = np.array(lsb_re)/128.0
        lsb_i = np.array(lsb_im)/128.0
        
        m=0
        with open('lsb_data_re'+str(k)+str(k+1)+'.txt','w') as f:
           for m in range(len(lsb_r)):
               the_str = ""
               the_str += str(lsb_r[m]) + "\n"
               f.write(the_str)
        m=0
        with open('lsb_data_im'+str(k)+str(k+1)+'.txt','w') as f:
           for m in range(len(lsb_i)):
               the_str = ""
               the_str += str(lsb_i[m]) + "\n"
               f.write(the_str)

        m=0
        with open('usb_data_re'+str(k)+str(k+1)+'.txt','w') as f:
           for m in range(len(usb_r)):
               the_str = ""
               the_str += str(usb_r[m]) + "\n"
               f.write(the_str)
        m=0
        with open('usb_data_im'+str(k)+str(k+1)+'.txt','w') as f:
           for m in range(len(usb_i)):
               the_str = ""
               the_str += str(usb_i[m]) + "\n"
               f.write(the_str)
        
        return usb_r, usb_i, lsb_r, lsb_i

def get_data_iq(rcv_nr):
    data = fpga.read('final_data_'+str(rcv_nr),2**16*2,0)         #read data from specific receiver number
    d_0=struct.unpack('>65536H',data) #< for little endian B for (8 bits) now made H for 16 bits
    d_0i = []
    d_0q = []
    for n in range(65536):
        d_0i.append(d_0[n]>>8)   #get the top 8 bits of final_data_rcv_nr 
        d_0q.append(d_0[n]&0xff) #get the bottom 8 bits of final_data_rcv_nr

    m=0
    with open('data_' +str(rcv_nr) +'i.txt','w') as f:
       for m in range(len(d_0i)):
           the_str = ""
           the_str += str(d_0i[m]) + "\n"
           f.write(the_str)
    m=0
    with open('data_' +str(rcv_nr) +'q.txt','w') as f1:
       for m in range(len(d_0q)):
           the_str = ""
           the_str += str(d_0q[m]) + "\n"
           f1.write(the_str)

#this function should do a byte swap on the channel specified. 
#As a first implementation ch should be represented in hex
def swap_byte(fpga,ch):
    fpga.write_int('rxslide',ch)   
    fpga.write_int('rxslide',0x00)

    #fpga.write_int('rxslide',ch)   
    #time.sleep(0.5)
    #if (hex(fpga.read_int('msb2_loc')) == 0x80):
    #       fpga.write_int('rxslide',0x00)          
    #       return;
    #time.sleep(1)
    #fpga.write_int('rxslide',0x00)
    #time.sleep(1)
    #while(hex(fpga.read_int('msb2_loc')) != 0x80):
    #    fpga.write_int('rxslide',ch) 
    #    time.sleep(0.5)
    #    if (hex(fpga.read_int('msb2_loc')) == 0x80):
    #       fpga.write_int('rxslide',0x00)          
    #       break;
    #    time.sleep(1)
    #    fpga.write_int('rxslide',0x00)
    #    time.sleep(1)    

def exit_fail():
    print 'FAILURE DETECTED. Log entries:\n',lh.printMessages()
    try:
        fpga.stop()
    except: pass
    raise
    exit()



def exit_clean():
    try:
        for f in fpgas: f.stop()
    except: pass
    exit()


if __name__ == '__main__':
    from optparse import OptionParser

    p = OptionParser()
    p.set_usage('f_engine_config_lo.py <ROACH_HOSTNAME_or_IP> [options]')
    p.set_description(__doc__)
    p.add_option('-n'  , '--noprogram'         , dest='noprogram'       , action='store_true'                      , help='Don\'t reprogram the roach.')  
    p.add_option('-c'  , '--set_coefficients'  , dest='set_coef'        , type = 'int'       , default=0           , help='Set the sideband separating coefficients.')  
    p.add_option('-l'  , '--lo'                , dest='lo'              , type = 'str'       , default='1450'      , help='Set the LO for sbs coefficients, default is 1450 MHz')  
    p.add_option('-r'  , '--reset_transceivers', dest='reset_trans'     , type = 'int'       , default=0           , help='Reset data input transceivers.')  
    p.add_option('-i'  , '--fft_shift'         , dest='fft_shift'       , type = 'long'      , default=0xaaaaaaaa  , help='Set the fft_shift register, default is 0xaaaaaaaa')  
    p.add_option('-b'  , '--bit_lock'          , dest='auto_align_bits' , type = 'int'       , default=0           , help='Automatically bit align the system')  
    p.add_option('-B'  , '--byte_lock'         , dest='auto_align_bytes', type = 'int'       , default=0           , help='1=calc and align bytes, 2=check byte alignment')
    p.add_option('-e'  , '--byte_swap'         , dest='swap_bytes_ch'   , type = 'long'      , default=-1          , help='Swap bytes of specified channel')
    p.add_option('-a'  , '--set_arm'           , dest='arm'             , type = 'int'       , default=0           , help='ARM the subsystem')  
    p.add_option('-t'  , '--get_time_data'     , dest='time_data'       , type = 'int'       , default=0           , help='Get time domain i and q data')  
    p.add_option('-s'  , '--get_sbs_data'      , dest='sbs_data'        , type = 'int'       , default=0           , help='Get sideband separated frequency data')  
    p.add_option('-d'  , '--boffile'           , dest='bof'             , type = 'str'       , default=boffile     , help='Specify the bof file to load')  
    p.add_option('-p'  , '--packetizer'        , dest='pack'            , type = 'int'       , default=0           , help='Reset GbE cores, start GbE cores, Set f_id & x_id registers on firmware. Set destination port')  
    p.add_option('-x'  , '--xid'               , dest='xid'             , type = 'int'       , default=-1          , help='The x id number for the sideband to grab')  
    p.add_option('-f'  , '--fid'               , dest='fid'             , type = 'int'       , default=-1          , help='The f id number for the roach.')  
    p.add_option('-o'  , '--dport'             , dest='dport'           , type = 'int'       , default=-1          , help='The destination port number')  
    p.add_option('-C'  , '--dcomp'             , dest='dcomp'           , type = 'str'       , default='blackhole' , help='The destination computer (paf0, tofu)')  
    #Luke added
    #p.add_option('-q   , '--IQ_stage'          , dest='iq_stage'        , type = 'int'       , default =0          , help='select stage: 1=initial/USB, 2=check/LSB')
    p.add_option('-z' , '--balance'           , dest='balance'         , type = 'int'        ,default=0             , help='attenuation balancing')
    opts, args = p.parse_args(sys.argv[1:])

    if args==[]:
       print 'Please specify a ROACH board. \nExiting.'
       exit()
    else:
       roach = args[0]

    if opts.bof != '':
       boffile = opts.bof

    if (opts.fid == -1):
       print 'Specify ROACH f_id.'
       exit()

    blade_roach = ['A', 'B', 'C', 'D', 'E']
    fid_prefix  = blade_roach[opts.fid]

    lsbre01 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre01_'+opts.lo+'.csv', delimiter = '/n')
    lsbre23 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre23_'+opts.lo+'.csv', delimiter = '/n')
    lsbre45 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre45_'+opts.lo+'.csv', delimiter = '/n')
    lsbre67 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre67_'+opts.lo+'.csv', delimiter = '/n')
                                                  
    lsbim01 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim01_'+opts.lo+'.csv', delimiter = '/n')
    lsbim23 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim23_'+opts.lo+'.csv', delimiter = '/n')
    lsbim45 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim45_'+opts.lo+'.csv', delimiter = '/n')
    lsbim67 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim67_'+opts.lo+'.csv', delimiter = '/n')
                                                  
    usbre01 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre01_'+opts.lo+'.csv', delimiter = '/n')
    usbre23 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre23_'+opts.lo+'.csv', delimiter = '/n')
    usbre45 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre45_'+opts.lo+'.csv', delimiter = '/n')
    usbre67 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre67_'+opts.lo+'.csv', delimiter = '/n')
                                                  
    usbim01 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim01_'+opts.lo+'.csv', delimiter = '/n')
    usbim23 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim23_'+opts.lo+'.csv', delimiter = '/n')
    usbim45 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim45_'+opts.lo+'.csv', delimiter = '/n')
    usbim67 = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim67_'+opts.lo+'.csv', delimiter = '/n')



    bj_lsbrefft0x0 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre0_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft0x1 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre1_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft0x2 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre2_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft0x3 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre3_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft0x4 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre4_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft0x5 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre5_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft0x6 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre6_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft0x7 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre7_'+opts.lo+'.csv', delimiter = '/n')
                                                                             
    bj_lsbimfft0x0 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim0_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft0x1 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim1_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft0x2 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim2_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft0x3 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim3_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft0x4 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim4_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft0x5 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim5_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft0x6 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim6_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft0x7 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim7_'+opts.lo+'.csv', delimiter = '/n')
                                                                             
    bj_usbrefft0x0 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre0_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft0x1 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre1_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft0x2 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre2_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft0x3 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre3_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft0x4 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre4_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft0x5 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre5_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft0x6 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre6_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft0x7 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre7_'+opts.lo+'.csv', delimiter = '/n')
                                                                             
    bj_usbimfft0x0 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim0_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft0x1 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim1_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft0x2 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim2_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft0x3 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim3_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft0x4 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim4_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft0x5 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim5_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft0x6 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim6_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft0x7 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim7_'+opts.lo+'.csv', delimiter = '/n')

    bj_lsbrefft1x0 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre0_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft1x1 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre1_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft1x2 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre2_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft1x3 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre3_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft1x4 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre4_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft1x5 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre5_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft1x6 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre6_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbrefft1x7 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbre7_'+opts.lo+'.csv', delimiter = '/n')
                                                                             
    bj_lsbimfft1x0 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim0_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft1x1 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim1_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft1x2 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim2_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft1x3 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim3_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft1x4 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim4_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft1x5 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim5_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft1x6 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim6_'+opts.lo+'.csv', delimiter = '/n')
    bj_lsbimfft1x7 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_lsbim7_'+opts.lo+'.csv', delimiter = '/n')
                                                                             
    bj_usbrefft1x0 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre0_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft1x1 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre1_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft1x2 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre2_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft1x3 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre3_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft1x4 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre4_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft1x5 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre5_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft1x6 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre6_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbrefft1x7 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbre7_'+opts.lo+'.csv', delimiter = '/n')
                                                                             
    bj_usbimfft1x0 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim0_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft1x1 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim1_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft1x2 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim2_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft1x3 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim3_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft1x4 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim4_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft1x5 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim5_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft1x6 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim6_'+opts.lo+'.csv', delimiter = '/n')
    bj_usbimfft1x7 = genfromtxt('./coefficients/'+opts.lo+'/BJ'+fid_prefix+'_256_usbim7_'+opts.lo+'.csv', delimiter = '/n')


    lsbre01_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre01_'+opts.lo+'_adj.csv', delimiter = '/n')
    lsbre23_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre23_'+opts.lo+'_adj.csv', delimiter = '/n')
    lsbre45_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre45_'+opts.lo+'_adj.csv', delimiter = '/n')
    lsbre67_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre67_'+opts.lo+'_adj.csv', delimiter = '/n')
                                                  
    lsbim01_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim01_'+opts.lo+'_adj.csv', delimiter = '/n')
    lsbim23_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim23_'+opts.lo+'_adj.csv', delimiter = '/n')
    lsbim45_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim45_'+opts.lo+'_adj.csv', delimiter = '/n')
    lsbim67_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim67_'+opts.lo+'_adj.csv', delimiter = '/n')
                                                  
    usbre01_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre01_'+opts.lo+'_adj.csv', delimiter = '/n')
    usbre23_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre23_'+opts.lo+'_adj.csv', delimiter = '/n')
    usbre45_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre45_'+opts.lo+'_adj.csv', delimiter = '/n')
    usbre67_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre67_'+opts.lo+'_adj.csv', delimiter = '/n')
                                                  
    usbim01_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim01_'+opts.lo+'_adj.csv', delimiter = '/n')
    usbim23_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim23_'+opts.lo+'_adj.csv', delimiter = '/n')
    usbim45_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim45_'+opts.lo+'_adj.csv', delimiter = '/n')
    usbim67_adj = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim67_'+opts.lo+'_adj.csv', delimiter = '/n')



    lsbre01_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre01_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    lsbre23_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre23_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    lsbre45_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre45_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    lsbre67_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbre67_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
                                                  
    lsbim01_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim01_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    lsbim23_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim23_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    lsbim45_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim45_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    lsbim67_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_lsbim67_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
                                                  
    usbre01_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre01_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    usbre23_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre23_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    usbre45_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre45_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    usbre67_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbre67_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
                                                  
    usbim01_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim01_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    usbim23_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim23_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    usbim45_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim45_'+opts.lo+'_adj_repl.csv', delimiter = '/n')
    usbim67_adj_repl = genfromtxt('./coefficients/'+opts.lo+'/'+fid_prefix+'_512_usbim67_'+opts.lo+'_adj_repl.csv', delimiter = '/n')

    mac_base0   = (2<<40) + (2<<32) + (10<<24) + (17<<16) + (16<<8) + (4*opts.fid+230)               #give each physical port a unique id 
    mac_base1   = (2<<40) + (2<<32) + (10<<24) + (17<<16) + (16<<8) + (4*opts.fid+231) 
    mac_base2   = (2<<40) + (2<<32) + (10<<24) + (17<<16) + (16<<8) + (4*opts.fid+232) 
    mac_base3   = (2<<40) + (2<<32) + (10<<24) + (17<<16) + (16<<8) + (4*opts.fid+233) 
    
    source_ip0 = 10*(2**24)+17*(2**16)+16*(2**8)+230+(4*opts.fid) 
    source_ip1 = 10*(2**24)+17*(2**16)+16*(2**8)+231+(4*opts.fid) 
    source_ip2 = 10*(2**24)+17*(2**16)+16*(2**8)+232+(4*opts.fid) 
    source_ip3 = 10*(2**24)+17*(2**16)+16*(2**8)+233+(4*opts.fid) 

try:
    lh = corr.log_handlers.DebugLogHandler()
    logger = logging.getLogger(roach)
    logger.addHandler(lh)
    logger.setLevel(10)

    print('Connecting to server %s... '%(roach)),
    fpga = corr.katcp_wrapper.FpgaClient(roach, logger=logger)
    time.sleep(1)

    if fpga.is_connected():
       print 'ok\n'
    else:
       print 'ERROR connecting to server %s.\n'%(roach)
       exit_fail()
    
    if not opts.noprogram:
       print '------------------------'
       print 'Programming ROACH: %s'%(roach) 
       sys.stdout.flush()
       fpga.progdev(boffile)
       time.sleep(1)
       print 'done. \n'
       
    if (opts.reset_trans):
       print 'Resetting the transceivers on: %s.\n'%(roach)
       fpga.write_int('gtxrxreset_in', 0xff)
       fpga.write_int('gtxrxreset_in', 0x00)
       print 'done \n'

    if (opts.set_coef==1):
       print 'Setting coefficients on: %s.\n'%(roach)
       # 000000 111111 #      
       lsbre01_to_pack = np.round(lsbre01*2**29)
       lsbre01_packed  = struct.pack('>512l', *lsbre01_to_pack)   #the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
       fpga.write('lsbre01', lsbre01_packed, 0)          
       
       lsbim01_to_pack = np.round(lsbim01*2**29)
       lsbim01_packed  = struct.pack('>512l', *lsbim01_to_pack)   
       fpga.write('lsbim01', lsbim01_packed, 0) 
  
       usbre01_to_pack = np.round(usbre01*2**29)    
       usbre01_packed  = struct.pack('>512l', *usbre01_to_pack)
       fpga.write('usbre01', usbre01_packed, 0)

       usbim01_to_pack = np.round(usbim01*2**29)
       usbim01_packed  = struct.pack('>512l', *usbim01_to_pack)
       fpga.write('usbim01', usbim01_packed, 0)

       # 222222 333333 #
       lsbre23_to_pack = np.round(lsbre23*2**29)
       lsbre23_packed  = struct.pack('>512l', *lsbre23_to_pack)
       fpga.write('lsbre23', lsbre23_packed, 0)

       lsbim23_to_pack = np.round(lsbim23*2**29)
       lsbim23_packed  = struct.pack('>512l', *lsbim23_to_pack)
       fpga.write('lsbim23', lsbim23_packed, 0)

       usbre23_to_pack = np.round(usbre23*2**29)
       usbre23_packed  = struct.pack('>512l', *usbre23_to_pack)
       fpga.write('usbre23', usbre23_packed, 0)

       usbim23_to_pack = np.round(usbim23*2**29)
       usbim23_packed  = struct.pack('>512l', *usbim23_to_pack)
       fpga.write('usbim23', usbim23_packed, 0)

       # 444444 555555 #
       lsbre45_to_pack = np.round(lsbre45*2**29)
       lsbre45_packed  = struct.pack('>512l', *lsbre45_to_pack)
       fpga.write('lsbre45', lsbre45_packed, 0)

       lsbim45_to_pack = np.round(lsbim45*2**29)
       lsbim45_packed  = struct.pack('>512l',*lsbim45_to_pack)
       fpga.write('lsbim45', lsbim45_packed, 0)

       usbre45_to_pack = np.round(usbre45*2**29)
       usbre45_packed  = struct.pack('>512l',*usbre45_to_pack)
       fpga.write('usbre45', usbre45_packed, 0)

       usbim45_to_pack = np.round(usbim45*2**29)
       usbim45_packed  = struct.pack('>512l',*usbim45_to_pack)
       fpga.write('usbim45', usbim45_packed, 0)

       # 666666 777777 #
       lsbre67_to_pack = np.round(lsbre67*2**29)
       lsbre67_packed  = struct.pack('>512l',*lsbre67_to_pack)
       fpga.write('lsbre67', lsbre67_packed, 0)
 
       lsbim67_to_pack = np.round(lsbim67*2**29)
       lsbim67_packed  = struct.pack('>512l',*lsbim67_to_pack)
       fpga.write('lsbim67', lsbim67_packed, 0)

       usbre67_to_pack = np.round(usbre67*2**29)
       usbre67_packed  = struct.pack('>512l',*usbre67_to_pack)
       fpga.write('usbre67', usbre67_packed, 0)

       usbim67_to_pack = np.round(usbim67*2**29)
       usbim67_packed  = struct.pack('>512l',*usbim67_to_pack)
       fpga.write('usbim67', usbim67_packed, 0)
       print 'done. \n'


    if (opts.set_coef==2):
       print 'Setting coefficients on: %s.\n'%(roach)
       # 000000 111111 #      
       lsbre01_to_pack = np.round(lsbre01_adj*2**29)
       lsbre01_packed  = struct.pack('>512l', *lsbre01_to_pack)   #the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
       fpga.write('lsbre01', lsbre01_packed, 0)          
       
       lsbim01_to_pack = np.round(lsbim01_adj*2**29)
       lsbim01_packed  = struct.pack('>512l', *lsbim01_to_pack)   
       fpga.write('lsbim01', lsbim01_packed, 0) 
  
       usbre01_to_pack = np.round(usbre01_adj*2**29)    
       usbre01_packed  = struct.pack('>512l', *usbre01_to_pack)
       fpga.write('usbre01', usbre01_packed, 0)

       usbim01_to_pack = np.round(usbim01_adj*2**29)
       usbim01_packed  = struct.pack('>512l', *usbim01_to_pack)
       fpga.write('usbim01', usbim01_packed, 0)

       # 222222 333333 #
       lsbre23_to_pack = np.round(lsbre23_adj*2**29)
       lsbre23_packed  = struct.pack('>512l', *lsbre23_to_pack)
       fpga.write('lsbre23', lsbre23_packed, 0)

       lsbim23_to_pack = np.round(lsbim23_adj*2**29)
       lsbim23_packed  = struct.pack('>512l', *lsbim23_to_pack)
       fpga.write('lsbim23', lsbim23_packed, 0)

       usbre23_to_pack = np.round(usbre23_adj*2**29)
       usbre23_packed  = struct.pack('>512l', *usbre23_to_pack)
       fpga.write('usbre23', usbre23_packed, 0)

       usbim23_to_pack = np.round(usbim23_adj*2**29)
       usbim23_packed  = struct.pack('>512l', *usbim23_to_pack)
       fpga.write('usbim23', usbim23_packed, 0)

       # 444444 555555 #
       lsbre45_to_pack = np.round(lsbre45_adj*2**29)
       lsbre45_packed  = struct.pack('>512l', *lsbre45_to_pack)
       fpga.write('lsbre45', lsbre45_packed, 0)

       lsbim45_to_pack = np.round(lsbim45_adj*2**29)
       lsbim45_packed  = struct.pack('>512l',*lsbim45_to_pack)
       fpga.write('lsbim45', lsbim45_packed, 0)

       usbre45_to_pack = np.round(usbre45_adj*2**29)
       usbre45_packed  = struct.pack('>512l',*usbre45_to_pack)
       fpga.write('usbre45', usbre45_packed, 0)

       usbim45_to_pack = np.round(usbim45_adj*2**29)
       usbim45_packed  = struct.pack('>512l',*usbim45_to_pack)
       fpga.write('usbim45', usbim45_packed, 0)

       # 666666 777777 #
       lsbre67_to_pack = np.round(lsbre67_adj*2**29)
       lsbre67_packed  = struct.pack('>512l',*lsbre67_to_pack)
       fpga.write('lsbre67', lsbre67_packed, 0)
 
       lsbim67_to_pack = np.round(lsbim67_adj*2**29)
       lsbim67_packed  = struct.pack('>512l',*lsbim67_to_pack)
       fpga.write('lsbim67', lsbim67_packed, 0)

       usbre67_to_pack = np.round(usbre67_adj*2**29)
       usbre67_packed  = struct.pack('>512l',*usbre67_to_pack)
       fpga.write('usbre67', usbre67_packed, 0)

       usbim67_to_pack = np.round(usbim67_adj*2**29)
       usbim67_packed  = struct.pack('>512l',*usbim67_to_pack)
       fpga.write('usbim67', usbim67_packed, 0)
       print 'done. \n'



    if (opts.set_coef==3):
       print 'Setting coefficients on: %s.\n'%(roach)
       # 000000 111111 #      
       lsbre01_to_pack = np.round(lsbre01_adj_repl*2**29)
       lsbre01_packed  = struct.pack('>512l', *lsbre01_to_pack)   #the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
       fpga.write('lsbre01', lsbre01_packed, 0)          
       
       lsbim01_to_pack = np.round(lsbim01_adj_repl*2**29)
       lsbim01_packed  = struct.pack('>512l', *lsbim01_to_pack)   
       fpga.write('lsbim01', lsbim01_packed, 0) 
  
       usbre01_to_pack = np.round(usbre01_adj_repl*2**29)    
       usbre01_packed  = struct.pack('>512l', *usbre01_to_pack)
       fpga.write('usbre01', usbre01_packed, 0)

       usbim01_to_pack = np.round(usbim01_adj_repl*2**29)
       usbim01_packed  = struct.pack('>512l', *usbim01_to_pack)
       fpga.write('usbim01', usbim01_packed, 0)

       # 222222 333333 #
       lsbre23_to_pack = np.round(lsbre23_adj_repl*2**29)
       lsbre23_packed  = struct.pack('>512l', *lsbre23_to_pack)
       fpga.write('lsbre23', lsbre23_packed, 0)

       lsbim23_to_pack = np.round(lsbim23_adj_repl*2**29)
       lsbim23_packed  = struct.pack('>512l', *lsbim23_to_pack)
       fpga.write('lsbim23', lsbim23_packed, 0)

       usbre23_to_pack = np.round(usbre23_adj_repl*2**29)
       usbre23_packed  = struct.pack('>512l', *usbre23_to_pack)
       fpga.write('usbre23', usbre23_packed, 0)

       usbim23_to_pack = np.round(usbim23_adj_repl*2**29)
       usbim23_packed  = struct.pack('>512l', *usbim23_to_pack)
       fpga.write('usbim23', usbim23_packed, 0)

       # 444444 555555 #
       lsbre45_to_pack = np.round(lsbre45_adj_repl*2**29)
       lsbre45_packed  = struct.pack('>512l', *lsbre45_to_pack)
       fpga.write('lsbre45', lsbre45_packed, 0)

       lsbim45_to_pack = np.round(lsbim45_adj_repl*2**29)
       lsbim45_packed  = struct.pack('>512l',*lsbim45_to_pack)
       fpga.write('lsbim45', lsbim45_packed, 0)

       usbre45_to_pack = np.round(usbre45_adj_repl*2**29)
       usbre45_packed  = struct.pack('>512l',*usbre45_to_pack)
       fpga.write('usbre45', usbre45_packed, 0)

       usbim45_to_pack = np.round(usbim45_adj_repl*2**29)
       usbim45_packed  = struct.pack('>512l',*usbim45_to_pack)
       fpga.write('usbim45', usbim45_packed, 0)

       # 666666 777777 #
       lsbre67_to_pack = np.round(lsbre67_adj_repl*2**29)
       lsbre67_packed  = struct.pack('>512l',*lsbre67_to_pack)
       fpga.write('lsbre67', lsbre67_packed, 0)
 
       lsbim67_to_pack = np.round(lsbim67_adj_repl*2**29)
       lsbim67_packed  = struct.pack('>512l',*lsbim67_to_pack)
       fpga.write('lsbim67', lsbim67_packed, 0)

       usbre67_to_pack = np.round(usbre67_adj_repl*2**29)
       usbre67_packed  = struct.pack('>512l',*usbre67_to_pack)
       fpga.write('usbre67', usbre67_packed, 0)

       usbim67_to_pack = np.round(usbim67_adj_repl*2**29)
       usbim67_packed  = struct.pack('>512l',*usbim67_to_pack)
       fpga.write('usbim67', usbim67_packed, 0)
       print 'done. \n'



if (opts.set_coef==4):
       print 'Setting coefficients on: %s.\n'%(roach)
       # 000000000000000000000 #      
       lsbrefft0x0_to_pack = np.round( bj_lsbrefft0x0*2**29)
       lsbrefft0x0_packed  = struct.pack('>512l', *lsbrefft0x0_to_pack)   #the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
       fpga.write('lsbre0_0', lsbrefft0x0_packed, 0)  

       lsbimfft0x0_to_pack = np.round( bj_lsbimfft0x0*2**29)
       lsbimfft0x0_packed  = struct.pack('>512l', *lsbimfft0x0_to_pack)
       fpga.write('lsbim0_0', lsbimfft0x0_packed, 0)

       usbrefft0x0_to_pack = np.round( bj_usbrefft0x0*2**29)
       usbrefft0x0_packed  = struct.pack('>512l', *usbrefft0x0_to_pack)
       fpga.write('usbre0_0', usbrefft0x0_packed, 0)

       usbimfft0x0_to_pack = np.round( bj_usbimfft0x0*2**29)
       usbimfft0x0_packed  = struct.pack('>512l', *usbimfft0x0_to_pack)
       fpga.write('usbim0_0', usbimfft0x0_packed, 0)

       # 11111111111111111111111 #      
       lsbrefft0x1_to_pack = np.round( bj_lsbrefft0x1*2**29)
       lsbrefft0x1_packed  = struct.pack('>512l', *lsbrefft0x1_to_pack)
       fpga.write('lsbre1_0', lsbrefft0x1_packed, 0)  

       lsbimfft0x1_to_pack = np.round( bj_lsbimfft0x1*2**29)
       lsbimfft0x1_packed  = struct.pack('>512l', *lsbimfft0x1_to_pack)
       fpga.write('lsbim1_0', lsbimfft0x1_packed, 0)

       usbrefft0x1_to_pack = np.round( bj_usbrefft0x1*2**29)
       usbrefft0x1_packed  = struct.pack('>512l', *usbrefft0x1_to_pack)
       fpga.write('usbre1_0', usbrefft0x1_packed, 0)

       usbimfft0x1_to_pack = np.round( bj_usbimfft0x1*2**29)
       usbimfft0x1_packed  = struct.pack('>512l', *usbimfft0x1_to_pack)
       fpga.write('usbim1_0', usbimfft0x1_packed, 0)

       # 2222222222222222222222 #      
       lsbrefft0x2_to_pack = np.round( bj_lsbrefft0x2*2**29)
       lsbrefft0x2_packed  = struct.pack('>512l', *lsbrefft0x2_to_pack)
       fpga.write('lsbre2_0', lsbrefft0x2_packed, 0)  

       lsbimfft0x2_to_pack = np.round( bj_lsbimfft0x2*2**29)
       lsbimfft0x2_packed  = struct.pack('>512l', *lsbimfft0x2_to_pack)
       fpga.write('lsbim2_0', lsbimfft0x2_packed, 0)

       usbrefft0x2_to_pack = np.round( bj_usbrefft0x2*2**29)
       usbrefft0x2_packed  = struct.pack('>512l', *usbrefft0x2_to_pack)
       fpga.write('usbre2_0', usbrefft0x2_packed, 0)

       usbimfft0x2_to_pack = np.round( bj_usbimfft0x2*2**29)
       usbimfft0x2_packed  = struct.pack('>512l', *usbimfft0x2_to_pack)
       fpga.write('usbim2_0', usbimfft0x2_packed, 0)

       # 3333333333333333333333 #      
       lsbrefft0x3_to_pack = np.round( bj_lsbrefft0x3*2**29)
       lsbrefft0x3_packed  = struct.pack('>512l', *lsbrefft0x3_to_pack)
       fpga.write('lsbre3_0', lsbrefft0x3_packed, 0)  

       lsbimfft0x3_to_pack = np.round( bj_lsbimfft0x3*2**29)
       lsbimfft0x3_packed  = struct.pack('>512l', *lsbimfft0x3_to_pack)
       fpga.write('lsbim3_0', lsbimfft0x3_packed, 0)

       usbrefft0x3_to_pack = np.round( bj_usbrefft0x3*2**29)
       usbrefft0x3_packed  = struct.pack('>512l', *usbrefft0x3_to_pack)
       fpga.write('usbre3_0', usbrefft0x3_packed, 0)

       usbimfft0x3_to_pack = np.round( bj_usbimfft0x3*2**29)
       usbimfft0x3_packed  = struct.pack('>512l', *usbimfft0x3_to_pack)
       fpga.write('usbim3_0', usbimfft0x3_packed, 0)

       # 4444444444444444444444 #      
       lsbrefft0x4_to_pack = np.round( bj_lsbrefft0x4*2**29)
       lsbrefft0x4_packed  = struct.pack('>512l', *lsbrefft0x4_to_pack)
       fpga.write('lsbre4_0', lsbrefft0x4_packed, 0)  

       lsbimfft0x4_to_pack = np.round( bj_lsbimfft0x4*2**29)
       lsbimfft0x4_packed  = struct.pack('>512l', *lsbimfft0x4_to_pack)
       fpga.write('lsbim4_0', lsbimfft0x4_packed, 0)

       usbrefft0x4_to_pack = np.round( bj_usbrefft0x4*2**29)
       usbrefft0x4_packed  = struct.pack('>512l', *usbrefft0x4_to_pack)
       fpga.write('usbre4_0', usbrefft0x4_packed, 0)

       usbimfft0x4_to_pack = np.round( bj_usbimfft0x4*2**29)
       usbimfft0x4_packed  = struct.pack('>512l', *usbimfft0x4_to_pack)
       fpga.write('usbim4_0', usbimfft0x4_packed, 0)

       # 5555555555555555555555 #      
       lsbrefft0x5_to_pack = np.round( bj_lsbrefft0x5*2**29)
       lsbrefft0x5_packed  = struct.pack('>512l', *lsbrefft0x5_to_pack)
       fpga.write('lsbre5_0', lsbrefft0x5_packed, 0)  

       lsbimfft0x5_to_pack = np.round( bj_lsbimfft0x5*2**29)
       lsbimfft0x5_packed  = struct.pack('>512l', *lsbimfft0x5_to_pack)
       fpga.write('lsbim5_0', lsbimfft0x5_packed, 0)

       usbrefft0x5_to_pack = np.round( bj_usbrefft0x5*2**29)
       usbrefft0x5_packed  = struct.pack('>512l', *usbrefft0x5_to_pack)
       fpga.write('usbre5_0', usbrefft0x5_packed, 0)

       usbimfft0x5_to_pack = np.round( bj_usbimfft0x5*2**29)
       usbimfft0x5_packed  = struct.pack('>512l', *usbimfft0x5_to_pack)
       fpga.write('usbim5_0', usbimfft0x5_packed, 0)

       # 6666666666666666666666 #      
       lsbrefft0x6_to_pack = np.round( bj_lsbrefft0x6*2**29)
       lsbrefft0x6_packed  = struct.pack('>512l', *lsbrefft0x6_to_pack)
       fpga.write('lsbre6_0', lsbrefft0x6_packed, 0)  

       lsbimfft0x6_to_pack = np.round( bj_lsbimfft0x6*2**29)
       lsbimfft0x6_packed  = struct.pack('>512l', *lsbimfft0x6_to_pack)
       fpga.write('lsbim6_0', lsbimfft0x6_packed, 0)

       usbrefft0x6_to_pack = np.round( bj_usbrefft0x6*2**29)
       usbrefft0x6_packed  = struct.pack('>512l', *usbrefft0x6_to_pack)
       fpga.write('usbre6_0', usbrefft0x6_packed, 0)

       usbimfft0x6_to_pack = np.round( bj_usbimfft0x6*2**29)
       usbimfft0x6_packed  = struct.pack('>512l', *usbimfft0x6_to_pack)
       fpga.write('usbim6_0', usbimfft0x6_packed, 0)

       # 7777777777777777777777 #      
       lsbrefft0x7_to_pack = np.round( bj_lsbrefft0x7*2**29)
       lsbrefft0x7_packed  = struct.pack('>512l', *lsbrefft0x7_to_pack)
       fpga.write('lsbre7_0', lsbrefft0x7_packed, 0)  

       lsbimfft0x7_to_pack = np.round( bj_lsbimfft0x7*2**29)
       lsbimfft0x7_packed  = struct.pack('>512l', *lsbimfft0x7_to_pack)
       fpga.write('lsbim7_0', lsbimfft0x7_packed, 0)

       usbrefft0x7_to_pack = np.round( bj_usbrefft0x7*2**29)
       usbrefft0x7_packed  = struct.pack('>512l', *usbrefft0x7_to_pack)
       fpga.write('usbre7_0', usbrefft0x7_packed, 0)

       usbimfft0x7_to_pack = np.round( bj_usbimfft0x7*2**29)
       usbimfft0x7_packed  = struct.pack('>512l', *usbimfft0x7_to_pack)
       fpga.write('usbim7_0', usbimfft0x7_packed, 0)

       # 000000000000000000000 #      
       lsbrefft1x0_to_pack = np.round( bj_lsbrefft1x0*2**29)
       lsbrefft1x0_packed  = struct.pack('>512l', *lsbrefft1x0_to_pack)   #the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
       fpga.write('lsbre0_1', lsbrefft1x0_packed, 0)  

       lsbimfft1x0_to_pack = np.round( bj_lsbimfft1x0*2**29)
       lsbimfft1x0_packed  = struct.pack('>512l', *lsbimfft1x0_to_pack)
       fpga.write('lsbim0_1', lsbimfft1x0_packed, 0)

       usbrefft1x0_to_pack = np.round( bj_usbrefft1x0*2**29)
       usbrefft1x0_packed  = struct.pack('>512l', *usbrefft1x0_to_pack)
       fpga.write('usbre0_1', usbrefft1x0_packed, 0)

       usbimfft1x0_to_pack = np.round( bj_usbimfft1x0*2**29)
       usbimfft1x0_packed  = struct.pack('>512l', *usbimfft1x0_to_pack)
       fpga.write('usbim0_1', usbimfft1x0_packed, 0)

       # 11111111111111111111111 #      
       lsbrefft1x1_to_pack = np.round( bj_lsbrefft1x1*2**29)
       lsbrefft1x1_packed  = struct.pack('>512l', *lsbrefft1x1_to_pack)
       fpga.write('lsbre1_1', lsbrefft1x1_packed, 0)  

       lsbimfft1x1_to_pack = np.round( bj_lsbimfft1x1*2**29)
       lsbimfft1x1_packed  = struct.pack('>512l', *lsbimfft1x1_to_pack)
       fpga.write('lsbim1_1', lsbimfft1x1_packed, 0)

       usbrefft1x1_to_pack = np.round( bj_usbrefft1x1*2**29)
       usbrefft1x1_packed  = struct.pack('>512l', *usbrefft1x1_to_pack)
       fpga.write('usbre1_1', usbrefft1x1_packed, 0)

       usbimfft1x1_to_pack = np.round( bj_usbimfft1x1*2**29)
       usbimfft1x1_packed  = struct.pack('>512l', *usbimfft1x1_to_pack)
       fpga.write('usbim1_1', usbimfft1x1_packed, 0)

       # 2222222222222222222222 #      
       lsbrefft1x2_to_pack = np.round( bj_lsbrefft1x2*2**29)
       lsbrefft1x2_packed  = struct.pack('>512l', *lsbrefft1x2_to_pack)
       fpga.write('lsbre2_1', lsbrefft1x2_packed, 0)  

       lsbimfft1x2_to_pack = np.round( bj_lsbimfft1x2*2**29)
       lsbimfft1x2_packed  = struct.pack('>512l', *lsbimfft1x2_to_pack)
       fpga.write('lsbim2_1', lsbimfft1x2_packed, 0)

       usbrefft1x2_to_pack = np.round( bj_usbrefft1x2*2**29)
       usbrefft1x2_packed  = struct.pack('>512l', *usbrefft1x2_to_pack)
       fpga.write('usbre2_1', usbrefft1x2_packed, 0)

       usbimfft1x2_to_pack = np.round( bj_usbimfft1x2*2**29)
       usbimfft1x2_packed  = struct.pack('>512l', *usbimfft1x2_to_pack)
       fpga.write('usbim2_1', usbimfft1x2_packed, 0)

       # 3333333333333333333333 #      
       lsbrefft1x3_to_pack = np.round( bj_lsbrefft1x3*2**29)
       lsbrefft1x3_packed  = struct.pack('>512l', *lsbrefft1x3_to_pack)
       fpga.write('lsbre3_1', lsbrefft1x3_packed, 0)  

       lsbimfft1x3_to_pack = np.round( bj_lsbimfft1x3*2**29)
       lsbimfft1x3_packed  = struct.pack('>512l', *lsbimfft1x3_to_pack)
       fpga.write('lsbim3_1', lsbimfft1x3_packed, 0)

       usbrefft1x3_to_pack = np.round( bj_usbrefft1x3*2**29)
       usbrefft1x3_packed  = struct.pack('>512l', *usbrefft1x3_to_pack)
       fpga.write('usbre3_1', usbrefft1x3_packed, 0)

       usbimfft1x3_to_pack = np.round( bj_usbimfft1x3*2**29)
       usbimfft1x3_packed  = struct.pack('>512l', *usbimfft1x3_to_pack)
       fpga.write('usbim3_1', usbimfft1x3_packed, 0)

       # 4444444444444444444444 #      
       lsbrefft1x4_to_pack = np.round( bj_lsbrefft1x4*2**29)
       lsbrefft1x4_packed  = struct.pack('>512l', *lsbrefft1x4_to_pack)
       fpga.write('lsbre4_1', lsbrefft1x4_packed, 0)  

       lsbimfft1x4_to_pack = np.round( bj_lsbimfft1x4*2**29)
       lsbimfft1x4_packed  = struct.pack('>512l', *lsbimfft1x4_to_pack)
       fpga.write('lsbim4_1', lsbimfft1x4_packed, 0)

       usbrefft1x4_to_pack = np.round( bj_usbrefft1x4*2**29)
       usbrefft1x4_packed  = struct.pack('>512l', *usbrefft1x4_to_pack)
       fpga.write('usbre4_1', usbrefft1x4_packed, 0)

       usbimfft1x4_to_pack = np.round( bj_usbimfft1x4*2**29)
       usbimfft1x4_packed  = struct.pack('>512l', *usbimfft1x4_to_pack)
       fpga.write('usbim4_1', usbimfft1x4_packed, 0)

       # 5555555555555555555555 #      
       lsbrefft1x5_to_pack = np.round( bj_lsbrefft1x5*2**29)
       lsbrefft1x5_packed  = struct.pack('>512l', *lsbrefft1x5_to_pack)
       fpga.write('lsbre5_1', lsbrefft1x5_packed, 0)  

       lsbimfft1x5_to_pack = np.round( bj_lsbimfft1x5*2**29)
       lsbimfft1x5_packed  = struct.pack('>512l', *lsbimfft1x5_to_pack)
       fpga.write('lsbim5_1', lsbimfft1x5_packed, 0)

       usbrefft1x5_to_pack = np.round( bj_usbrefft1x5*2**29)
       usbrefft1x5_packed  = struct.pack('>512l', *usbrefft1x5_to_pack)
       fpga.write('usbre5_1', usbrefft1x5_packed, 0)

       usbimfft1x5_to_pack = np.round( bj_usbimfft1x5*2**29)
       usbimfft1x5_packed  = struct.pack('>512l', *usbimfft1x5_to_pack)
       fpga.write('usbim5_1', usbimfft1x5_packed, 0)

       # 6666666666666666666666 #      
       lsbrefft1x6_to_pack = np.round( bj_lsbrefft1x6*2**29)
       lsbrefft1x6_packed  = struct.pack('>512l', *lsbrefft1x6_to_pack)
       fpga.write('lsbre6_1', lsbrefft1x6_packed, 0)  

       lsbimfft1x6_to_pack = np.round( bj_lsbimfft1x6*2**29)
       lsbimfft1x6_packed  = struct.pack('>512l', *lsbimfft1x6_to_pack)
       fpga.write('lsbim6_1', lsbimfft1x6_packed, 0)

       usbrefft1x6_to_pack = np.round( bj_usbrefft1x6*2**29)
       usbrefft1x6_packed  = struct.pack('>512l', *usbrefft1x6_to_pack)
       fpga.write('usbre6_1', usbrefft1x6_packed, 0)

       usbimfft1x6_to_pack = np.round( bj_usbimfft1x6*2**29)
       usbimfft1x6_packed  = struct.pack('>512l', *usbimfft1x6_to_pack)
       fpga.write('usbim6_1', usbimfft1x6_packed, 0)

       # 7777777777777777777777 #      
       lsbrefft1x7_to_pack = np.round( bj_lsbrefft1x7*2**29)
       lsbrefft1x7_packed  = struct.pack('>512l', *lsbrefft1x7_to_pack)
       fpga.write('lsbre7_1', lsbrefft1x7_packed, 0)  

       lsbimfft1x7_to_pack = np.round( bj_lsbimfft1x7*2**29)
       lsbimfft1x7_packed  = struct.pack('>512l', *lsbimfft1x7_to_pack)
       fpga.write('lsbim7_1', lsbimfft1x7_packed, 0)

       usbrefft1x7_to_pack = np.round( bj_usbrefft1x7*2**29)
       usbrefft1x7_packed  = struct.pack('>512l', *usbrefft1x7_to_pack)
       fpga.write('usbre7_1', usbrefft1x7_packed, 0)

       usbimfft1x7_to_pack = np.round( bj_usbimfft1x7*2**29)
       usbimfft1x7_packed  = struct.pack('>512l', *usbimfft1x7_to_pack)
       fpga.write('usbim7_1', usbimfft1x7_packed, 0)



       # 000000000000000000000 #      
       lsbrefft2x0_to_pack = np.round( bj_lsbrefft2x0*2**29)
       lsbrefft2x0_packed  = struct.pack('>512l', *lsbrefft2x0_to_pack)   #the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
       fpga.write('lsbre0_2', lsbrefft2x0_packed, 0)  

       lsbimfft2x0_to_pack = np.round( bj_lsbimfft2x0*2**29)
       lsbimfft2x0_packed  = struct.pack('>512l', *lsbimfft2x0_to_pack)
       fpga.write('lsbim0_2', lsbimfft2x0_packed, 0)

       usbrefft2x0_to_pack = np.round( bj_usbrefft2x0*2**29)
       usbrefft2x0_packed  = struct.pack('>512l', *usbrefft2x0_to_pack)
       fpga.write('usbre0_2', usbrefft2x0_packed, 0)

       usbimfft2x0_to_pack = np.round( bj_usbimfft2x0*2**29)
       usbimfft2x0_packed  = struct.pack('>512l', *usbimfft2x0_to_pack)
       fpga.write('usbim0_2', usbimfft2x0_packed, 0)

       # 11111111111111111111111 #      
       lsbrefft2x1_to_pack = np.round( bj_lsbrefft2x1*2**29)
       lsbrefft2x1_packed  = struct.pack('>512l', *lsbrefft2x1_to_pack)
       fpga.write('lsbre1_2', lsbrefft2x1_packed, 0)  

       lsbimfft2x1_to_pack = np.round( bj_lsbimfft2x1*2**29)
       lsbimfft2x1_packed  = struct.pack('>512l', *lsbimfft2x1_to_pack)
       fpga.write('lsbim1_2', lsbimfft2x1_packed, 0)

       usbrefft2x1_to_pack = np.round( bj_usbrefft2x1*2**29)
       usbrefft2x1_packed  = struct.pack('>512l', *usbrefft2x1_to_pack)
       fpga.write('usbre1_2', usbrefft2x1_packed, 0)

       usbimfft2x1_to_pack = np.round( bj_usbimfft2x1*2**29)
       usbimfft2x1_packed  = struct.pack('>512l', *usbimfft2x1_to_pack)
       fpga.write('usbim1_2', usbimfft2x1_packed, 0)

       # 2222222222222222222222 #      
       lsbrefft2x2_to_pack = np.round( bj_lsbrefft2x2*2**29)
       lsbrefft2x2_packed  = struct.pack('>512l', *lsbrefft2x2_to_pack)
       fpga.write('lsbre2_2', lsbrefft2x2_packed, 0)  

       lsbimfft2x2_to_pack = np.round( bj_lsbimfft2x2*2**29)
       lsbimfft2x2_packed  = struct.pack('>512l', *lsbimfft2x2_to_pack)
       fpga.write('lsbim2_2', lsbimfft2x2_packed, 0)

       usbrefft2x2_to_pack = np.round( bj_usbrefft2x2*2**29)
       usbrefft2x2_packed  = struct.pack('>512l', *usbrefft2x2_to_pack)
       fpga.write('usbre2_2', usbrefft2x2_packed, 0)

       usbimfft2x2_to_pack = np.round( bj_usbimfft2x2*2**29)
       usbimfft2x2_packed  = struct.pack('>512l', *usbimfft2x2_to_pack)
       fpga.write('usbim2_2', usbimfft2x2_packed, 0)

       # 3333333333333333333333 #      
       lsbrefft2x3_to_pack = np.round( bj_lsbrefft2x3*2**29)
       lsbrefft2x3_packed  = struct.pack('>512l', *lsbrefft2x3_to_pack)
       fpga.write('lsbre3_2', lsbrefft2x3_packed, 0)  

       lsbimfft2x3_to_pack = np.round( bj_lsbimfft2x3*2**29)
       lsbimfft2x3_packed  = struct.pack('>512l', *lsbimfft2x3_to_pack)
       fpga.write('lsbim3_2', lsbimfft2x3_packed, 0)

       usbrefft2x3_to_pack = np.round( bj_usbrefft2x3*2**29)
       usbrefft2x3_packed  = struct.pack('>512l', *usbrefft2x3_to_pack)
       fpga.write('usbre3_2', usbrefft2x3_packed, 0)

       usbimfft2x3_to_pack = np.round( bj_usbimfft2x3*2**29)
       usbimfft2x3_packed  = struct.pack('>512l', *usbimfft2x3_to_pack)
       fpga.write('usbim3_2', usbimfft2x3_packed, 0)

       # 4444444444444444444444 #      
       lsbrefft2x4_to_pack = np.round( bj_lsbrefft2x4*2**29)
       lsbrefft2x4_packed  = struct.pack('>512l', *lsbrefft2x4_to_pack)
       fpga.write('lsbre4_2', lsbrefft2x4_packed, 0)  

       lsbimfft2x4_to_pack = np.round( bj_lsbimfft2x4*2**29)
       lsbimfft2x4_packed  = struct.pack('>512l', *lsbimfft2x4_to_pack)
       fpga.write('lsbim4_2', lsbimfft2x4_packed, 0)

       usbrefft2x4_to_pack = np.round( bj_usbrefft2x4*2**29)
       usbrefft2x4_packed  = struct.pack('>512l', *usbrefft2x4_to_pack)
       fpga.write('usbre4_2', usbrefft2x4_packed, 0)

       usbimfft2x4_to_pack = np.round( bj_usbimfft2x4*2**29)
       usbimfft2x4_packed  = struct.pack('>512l', *usbimfft2x4_to_pack)
       fpga.write('usbim4_2', usbimfft2x4_packed, 0)

       # 5555555555555555555555 #      
       lsbrefft2x5_to_pack = np.round( bj_lsbrefft2x5*2**29)
       lsbrefft2x5_packed  = struct.pack('>512l', *lsbrefft2x5_to_pack)
       fpga.write('lsbre5_2', lsbrefft2x5_packed, 0)  

       lsbimfft2x5_to_pack = np.round( bj_lsbimfft2x5*2**29)
       lsbimfft2x5_packed  = struct.pack('>512l', *lsbimfft2x5_to_pack)
       fpga.write('lsbim5_2', lsbimfft2x5_packed, 0)

       usbrefft2x5_to_pack = np.round( bj_usbrefft2x5*2**29)
       usbrefft2x5_packed  = struct.pack('>512l', *usbrefft2x5_to_pack)
       fpga.write('usbre5_2', usbrefft2x5_packed, 0)

       usbimfft2x5_to_pack = np.round( bj_usbimfft2x5*2**29)
       usbimfft2x5_packed  = struct.pack('>512l', *usbimfft2x5_to_pack)
       fpga.write('usbim5_2', usbimfft2x5_packed, 0)

       # 6666666666666666666666 #      
       lsbrefft2x6_to_pack = np.round( bj_lsbrefft2x6*2**29)
       lsbrefft2x6_packed  = struct.pack('>512l', *lsbrefft2x6_to_pack)
       fpga.write('lsbre6_2', lsbrefft2x6_packed, 0)  

       lsbimfft2x6_to_pack = np.round( bj_lsbimfft2x6*2**29)
       lsbimfft2x6_packed  = struct.pack('>512l', *lsbimfft2x6_to_pack)
       fpga.write('lsbim6_2', lsbimfft2x6_packed, 0)

       usbrefft2x6_to_pack = np.round( bj_usbrefft2x6*2**29)
       usbrefft2x6_packed  = struct.pack('>512l', *usbrefft2x6_to_pack)
       fpga.write('usbre6_2', usbrefft2x6_packed, 0)

       usbimfft2x6_to_pack = np.round( bj_usbimfft2x6*2**29)
       usbimfft2x6_packed  = struct.pack('>512l', *usbimfft2x6_to_pack)
       fpga.write('usbim6_2', usbimfft2x6_packed, 0)

       # 7777777777777777777777 #      
       lsbrefft2x7_to_pack = np.round( bj_lsbrefft2x7*2**29)
       lsbrefft2x7_packed  = struct.pack('>512l', *lsbrefft2x7_to_pack)
       fpga.write('lsbre7_2', lsbrefft2x7_packed, 0)  

       lsbimfft2x7_to_pack = np.round( bj_lsbimfft2x7*2**29)
       lsbimfft2x7_packed  = struct.pack('>512l', *lsbimfft2x7_to_pack)
       fpga.write('lsbim7_2', lsbimfft2x7_packed, 0)

       usbrefft2x7_to_pack = np.round( bj_usbrefft2x7*2**29)
       usbrefft2x7_packed  = struct.pack('>512l', *usbrefft2x7_to_pack)
       fpga.write('usbre7_2', usbrefft2x7_packed, 0)

       usbimfft2x7_to_pack = np.round( bj_usbimfft2x7*2**29)
       usbimfft2x7_packed  = struct.pack('>512l', *usbimfft2x7_to_pack)
       fpga.write('usbim7_2', usbimfft2x7_packed, 0)



       # 000000000000000000000 #      
       lsbrefft3x0_to_pack = np.round( bj_lsbrefft3x0*2**29)
       lsbrefft3x0_packed  = struct.pack('>512l', *lsbrefft3x0_to_pack)   #the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
       fpga.write('lsbre0_3', lsbrefft3x0_packed, 0)  

       lsbimfft3x0_to_pack = np.round( bj_lsbimfft3x0*2**29)
       lsbimfft3x0_packed  = struct.pack('>512l', *lsbimfft3x0_to_pack)
       fpga.write('lsbim0_3', lsbimfft3x0_packed, 0)

       usbrefft3x0_to_pack = np.round( bj_usbrefft3x0*2**29)
       usbrefft3x0_packed  = struct.pack('>512l', *usbrefft3x0_to_pack)
       fpga.write('usbre0_3', usbrefft3x0_packed, 0)

       usbimfft3x0_to_pack = np.round( bj_usbimfft3x0*2**29)
       usbimfft3x0_packed  = struct.pack('>512l', *usbimfft3x0_to_pack)
       fpga.write('usbim0_3', usbimfft3x0_packed, 0)

       # 11111111111111111111111 #      
       lsbrefft3x1_to_pack = np.round( bj_lsbrefft3x1*2**29)
       lsbrefft3x1_packed  = struct.pack('>512l', *lsbrefft3x1_to_pack)
       fpga.write('lsbre1_3', lsbrefft3x1_packed, 0)  

       lsbimfft3x1_to_pack = np.round( bj_lsbimfft3x1*2**29)
       lsbimfft3x1_packed  = struct.pack('>512l', *lsbimfft3x1_to_pack)
       fpga.write('lsbim1_3', lsbimfft3x1_packed, 0)

       usbrefft3x1_to_pack = np.round( bj_usbrefft3x1*2**29)
       usbrefft3x1_packed  = struct.pack('>512l', *usbrefft3x1_to_pack)
       fpga.write('usbre1_3', usbrefft3x1_packed, 0)

       usbimfft3x1_to_pack = np.round( bj_usbimfft3x1*2**29)
       usbimfft3x1_packed  = struct.pack('>512l', *usbimfft3x1_to_pack)
       fpga.write('usbim1_3', usbimfft3x1_packed, 0)

       # 2222222222222222222222 #      
       lsbrefft3x2_to_pack = np.round( bj_lsbrefft3x2*2**29)
       lsbrefft3x2_packed  = struct.pack('>512l', *lsbrefft3x2_to_pack)
       fpga.write('lsbre2_3', lsbrefft3x2_packed, 0)  

       lsbimfft3x2_to_pack = np.round( bj_lsbimfft3x2*2**29)
       lsbimfft3x2_packed  = struct.pack('>512l', *lsbimfft3x2_to_pack)
       fpga.write('lsbim2_3', lsbimfft3x2_packed, 0)

       usbrefft3x2_to_pack = np.round( bj_usbrefft3x2*2**29)
       usbrefft3x2_packed  = struct.pack('>512l', *usbrefft3x2_to_pack)
       fpga.write('usbre2_3', usbrefft3x2_packed, 0)

       usbimfft3x2_to_pack = np.round( bj_usbimfft3x2*2**29)
       usbimfft3x2_packed  = struct.pack('>512l', *usbimfft3x2_to_pack)
       fpga.write('usbim2_3', usbimfft3x2_packed, 0)

       # 3333333333333333333333 #      
       lsbrefft3x3_to_pack = np.round( bj_lsbrefft3x3*2**29)
       lsbrefft3x3_packed  = struct.pack('>512l', *lsbrefft3x3_to_pack)
       fpga.write('lsbre3_3', lsbrefft3x3_packed, 0)  

       lsbimfft3x3_to_pack = np.round( bj_lsbimfft3x3*2**29)
       lsbimfft3x3_packed  = struct.pack('>512l', *lsbimfft3x3_to_pack)
       fpga.write('lsbim3_3', lsbimfft3x3_packed, 0)

       usbrefft3x3_to_pack = np.round( bj_usbrefft3x3*2**29)
       usbrefft3x3_packed  = struct.pack('>512l', *usbrefft3x3_to_pack)
       fpga.write('usbre3_3', usbrefft3x3_packed, 0)

       usbimfft3x3_to_pack = np.round( bj_usbimfft3x3*2**29)
       usbimfft3x3_packed  = struct.pack('>512l', *usbimfft3x3_to_pack)
       fpga.write('usbim3_3', usbimfft3x3_packed, 0)

       # 4444444444444444444444 #      
       lsbrefft3x4_to_pack = np.round( bj_lsbrefft3x4*2**29)
       lsbrefft3x4_packed  = struct.pack('>512l', *lsbrefft3x4_to_pack)
       fpga.write('lsbre4_3', lsbrefft3x4_packed, 0)  

       lsbimfft3x4_to_pack = np.round( bj_lsbimfft3x4*2**29)
       lsbimfft3x4_packed  = struct.pack('>512l', *lsbimfft3x4_to_pack)
       fpga.write('lsbim4_3', lsbimfft3x4_packed, 0)

       usbrefft3x4_to_pack = np.round( bj_usbrefft3x4*2**29)
       usbrefft3x4_packed  = struct.pack('>512l', *usbrefft3x4_to_pack)
       fpga.write('usbre4_3', usbrefft3x4_packed, 0)

       usbimfft3x4_to_pack = np.round( bj_usbimfft3x4*2**29)
       usbimfft3x4_packed  = struct.pack('>512l', *usbimfft3x4_to_pack)
       fpga.write('usbim4_3', usbimfft3x4_packed, 0)

       # 5555555555555555555555 #      
       lsbrefft3x5_to_pack = np.round( bj_lsbrefft3x5*2**29)
       lsbrefft3x5_packed  = struct.pack('>512l', *lsbrefft3x5_to_pack)
       fpga.write('lsbre5_3', lsbrefft3x5_packed, 0)  

       lsbimfft3x5_to_pack = np.round( bj_lsbimfft3x5*2**29)
       lsbimfft3x5_packed  = struct.pack('>512l', *lsbimfft3x5_to_pack)
       fpga.write('lsbim5_3', lsbimfft3x5_packed, 0)

       usbrefft3x5_to_pack = np.round( bj_usbrefft3x5*2**29)
       usbrefft3x5_packed  = struct.pack('>512l', *usbrefft3x5_to_pack)
       fpga.write('usbre5_3', usbrefft3x5_packed, 0)

       usbimfft3x5_to_pack = np.round( bj_usbimfft3x5*2**29)
       usbimfft3x5_packed  = struct.pack('>512l', *usbimfft3x5_to_pack)
       fpga.write('usbim5_3', usbimfft3x5_packed, 0)

       # 6666666666666666666666 #      
       lsbrefft3x6_to_pack = np.round( bj_lsbrefft3x6*2**29)
       lsbrefft3x6_packed  = struct.pack('>512l', *lsbrefft3x6_to_pack)
       fpga.write('lsbre6_3', lsbrefft3x6_packed, 0)  

       lsbimfft3x6_to_pack = np.round( bj_lsbimfft3x6*2**29)
       lsbimfft3x6_packed  = struct.pack('>512l', *lsbimfft3x6_to_pack)
       fpga.write('lsbim6_3', lsbimfft3x6_packed, 0)

       usbrefft3x6_to_pack = np.round( bj_usbrefft3x6*2**29)
       usbrefft3x6_packed  = struct.pack('>512l', *usbrefft3x6_to_pack)
       fpga.write('usbre6_3', usbrefft3x6_packed, 0)

       usbimfft3x6_to_pack = np.round( bj_usbimfft3x6*2**29)
       usbimfft3x6_packed  = struct.pack('>512l', *usbimfft3x6_to_pack)
       fpga.write('usbim6_3', usbimfft3x6_packed, 0)

       # 7777777777777777777777 #      
       lsbrefft3x7_to_pack = np.round( bj_lsbrefft3x7*2**29)
       lsbrefft3x7_packed  = struct.pack('>512l', *lsbrefft3x7_to_pack)
       fpga.write('lsbre7_3', lsbrefft3x7_packed, 0)  

       lsbimfft3x7_to_pack = np.round( bj_lsbimfft3x7*2**29)
       lsbimfft3x7_packed  = struct.pack('>512l', *lsbimfft3x7_to_pack)
       fpga.write('lsbim7_3', lsbimfft3x7_packed, 0)

       usbrefft3x7_to_pack = np.round( bj_usbrefft3x7*2**29)
       usbrefft3x7_packed  = struct.pack('>512l', *usbrefft3x7_to_pack)
       fpga.write('usbre7_3', usbrefft3x7_packed, 0)

       usbimfft3x7_to_pack = np.round( bj_usbimfft3x7*2**29)
       usbimfft3x7_packed  = struct.pack('>512l', *usbimfft3x7_to_pack)
       fpga.write('usbim7_3', usbimfft3x7_packed, 0)



       # 000000000000000000000 #      
       lsbrefft4x0_to_pack = np.round( bj_lsbrefft4x0*2**29)
       lsbrefft4x0_packed  = struct.pack('>512l', *lsbrefft4x0_to_pack)   #the star is there so that it actually reads lsbre01_topack[0] lsbre01_topack[1] lsbre01_topack[2] .... lsbre01_topack[512]
       fpga.write('lsbre0_4', lsbrefft4x0_packed, 0)  

       lsbimfft4x0_to_pack = np.round( bj_lsbimfft4x0*2**29)
       lsbimfft4x0_packed  = struct.pack('>512l', *lsbimfft4x0_to_pack)
       fpga.write('lsbim0_4', lsbimfft4x0_packed, 0)

       usbrefft4x0_to_pack = np.round( bj_usbrefft4x0*2**29)
       usbrefft4x0_packed  = struct.pack('>512l', *usbrefft4x0_to_pack)
       fpga.write('usbre0_4', usbrefft4x0_packed, 0)

       usbimfft4x0_to_pack = np.round( bj_usbimfft4x0*2**29)
       usbimfft4x0_packed  = struct.pack('>512l', *usbimfft4x0_to_pack)
       fpga.write('usbim0_4', usbimfft4x0_packed, 0)

       # 11111111111111111111111 #      
       lsbrefft4x1_to_pack = np.round( bj_lsbrefft4x1*2**29)
       lsbrefft4x1_packed  = struct.pack('>512l', *lsbrefft4x1_to_pack)
       fpga.write('lsbre1_4', lsbrefft4x1_packed, 0)  

       lsbimfft4x1_to_pack = np.round( bj_lsbimfft4x1*2**29)
       lsbimfft4x1_packed  = struct.pack('>512l', *lsbimfft4x1_to_pack)
       fpga.write('lsbim1_4', lsbimfft4x1_packed, 0)

       usbrefft4x1_to_pack = np.round( bj_usbrefft4x1*2**29)
       usbrefft4x1_packed  = struct.pack('>512l', *usbrefft4x1_to_pack)
       fpga.write('usbre1_4', usbrefft4x1_packed, 0)

       usbimfft4x1_to_pack = np.round( bj_usbimfft4x1*2**29)
       usbimfft4x1_packed  = struct.pack('>512l', *usbimfft4x1_to_pack)
       fpga.write('usbim1_4', usbimfft4x1_packed, 0)

       # 2222222222222222222222 #      
       lsbrefft4x2_to_pack = np.round( bj_lsbrefft4x2*2**29)
       lsbrefft4x2_packed  = struct.pack('>512l', *lsbrefft4x2_to_pack)
       fpga.write('lsbre2_4', lsbrefft4x2_packed, 0)  

       lsbimfft4x2_to_pack = np.round( bj_lsbimfft4x2*2**29)
       lsbimfft4x2_packed  = struct.pack('>512l', *lsbimfft4x2_to_pack)
       fpga.write('lsbim2_4', lsbimfft4x2_packed, 0)

       usbrefft4x2_to_pack = np.round( bj_usbrefft4x2*2**29)
       usbrefft4x2_packed  = struct.pack('>512l', *usbrefft4x2_to_pack)
       fpga.write('usbre2_4', usbrefft4x2_packed, 0)

       usbimfft4x2_to_pack = np.round( bj_usbimfft4x2*2**29)
       usbimfft4x2_packed  = struct.pack('>512l', *usbimfft4x2_to_pack)
       fpga.write('usbim2_4', usbimfft4x2_packed, 0)

       # 3333333333333333333333 #      
       lsbrefft4x3_to_pack = np.round( bj_lsbrefft4x3*2**29)
       lsbrefft4x3_packed  = struct.pack('>512l', *lsbrefft4x3_to_pack)
       fpga.write('lsbre3_4', lsbrefft4x3_packed, 0)  

       lsbimfft4x3_to_pack = np.round( bj_lsbimfft4x3*2**29)
       lsbimfft4x3_packed  = struct.pack('>512l', *lsbimfft4x3_to_pack)
       fpga.write('lsbim3_4', lsbimfft4x3_packed, 0)

       usbrefft4x3_to_pack = np.round( bj_usbrefft4x3*2**29)
       usbrefft4x3_packed  = struct.pack('>512l', *usbrefft4x3_to_pack)
       fpga.write('usbre3_4', usbrefft4x3_packed, 0)

       usbimfft4x3_to_pack = np.round( bj_usbimfft4x3*2**29)
       usbimfft4x3_packed  = struct.pack('>512l', *usbimfft4x3_to_pack)
       fpga.write('usbim3_4', usbimfft4x3_packed, 0)

       # 4444444444444444444444 #      
       lsbrefft4x4_to_pack = np.round( bj_lsbrefft4x4*2**29)
       lsbrefft4x4_packed  = struct.pack('>512l', *lsbrefft4x4_to_pack)
       fpga.write('lsbre4_4', lsbrefft4x4_packed, 0)  

       lsbimfft4x4_to_pack = np.round( bj_lsbimfft4x4*2**29)
       lsbimfft4x4_packed  = struct.pack('>512l', *lsbimfft4x4_to_pack)
       fpga.write('lsbim4_4', lsbimfft4x4_packed, 0)

       usbrefft4x4_to_pack = np.round( bj_usbrefft4x4*2**29)
       usbrefft4x4_packed  = struct.pack('>512l', *usbrefft4x4_to_pack)
       fpga.write('usbre4_4', usbrefft4x4_packed, 0)

       usbimfft4x4_to_pack = np.round( bj_usbimfft4x4*2**29)
       usbimfft4x4_packed  = struct.pack('>512l', *usbimfft4x4_to_pack)
       fpga.write('usbim4_4', usbimfft4x4_packed, 0)

       # 5555555555555555555555 #      
       lsbrefft4x5_to_pack = np.round( bj_lsbrefft4x5*2**29)
       lsbrefft4x5_packed  = struct.pack('>512l', *lsbrefft4x5_to_pack)
       fpga.write('lsbre5_4', lsbrefft4x5_packed, 0)  

       lsbimfft4x5_to_pack = np.round( bj_lsbimfft4x5*2**29)
       lsbimfft4x5_packed  = struct.pack('>512l', *lsbimfft4x5_to_pack)
       fpga.write('lsbim5_4', lsbimfft4x5_packed, 0)

       usbrefft4x5_to_pack = np.round( bj_usbrefft4x5*2**29)
       usbrefft4x5_packed  = struct.pack('>512l', *usbrefft4x5_to_pack)
       fpga.write('usbre5_4', usbrefft4x5_packed, 0)

       usbimfft4x5_to_pack = np.round( bj_usbimfft4x5*2**29)
       usbimfft4x5_packed  = struct.pack('>512l', *usbimfft4x5_to_pack)
       fpga.write('usbim5_4', usbimfft4x5_packed, 0)

       # 6666666666666666666666 #      
       lsbrefft4x6_to_pack = np.round( bj_lsbrefft14x6*2**29)
       lsbrefft4x6_packed  = struct.pack('>512l', *lsbrefft4x6_to_pack)
       fpga.write('lsbre6_4', lsbrefft4x6_packed, 0)  

       lsbimfft4x6_to_pack = np.round( bj_lsbimfft4x6*2**29)
       lsbimfft4x6_packed  = struct.pack('>512l', *lsbimfft4x6_to_pack)
       fpga.write('lsbim6_4', lsbimfft4x6_packed, 0)

       usbrefft4x6_to_pack = np.round( bj_usbrefft4x6*2**29)
       usbrefft4x6_packed  = struct.pack('>512l', *usbrefft4x6_to_pack)
       fpga.write('usbre6_4', usbrefft4x6_packed, 0)

       usbimfft4x6_to_pack = np.round( bj_usbimfft4x6*2**29)
       usbimfft4x6_packed  = struct.pack('>512l', *usbimfft4x6_to_pack)
       fpga.write('usbim6_4', usbimfft4x6_packed, 0)

       # 7777777777777777777777 #      
       lsbrefft4x7_to_pack = np.round( bj_lsbrefft4x7*2**29)
       lsbrefft4x7_packed  = struct.pack('>512l', *lsbrefft4x7_to_pack)
       fpga.write('lsbre7_4', lsbrefft4x7_packed, 0)  

       lsbimfft4x7_to_pack = np.round( bj_lsbimfft4x7*2**29)
       lsbimfft4x7_packed  = struct.pack('>512l', *lsbimfft4x7_to_pack)
       fpga.write('lsbim7_4', lsbimfft4x7_packed, 0)

       usbrefft4x7_to_pack = np.round( bj_usbrefft4x7*2**29)
       usbrefft4x7_packed  = struct.pack('>512l', *usbrefft4x7_to_pack)
       fpga.write('usbre7_4', usbrefft4x7_packed, 0)

       usbimfft4x7_to_pack = np.round( bj_usbimfft4x7*2**29)
       usbimfft4x7_packed  = struct.pack('>512l', *usbimfft4x7_to_pack)
       fpga.write('usbim7_4', usbimfft4x7_packed, 0)




       print 'done. \n'


    if (opts.auto_align_bits):
       print 'Aligning bits... \n'
       fpga.write_int('msb_realign',1)
       time.sleep(0.5)
       fpga.write_int('msb_realign',0)
       time.sleep(0.5)             #luke
       if(fpga.read_int('bit_locked') !=255):#luke
            print 'bit locked failed. \n'   #luke
       print 'done. \n'

    if (opts.auto_align_bytes==1):
       print 'Aligning IQ... \n'
       
       ###############
       #Luke debug
       ###############

    
       time.sleep(0.3)
       ####################################
       ####################################
       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,0)
       usb0_mag1, lsb0_mag1, usb1_mag1, lsb1_mag1 = treat_data_sbs(0,0)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag1, lsb2_mag1, usb3_mag1, lsb3_mag1 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag1, lsb4_mag1, usb5_mag1, lsb5_mag1 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag1, lsb6_mag1, usb7_mag1, lsb7_mag1 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag2, lsb0_mag2, usb1_mag2, lsb1_mag2 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag2, lsb2_mag2, usb3_mag2, lsb3_mag2 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag2, lsb4_mag2, usb5_mag2, lsb5_mag2 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag2, lsb6_mag2, usb7_mag2, lsb7_mag2 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag3, lsb0_mag3, usb1_mag3, lsb1_mag3 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag3, lsb2_mag3, usb3_mag3, lsb3_mag3 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag3, lsb4_mag3, usb5_mag3, lsb5_mag3 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag3, lsb6_mag3, usb7_mag3, lsb7_mag3 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag4, lsb0_mag4, usb1_mag4, lsb1_mag4 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag4, lsb2_mag4, usb3_mag4, lsb3_mag4 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag4, lsb4_mag4, usb5_mag4, lsb5_mag4 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag4, lsb6_mag4, usb7_mag4, lsb7_mag4 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag5, lsb0_mag5, usb1_mag5, lsb1_mag5 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag5, lsb2_mag5, usb3_mag5, lsb3_mag5 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag5, lsb4_mag5, usb5_mag5, lsb5_mag5 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag5, lsb6_mag5, usb7_mag5, lsb7_mag5 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag6, lsb0_mag6, usb1_mag6, lsb1_mag6 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag6, lsb2_mag6, usb3_mag6, lsb3_mag6 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag6, lsb4_mag6, usb5_mag6, lsb5_mag6 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag6, lsb6_mag6, usb7_mag6, lsb7_mag6 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag7, lsb0_mag7, usb1_mag7, lsb1_mag7 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag7, lsb2_mag7, usb3_mag7, lsb3_mag7 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag7, lsb4_mag7, usb5_mag7, lsb5_mag7 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag7, lsb6_mag7, usb7_mag7, lsb7_mag7 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag8, lsb0_mag8, usb1_mag8, lsb1_mag8 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag8, lsb2_mag8, usb3_mag8, lsb3_mag8 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag8, lsb4_mag8, usb5_mag8, lsb5_mag8 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag8, lsb6_mag8, usb7_mag8, lsb7_mag8 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag9, lsb0_mag9, usb1_mag9, lsb1_mag9 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag9, lsb2_mag9, usb3_mag9, lsb3_mag9 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag9, lsb4_mag9, usb5_mag9, lsb5_mag9 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag9, lsb6_mag9, usb7_mag9, lsb7_mag9 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag10, lsb0_mag10, usb1_mag10, lsb1_mag10 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag10, lsb2_mag10, usb3_mag10, lsb3_mag10 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag10, lsb4_mag10, usb5_mag10, lsb5_mag10 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag10, lsb6_mag10, usb7_mag10, lsb7_mag10 = treat_data_sbs(6,1)

       time.sleep(0.1)
       
       usb0_mag=usb0_mag1
       lsb0_mag=lsb0_mag1
       usb1_mag=usb1_mag1
       lsb1_mag=lsb1_mag1
       usb2_mag=usb2_mag1
       lsb2_mag=lsb2_mag1
       usb3_mag=usb3_mag1
       lsb3_mag=lsb3_mag1
       usb4_mag=usb4_mag1
       lsb4_mag=lsb4_mag1
       usb5_mag=usb5_mag1
       lsb5_mag=lsb5_mag1
       usb6_mag=usb6_mag1
       lsb6_mag=lsb6_mag1
       usb7_mag=usb7_mag1
       lsb7_mag=lsb7_mag1

       ii=1
       #while ii<257:
       ########luke
       while ii<246:
            usb0_mag[ii] = usb0_mag1[ii]+usb0_mag2[ii]+usb0_mag3[ii]+usb0_mag4[ii]+usb0_mag5[ii]+usb0_mag6[ii]+usb0_mag7[ii]+usb0_mag8[ii]+usb0_mag9[ii]+usb0_mag10[ii]
            lsb0_mag[ii] = lsb0_mag1[ii]+lsb0_mag2[ii]+lsb0_mag3[ii]+lsb0_mag5[ii]+lsb0_mag5[ii]+lsb0_mag6[ii]+lsb0_mag7[ii]+lsb0_mag8[ii]+lsb0_mag9[ii]+lsb0_mag10[ii]
            usb1_mag[ii] = usb1_mag1[ii]+usb1_mag2[ii]+usb1_mag3[ii]+usb1_mag4[ii]+usb1_mag5[ii]+usb1_mag6[ii]+usb1_mag7[ii]+usb1_mag8[ii]+usb1_mag9[ii]+usb1_mag10[ii]
            lsb1_mag[ii] = lsb1_mag1[ii]+lsb1_mag2[ii]+lsb1_mag3[ii]+lsb1_mag5[ii]+lsb1_mag5[ii]+lsb1_mag6[ii]+lsb1_mag7[ii]+lsb1_mag8[ii]+lsb1_mag9[ii]+lsb1_mag10[ii]
            usb2_mag[ii] = usb2_mag1[ii]+usb2_mag2[ii]+usb2_mag3[ii]+usb2_mag4[ii]+usb2_mag5[ii]+usb2_mag6[ii]+usb2_mag7[ii]+usb2_mag8[ii]+usb2_mag9[ii]+usb2_mag10[ii]
            lsb2_mag[ii] = lsb2_mag1[ii]+lsb2_mag2[ii]+lsb2_mag3[ii]+lsb2_mag5[ii]+lsb2_mag5[ii]+lsb2_mag6[ii]+lsb2_mag7[ii]+lsb2_mag8[ii]+lsb2_mag9[ii]+lsb2_mag10[ii]
            usb3_mag[ii] = usb3_mag1[ii]+usb3_mag2[ii]+usb3_mag3[ii]+usb3_mag4[ii]+usb3_mag5[ii]+usb3_mag6[ii]+usb3_mag7[ii]+usb3_mag8[ii]+usb3_mag9[ii]+usb3_mag10[ii]
            lsb3_mag[ii] = lsb3_mag1[ii]+lsb3_mag2[ii]+lsb3_mag3[ii]+lsb3_mag5[ii]+lsb3_mag5[ii]+lsb3_mag6[ii]+lsb3_mag7[ii]+lsb3_mag8[ii]+lsb3_mag9[ii]+lsb3_mag10[ii]
            usb4_mag[ii] = usb4_mag1[ii]+usb4_mag2[ii]+usb4_mag3[ii]+usb4_mag4[ii]+usb4_mag5[ii]+usb4_mag6[ii]+usb4_mag7[ii]+usb4_mag8[ii]+usb4_mag9[ii]+usb4_mag10[ii]
            lsb4_mag[ii] = lsb4_mag1[ii]+lsb4_mag2[ii]+lsb4_mag3[ii]+lsb4_mag5[ii]+lsb4_mag5[ii]+lsb4_mag6[ii]+lsb4_mag7[ii]+lsb4_mag8[ii]+lsb4_mag9[ii]+lsb4_mag10[ii]
            usb5_mag[ii] = usb5_mag1[ii]+usb5_mag2[ii]+usb5_mag3[ii]+usb5_mag4[ii]+usb5_mag5[ii]+usb5_mag6[ii]+usb5_mag7[ii]+usb5_mag8[ii]+usb5_mag9[ii]+usb5_mag10[ii]
            lsb5_mag[ii] = lsb5_mag1[ii]+lsb5_mag2[ii]+lsb5_mag3[ii]+lsb5_mag5[ii]+lsb5_mag5[ii]+lsb5_mag6[ii]+lsb5_mag7[ii]+lsb5_mag8[ii]+lsb5_mag9[ii]+lsb5_mag10[ii]
            usb6_mag[ii] = usb6_mag1[ii]+usb6_mag2[ii]+usb6_mag3[ii]+usb6_mag4[ii]+usb6_mag5[ii]+usb6_mag6[ii]+usb6_mag7[ii]+usb6_mag8[ii]+usb6_mag9[ii]+usb6_mag10[ii]
            lsb6_mag[ii] = lsb6_mag1[ii]+lsb6_mag2[ii]+lsb6_mag3[ii]+lsb6_mag5[ii]+lsb6_mag5[ii]+lsb6_mag6[ii]+lsb6_mag7[ii]+lsb6_mag8[ii]+lsb6_mag9[ii]+lsb6_mag10[ii]
            usb7_mag[ii] = usb7_mag1[ii]+usb7_mag2[ii]+usb7_mag3[ii]+usb7_mag4[ii]+usb7_mag5[ii]+usb7_mag6[ii]+usb7_mag7[ii]+usb7_mag8[ii]+usb7_mag9[ii]+usb7_mag10[ii]
            lsb7_mag[ii] = lsb7_mag1[ii]+lsb7_mag2[ii]+lsb7_mag3[ii]+lsb7_mag5[ii]+lsb7_mag5[ii]+lsb7_mag6[ii]+lsb7_mag7[ii]+lsb7_mag8[ii]+lsb7_mag9[ii]+lsb7_mag10[ii]
            ii=ii+1
        
       usb0_mag = usb0_mag1
       lsb0_mag = lsb0_mag1

       mag0_rat_a=usb0_mag[166]/lsb0_mag[166]
       mag1_rat_a=usb1_mag[166]/lsb1_mag[166]
       mag2_rat_a=usb2_mag[166]/lsb2_mag[166]
       mag3_rat_a=usb3_mag[166]/lsb3_mag[166]
       mag4_rat_a=usb4_mag[166]/lsb4_mag[166]
       mag5_rat_a=usb5_mag[166]/lsb5_mag[166]
       mag6_rat_a=usb6_mag[166]/lsb6_mag[166]
       mag7_rat_a=usb7_mag[166]/lsb7_mag[166]

       #print 'numpy.argmax0\n'
       #print np.argmax(lsb0_mag)
       #print lsb0_mag[np.argmax(lsb0_mag)-1]
       #print lsb0_mag[np.argmax(lsb0_mag)]
       #print lsb0_mag[np.argmax(lsb0_mag)+1]
       #print np.argmax(usb0_mag)
       #print usb0_mag[np.argmax(usb0_mag)-1]
       #print usb0_mag[np.argmax(usb0_mag)]
       #print usb0_mag[np.argmax(usb0_mag)+1]

       print '0000000'
       print lsb0_mag[34] #10MHz offset
       print usb0_mag[34]
       print '\n'
       if(usb0_mag[34]<lsb0_mag[34]):
            print 'flipping 0\n'
            count1=0
            while (count1<8):
                fpga.write_int('rxslide',2**0)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                count1=count1+1

       print '1111111'
       print lsb1_mag[34]
       print usb1_mag[34]
       print '\n'
       if(usb1_mag[34]<lsb1_mag[34]):
            print 'flipping 1\n'
            count1=0
            while (count1<8):
                fpga.write_int('rxslide',2**1)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                count1=count1+1

       print '2222222'
       print lsb2_mag[34]
       print usb2_mag[34]
       print '\n'
       if(usb2_mag[34]<lsb2_mag[34]):
       #if(usb2_mag[34]>lsb2_mag[34]):
            print 'flipping 2\n'
            count1=0
            while (count1<8):
                fpga.write_int('rxslide',2**2)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                count1=count1+1

       print '3333333'
       print lsb3_mag[34]
       print usb3_mag[34]
       print '\n'
       if(usb3_mag[34]<lsb3_mag[34]):
            print 'flipping 3\n'
            count1=0
            while (count1<8):
                fpga.write_int('rxslide',2**3)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                count1=count1+1

       print '4444444'
       print lsb4_mag[34]
       print usb4_mag[34]
       print '\n'
       if(usb4_mag[34]<lsb4_mag[34]):
            print 'flipping 4\n'
            count1=0
            while (count1<8):
                fpga.write_int('rxslide',2**4)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                count1=count1+1

       print '5555555'
       print lsb5_mag[34]
       print usb5_mag[34]
       print '\n'
       if(usb5_mag[34]<lsb5_mag[34]):
            print 'flipping 5\n'
            count1=0
            while (count1<8):
                fpga.write_int('rxslide',2**5)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                count1=count1+1

       print '6666666'
       print lsb6_mag[34]
       print usb6_mag[34]
       print '\n'
       if(usb6_mag[34]<lsb6_mag[34]):
            print 'flipping 6\n'
            count1=0
            while (count1<8):
                fpga.write_int('rxslide',2**6)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                count1=count1+1

       print '7777777'
       print lsb7_mag[34]
       print usb7_mag[34]
       print '\n'
       if(usb7_mag[34]<lsb7_mag[34]):
            print 'flipping 7\n'
            count1=0
            while (count1<8):
                fpga.write_int('rxslide',2**7)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                count1=count1+1

    elif (opts.auto_align_bytes == 3):



       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)
       time.sleep(0.1)
       fpga.write_int('rxslide',255)
       time.sleep(0.1)
       fpga.write_int('rxslide',0)





       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,0)
       usb0_mag1, lsb0_mag1, usb1_mag1, lsb1_mag1 = treat_data_sbs(0,0)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag1, lsb2_mag1, usb3_mag1, lsb3_mag1 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag1, lsb4_mag1, usb5_mag1, lsb5_mag1 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag1, lsb6_mag1, usb7_mag1, lsb7_mag1 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag2, lsb0_mag2, usb1_mag2, lsb1_mag2 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag2, lsb2_mag2, usb3_mag2, lsb3_mag2 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag2, lsb4_mag2, usb5_mag2, lsb5_mag2 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag2, lsb6_mag2, usb7_mag2, lsb7_mag2 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag3, lsb0_mag3, usb1_mag3, lsb1_mag3 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag3, lsb2_mag3, usb3_mag3, lsb3_mag3 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag3, lsb4_mag3, usb5_mag3, lsb5_mag3 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag3, lsb6_mag3, usb7_mag3, lsb7_mag3 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag4, lsb0_mag4, usb1_mag4, lsb1_mag4 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag4, lsb2_mag4, usb3_mag4, lsb3_mag4 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag4, lsb4_mag4, usb5_mag4, lsb5_mag4 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag4, lsb6_mag4, usb7_mag4, lsb7_mag4 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag5, lsb0_mag5, usb1_mag5, lsb1_mag5 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag5, lsb2_mag5, usb3_mag5, lsb3_mag5 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag5, lsb4_mag5, usb5_mag5, lsb5_mag5 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag5, lsb6_mag5, usb7_mag5, lsb7_mag5 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag6, lsb0_mag6, usb1_mag6, lsb1_mag6 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag6, lsb2_mag6, usb3_mag6, lsb3_mag6 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag6, lsb4_mag6, usb5_mag6, lsb5_mag6 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag6, lsb6_mag6, usb7_mag6, lsb7_mag6 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag7, lsb0_mag7, usb1_mag7, lsb1_mag7 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag7, lsb2_mag7, usb3_mag7, lsb3_mag7 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag7, lsb4_mag7, usb5_mag7, lsb5_mag7 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag7, lsb6_mag7, usb7_mag7, lsb7_mag7 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag8, lsb0_mag8, usb1_mag8, lsb1_mag8 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag8, lsb2_mag8, usb3_mag8, lsb3_mag8 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag8, lsb4_mag8, usb5_mag8, lsb5_mag8 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag8, lsb6_mag8, usb7_mag8, lsb7_mag8 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag9, lsb0_mag9, usb1_mag9, lsb1_mag9 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag9, lsb2_mag9, usb3_mag9, lsb3_mag9 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag9, lsb4_mag9, usb5_mag9, lsb5_mag9 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag9, lsb6_mag9, usb7_mag9, lsb7_mag9 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag10, lsb0_mag10, usb1_mag10, lsb1_mag10 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag10, lsb2_mag10, usb3_mag10, lsb3_mag10 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag10, lsb4_mag10, usb5_mag10, lsb5_mag10 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag10, lsb6_mag10, usb7_mag10, lsb7_mag10 = treat_data_sbs(6,1)

       time.sleep(0.1)
       
       usb0_mag_f=usb0_mag1
       lsb0_mag_f=lsb0_mag1
       usb1_mag_f=usb1_mag1
       lsb1_mag_f=lsb1_mag1
       usb2_mag_f=usb2_mag1
       lsb2_mag_f=lsb2_mag1
       usb3_mag_f=usb3_mag1
       lsb3_mag_f=lsb3_mag1
       usb4_mag_f=usb4_mag1
       lsb4_mag_f=lsb4_mag1
       usb5_mag_f=usb5_mag1
       lsb5_mag_f=lsb5_mag1
       usb6_mag_f=usb6_mag1
       lsb6_mag_f=lsb6_mag1
       usb7_mag_f=usb7_mag1
       lsb7_mag_f=lsb7_mag1

       ii=1
       #while ii<257:
       ########luke
       while ii<246:
            usb0_mag_f[ii] = usb0_mag1[ii]+usb0_mag2[ii]+usb0_mag3[ii]+usb0_mag4[ii]+usb0_mag5[ii]+usb0_mag6[ii]+usb0_mag7[ii]+usb0_mag8[ii]+usb0_mag9[ii]+usb0_mag10[ii]
            lsb0_mag_f[ii] = lsb0_mag1[ii]+lsb0_mag2[ii]+lsb0_mag3[ii]+lsb0_mag5[ii]+lsb0_mag5[ii]+lsb0_mag6[ii]+lsb0_mag7[ii]+lsb0_mag8[ii]+lsb0_mag9[ii]+lsb0_mag10[ii]
            usb1_mag_f[ii] = usb1_mag1[ii]+usb1_mag2[ii]+usb1_mag3[ii]+usb1_mag4[ii]+usb1_mag5[ii]+usb1_mag6[ii]+usb1_mag7[ii]+usb1_mag8[ii]+usb1_mag9[ii]+usb1_mag10[ii]
            lsb1_mag_f[ii] = lsb1_mag1[ii]+lsb1_mag2[ii]+lsb1_mag3[ii]+lsb1_mag5[ii]+lsb1_mag5[ii]+lsb1_mag6[ii]+lsb1_mag7[ii]+lsb1_mag8[ii]+lsb1_mag9[ii]+lsb1_mag10[ii]
            usb2_mag_f[ii] = usb2_mag1[ii]+usb2_mag2[ii]+usb2_mag3[ii]+usb2_mag4[ii]+usb2_mag5[ii]+usb2_mag6[ii]+usb2_mag7[ii]+usb2_mag8[ii]+usb2_mag9[ii]+usb2_mag10[ii]
            lsb2_mag_f[ii] = lsb2_mag1[ii]+lsb2_mag2[ii]+lsb2_mag3[ii]+lsb2_mag5[ii]+lsb2_mag5[ii]+lsb2_mag6[ii]+lsb2_mag7[ii]+lsb2_mag8[ii]+lsb2_mag9[ii]+lsb2_mag10[ii]
            usb3_mag_f[ii] = usb3_mag1[ii]+usb3_mag2[ii]+usb3_mag3[ii]+usb3_mag4[ii]+usb3_mag5[ii]+usb3_mag6[ii]+usb3_mag7[ii]+usb3_mag8[ii]+usb3_mag9[ii]+usb3_mag10[ii]
            lsb3_mag_f[ii] = lsb3_mag1[ii]+lsb3_mag2[ii]+lsb3_mag3[ii]+lsb3_mag5[ii]+lsb3_mag5[ii]+lsb3_mag6[ii]+lsb3_mag7[ii]+lsb3_mag8[ii]+lsb3_mag9[ii]+lsb3_mag10[ii]
            usb4_mag_f[ii] = usb4_mag1[ii]+usb4_mag2[ii]+usb4_mag3[ii]+usb4_mag4[ii]+usb4_mag5[ii]+usb4_mag6[ii]+usb4_mag7[ii]+usb4_mag8[ii]+usb4_mag9[ii]+usb4_mag10[ii]
            lsb4_mag_f[ii] = lsb4_mag1[ii]+lsb4_mag2[ii]+lsb4_mag3[ii]+lsb4_mag5[ii]+lsb4_mag5[ii]+lsb4_mag6[ii]+lsb4_mag7[ii]+lsb4_mag8[ii]+lsb4_mag9[ii]+lsb4_mag10[ii]
            usb5_mag_f[ii] = usb5_mag1[ii]+usb5_mag2[ii]+usb5_mag3[ii]+usb5_mag4[ii]+usb5_mag5[ii]+usb5_mag6[ii]+usb5_mag7[ii]+usb5_mag8[ii]+usb5_mag9[ii]+usb5_mag10[ii]
            lsb5_mag_f[ii] = lsb5_mag1[ii]+lsb5_mag2[ii]+lsb5_mag3[ii]+lsb5_mag5[ii]+lsb5_mag5[ii]+lsb5_mag6[ii]+lsb5_mag7[ii]+lsb5_mag8[ii]+lsb5_mag9[ii]+lsb5_mag10[ii]
            usb6_mag_f[ii] = usb6_mag1[ii]+usb6_mag2[ii]+usb6_mag3[ii]+usb6_mag4[ii]+usb6_mag5[ii]+usb6_mag6[ii]+usb6_mag7[ii]+usb6_mag8[ii]+usb6_mag9[ii]+usb6_mag10[ii]
            lsb6_mag_f[ii] = lsb6_mag1[ii]+lsb6_mag2[ii]+lsb6_mag3[ii]+lsb6_mag5[ii]+lsb6_mag5[ii]+lsb6_mag6[ii]+lsb6_mag7[ii]+lsb6_mag8[ii]+lsb6_mag9[ii]+lsb6_mag10[ii]
            usb7_mag_f[ii] = usb7_mag1[ii]+usb7_mag2[ii]+usb7_mag3[ii]+usb7_mag4[ii]+usb7_mag5[ii]+usb7_mag6[ii]+usb7_mag7[ii]+usb7_mag8[ii]+usb7_mag9[ii]+usb7_mag10[ii]
            lsb7_mag_f[ii] = lsb7_mag1[ii]+lsb7_mag2[ii]+lsb7_mag3[ii]+lsb7_mag5[ii]+lsb7_mag5[ii]+lsb7_mag6[ii]+lsb7_mag7[ii]+lsb7_mag8[ii]+lsb7_mag9[ii]+lsb7_mag10[ii]
            ii=ii+1

       usb0_mag_f=usb0_mag1
       lsb0_mag_f=lsb0_mag1

       count2 =0
       usb_mag_ssum = [0,0,0,0,0,0,0,0]
       usb_mag_ssum_f = [0,0,0,0,0,0,0,0]

       lsb_mag_ssum = [0,0,0,0,0,0,0,0]
       lsb_mag_ssum_f = [0,0,0,0,0,0,0,0]

       usb_mag_ssum_ms   = [0,0,0,0,0,0,0,0]
       usb_mag_ssum_ms_f = [0,0,0,0,0,0,0,0]

       lsb_mag_ssum_ms = [0,0,0,0,0,0,0,0]
       lsb_mag_ssum_ms_f = [0,0,0,0,0,0,0,0]
       
       while (count2<246):
            usb_mag_ssum[0]   = usb0_mag[count2]  +usb_mag_ssum[0]
            usb_mag_ssum_f[0] = usb0_mag_f[count2]+usb_mag_ssum_f[0]
            usb_mag_ssum[1]   = usb1_mag[count2]  +usb_mag_ssum[1]
            usb_mag_ssum_f[1] = usb1_mag_f[count2]+usb_mag_ssum_f[1]
            usb_mag_ssum[2]   = usb2_mag[count2]  +usb_mag_ssum[2]
            usb_mag_ssum_f[2] = usb2_mag_f[count2]+usb_mag_ssum_f[2]
            usb_mag_ssum[3]   = usb3_mag[count2]  +usb_mag_ssum[3]
            usb_mag_ssum_f[3] = usb3_mag_f[count2]+usb_mag_ssum_f[3]
            usb_mag_ssum[4]   = usb4_mag[count2]  +usb_mag_ssum[4]
            usb_mag_ssum_f[4] = usb4_mag_f[count2]+usb_mag_ssum_f[4]
            usb_mag_ssum[5]   = usb5_mag[count2]  +usb_mag_ssum[5]
            usb_mag_ssum_f[5] = usb5_mag_f[count2]+usb_mag_ssum_f[5]
            usb_mag_ssum[6]   = usb6_mag[count2]  +usb_mag_ssum[6]
            usb_mag_ssum_f[6] = usb6_mag_f[count2]+usb_mag_ssum_f[6]
            usb_mag_ssum[7]   = usb7_mag[count2]  +usb_mag_ssum[7]
            usb_mag_ssum_f[7] = usb7_mag_f[count2]+usb_mag_ssum_f[7]

            lsb_mag_ssum[0]   = lsb0_mag[count2]  +lsb_mag_ssum[0]
            lsb_mag_ssum_f[0] = lsb0_mag_f[count2]+lsb_mag_ssum_f[0]
            lsb_mag_ssum[1]   = lsb1_mag[count2]  +lsb_mag_ssum[1]
            lsb_mag_ssum_f[1] = lsb1_mag_f[count2]+lsb_mag_ssum_f[1]
            lsb_mag_ssum[2]   = lsb2_mag[count2]  +lsb_mag_ssum[2]
            lsb_mag_ssum_f[2] = lsb2_mag_f[count2]+lsb_mag_ssum_f[2]
            lsb_mag_ssum[3]   = lsb3_mag[count2]  +lsb_mag_ssum[3]
            lsb_mag_ssum_f[3] = lsb3_mag_f[count2]+lsb_mag_ssum_f[3]
            lsb_mag_ssum[4]   = lsb4_mag[count2]  +lsb_mag_ssum[4]
            lsb_mag_ssum_f[4] = lsb4_mag_f[count2]+lsb_mag_ssum_f[4]
            lsb_mag_ssum[5]   = lsb5_mag[count2]  +lsb_mag_ssum[5]
            lsb_mag_ssum_f[5] = lsb5_mag_f[count2]+lsb_mag_ssum_f[5]
            lsb_mag_ssum[6]   = lsb6_mag[count2]  +lsb_mag_ssum[6]
            lsb_mag_ssum_f[6] = lsb6_mag_f[count2]+lsb_mag_ssum_f[6]
            lsb_mag_ssum[7]   = lsb7_mag[count2]  +lsb_mag_ssum[7]
            lsb_mag_ssum_f[7] = lsb7_mag_f[count2]+lsb_mag_ssum_f[7]

            usb_mag_ssum_ms[0]=usb_mag_ssum_ms[0]+usb0_mag[count2]**2
            usb_mag_ssum_ms[1]=usb_mag_ssum_ms[1]+usb1_mag[count2]**2
            usb_mag_ssum_ms[2]=usb_mag_ssum_ms[2]+usb2_mag[count2]**2
            usb_mag_ssum_ms[3]=usb_mag_ssum_ms[3]+usb3_mag[count2]**2
            usb_mag_ssum_ms[4]=usb_mag_ssum_ms[4]+usb4_mag[count2]**2
            usb_mag_ssum_ms[5]=usb_mag_ssum_ms[5]+usb5_mag[count2]**2
            usb_mag_ssum_ms[6]=usb_mag_ssum_ms[6]+usb6_mag[count2]**2
            usb_mag_ssum_ms[7]=usb_mag_ssum_ms[7]+usb7_mag[count2]**2

            lsb_mag_ssum_ms[0]=lsb_mag_ssum_ms[0]+lsb0_mag[count2]**2
            lsb_mag_ssum_ms[1]=lsb_mag_ssum_ms[1]+lsb1_mag[count2]**2
            lsb_mag_ssum_ms[2]=lsb_mag_ssum_ms[2]+lsb2_mag[count2]**2
            lsb_mag_ssum_ms[3]=lsb_mag_ssum_ms[3]+lsb3_mag[count2]**2
            lsb_mag_ssum_ms[4]=lsb_mag_ssum_ms[4]+lsb4_mag[count2]**2
            lsb_mag_ssum_ms[5]=lsb_mag_ssum_ms[5]+lsb5_mag[count2]**2
            lsb_mag_ssum_ms[6]=lsb_mag_ssum_ms[6]+lsb6_mag[count2]**2
            lsb_mag_ssum_ms[7]=lsb_mag_ssum_ms[7]+lsb7_mag[count2]**2

            usb_mag_ssum_ms_f[0]=usb_mag_ssum_ms_f[0]+usb0_mag_f[count2]**2
            usb_mag_ssum_ms_f[1]=usb_mag_ssum_ms_f[1]+usb1_mag_f[count2]**2
            usb_mag_ssum_ms_f[2]=usb_mag_ssum_ms_f[2]+usb2_mag_f[count2]**2
            usb_mag_ssum_ms_f[3]=usb_mag_ssum_ms_f[3]+usb3_mag_f[count2]**2
            usb_mag_ssum_ms_f[4]=usb_mag_ssum_ms_f[4]+usb4_mag_f[count2]**2
            usb_mag_ssum_ms_f[5]=usb_mag_ssum_ms_f[5]+usb5_mag_f[count2]**2
            usb_mag_ssum_ms_f[6]=usb_mag_ssum_ms_f[6]+usb6_mag_f[count2]**2
            usb_mag_ssum_ms_f[7]=usb_mag_ssum_ms_f[7]+usb7_mag_f[count2]**2

            lsb_mag_ssum_ms_f[0]=lsb_mag_ssum_ms_f[0]+lsb0_mag_f[count2]**2
            lsb_mag_ssum_ms_f[1]=lsb_mag_ssum_ms_f[1]+lsb1_mag_f[count2]**2
            lsb_mag_ssum_ms_f[2]=lsb_mag_ssum_ms_f[2]+lsb2_mag_f[count2]**2
            lsb_mag_ssum_ms_f[3]=lsb_mag_ssum_ms_f[3]+lsb3_mag_f[count2]**2
            lsb_mag_ssum_ms_f[4]=lsb_mag_ssum_ms_f[4]+lsb4_mag_f[count2]**2
            lsb_mag_ssum_ms_f[5]=lsb_mag_ssum_ms_f[5]+lsb5_mag_f[count2]**2
            lsb_mag_ssum_ms_f[6]=lsb_mag_ssum_ms_f[6]+lsb6_mag_f[count2]**2
            lsb_mag_ssum_ms_f[7]=lsb_mag_ssum_ms_f[7]+lsb7_mag_f[count2]**2
            count2=count2+1

       #%print 'numpy.argmax0\n'
       #%print np.argmax(lsb0_mag_f)
       #%print lsb0_mag_f[np.argmax(lsb0_mag_f)]
       #%print np.argmax(usb0_mag_f)
       #%print usb0_mag_f[np.argmax(usb0_mag_f)]
       #%print np.argmax(lsb0_mag)
       #%print lsb0_mag[np.argmax(lsb0_mag)]
       #%print np.argmax(usb0_mag)
       #%print usb0_mag[np.argmax(usb0_mag)]
       #%print usb_mag_ssum[0]-lsb_mag_ssum[0]
       #%print usb_mag_ssum_f[0]-lsb_mag_ssum_f[0]
       #%print math.sqrt(usb_mag_ssum_ms[0]/256)
       #%print math.sqrt(usb_mag_ssum_ms_f[0]/256)
       #%print math.sqrt(lsb_mag_ssum_ms[0]/256)
       #%print math.sqrt(lsb_mag_ssum_ms_f[0]/256)

       flip_bytes = [0,0,0,0,0,0,0,0]

       print 'numpy.argmax0\n'
       print np.argmax(lsb0_mag_f)
       print lsb0_mag_f[np.argmax(lsb0_mag_f)]
       print np.argmax(usb0_mag_f)
       print usb0_mag_f[np.argmax(usb0_mag_f)]
       print np.argmax(lsb0_mag)
       print lsb0_mag[np.argmax(lsb0_mag)]
       print np.argmax(usb0_mag)
       print usb0_mag[np.argmax(usb0_mag)]

      # if((

       print 'numpy.argmax1\n'
       print np.argmax(lsb1_mag_f)
       print lsb1_mag_f[np.argmax(lsb1_mag_f)]
       print np.argmax(usb1_mag_f)
       print usb1_mag_f[np.argmax(usb1_mag_f)]
       print np.argmax(lsb1_mag)
       print lsb1_mag[np.argmax(lsb1_mag)]
       print np.argmax(usb1_mag)
       print usb1_mag[np.argmax(usb1_mag)]

       print 'numpy.argmax2\n'
       print np.argmax(lsb2_mag_f)
       print lsb2_mag_f[np.argmax(lsb2_mag_f)]
       print np.argmax(usb2_mag_f)
       print usb2_mag_f[np.argmax(usb2_mag_f)]
       print np.argmax(lsb2_mag)
       print lsb2_mag[np.argmax(lsb2_mag)]
       print np.argmax(usb2_mag)
       print usb2_mag[np.argmax(usb2_mag)]
    
       print 'numpy.argmax3\n'
       print np.argmax(lsb3_mag_f)
       print lsb3_mag_f[np.argmax(lsb3_mag_f)]
       print np.argmax(usb3_mag_f)
       print usb3_mag_f[np.argmax(usb3_mag_f)]
       print np.argmax(lsb3_mag)
       print lsb3_mag[np.argmax(lsb3_mag)]
       print np.argmax(usb3_mag)
       print usb3_mag[np.argmax(usb3_mag)]

       print 'numpy.argmax4\n'
       print np.argmax(lsb4_mag_f)
       print lsb4_mag_f[np.argmax(lsb4_mag_f)]
       print np.argmax(usb4_mag_f)
       print usb4_mag_f[np.argmax(usb4_mag_f)]
       print np.argmax(lsb4_mag)
       print lsb4_mag[np.argmax(lsb4_mag)]
       print np.argmax(usb4_mag)
       print usb4_mag[np.argmax(usb4_mag)]

       print 'numpy.argmax5\n'
       print np.argmax(lsb5_mag_f)
       print lsb5_mag_f[np.argmax(lsb5_mag_f)]
       print np.argmax(usb5_mag_f)
       print usb5_mag_f[np.argmax(usb5_mag_f)]
       print np.argmax(lsb5_mag)
       print lsb5_mag[np.argmax(lsb5_mag)]
       print np.argmax(usb5_mag)
       print usb5_mag[np.argmax(usb5_mag)]

       print 'numpy.argmax6\n'
       print np.argmax(lsb6_mag_f)
       print lsb6_mag_f[np.argmax(lsb6_mag_f)]
       print np.argmax(usb6_mag_f)
       print usb6_mag_f[np.argmax(usb6_mag_f)]
       print np.argmax(lsb6_mag)
       print lsb6_mag[np.argmax(lsb6_mag)]
       print np.argmax(usb6_mag)
       print usb6_mag[np.argmax(usb6_mag)]

       print 'numpy.argmax7\n'
       print np.argmax(lsb7_mag_f)
       print lsb7_mag_f[np.argmax(lsb7_mag_f)]
       print np.argmax(usb7_mag_f)
       print usb7_mag_f[np.argmax(usb7_mag_f)]
       print np.argmax(lsb7_mag)
       print lsb7_mag[np.argmax(lsb7_mag)]
       print np.argmax(usb7_mag)
       print usb7_mag[np.argmax(usb7_mag)]

    
       mag0_rat_b=usb0_mag_f[166]/lsb0_mag_f[166]
       mag1_rat_b=usb1_mag_f[166]/lsb1_mag_f[166]
       mag2_rat_b=usb2_mag_f[166]/lsb2_mag_f[166]
       mag3_rat_b=usb3_mag_f[166]/lsb3_mag_f[166]
       mag4_rat_b=usb4_mag_f[166]/lsb4_mag_f[166]
       mag5_rat_b=usb5_mag_f[166]/lsb5_mag_f[166]
       mag6_rat_b=usb6_mag_f[166]/lsb6_mag_f[166]
       mag7_rat_b=usb7_mag_f[166]/lsb7_mag_f[166]


       mag_usb_arr0 = [usb0_mag[166], usb1_mag[166], usb2_mag[166], usb3_mag[166], usb4_mag[166], usb5_mag[166], usb6_mag[166], usb7_mag[166]]
       mag_lsb_arr0 = [lsb0_mag[166], lsb1_mag[166], lsb2_mag[166], lsb3_mag[166], lsb4_mag[166], lsb5_mag[166], lsb6_mag[166], lsb7_mag[166]]

       mag_usb_arr1 = [usb0_mag_f[166], usb1_mag_f[166], usb2_mag_f[166], usb3_mag_f[166], usb4_mag_f[166], usb5_mag_f[166], usb6_mag_f[166], usb7_mag_f[166]]
       mag_lsb_arr1 = [lsb0_mag_f[166], lsb1_mag_f[166], lsb2_mag_f[166], lsb3_mag_f[166], lsb4_mag_f[166], lsb5_mag_f[166], lsb6_mag_f[166], lsb7_mag_f[166]]


       print ('mag0_rat_a')
       print mag_usb_arr0[0]/mag_lsb_arr0[0]#mag0_rat_a
       print ('mag0_rat_b')
       print mag_usb_arr1[0]/mag_lsb_arr1[0]#mag0_rat_b

       print ('mag1_rat_a')
       print mag_usb_arr0[1]/mag_lsb_arr0[1]#mag1_rat_a
       print ('mag1_rat_b')
       print mag_usb_arr1[1]/mag_lsb_arr1[1]#mag1_rat_b

       print ('mag2_rat_a')
       print mag_usb_arr0[2]/mag_lsb_arr0[2]#mag2_rat_a
       print ('mag2_rat_b')
       print mag_usb_arr1[2]/mag_lsb_arr1[2]#mag2_rat_b

       print ('mag3_rat_a')
       print mag_usb_arr0[3]/mag_lsb_arr0[3]#mag3_rat_a
       print ('mag3_rat_b')
       print mag_usb_arr1[3]/mag_lsb_arr1[3]#mag3_rat_b

       print ('mag4_rat_a')
       print mag_usb_arr0[4]/mag_lsb_arr0[4]#mag4_rat_a
       print ('mag4_rat_b')
       print mag_usb_arr1[4]/mag_lsb_arr1[4]#mag4_rat_b

       print ('mag5_rat_a')
       print mag_usb_arr0[5]/mag_lsb_arr0[5]#mag5_rat_a
       print ('mag5_rat_b')
       print mag_usb_arr1[5]/mag_lsb_arr1[5]#mag5_rat_b

       print ('mag6_rat_a')
       print mag_usb_arr0[6]/mag_lsb_arr0[6]#mag6_rat_a
       print ('mag6_rat_b')
       print mag_usb_arr1[6]/mag_lsb_arr1[6]#mag6_rat_b

       print ('mag7_rat_a')
       print mag_usb_arr0[7]/mag_lsb_arr0[7]#mag7_rat_a
       print ('mag7_rat_b')
       print mag_usb_arr1[7]/mag_lsb_arr1[7]#mag7_rat_b

   
       mag_rats_a = [mag0_rat_a, mag1_rat_a, mag2_rat_a, mag3_rat_a, mag4_rat_a, mag5_rat_a, mag6_rat_a, mag7_rat_a]
       mag_rats_b = [mag0_rat_b, mag1_rat_b, mag2_rat_b, mag3_rat_b, mag4_rat_b, mag5_rat_b, mag6_rat_b, mag7_rat_b]

       count=0
       while(count<8):
            #if(mag_rats_b[count]>mag_rats_a[count]):
            #if((mag_usb_arr0[count]-mag_lsb_arr0[count])>(mag_usb_arr1[count]-mag_lsb_arr1[count])):
            if((usb_mag_ssum[count]-lsb_mag_ssum[count])<(usb_mag_ssum_f[count]-lsb_mag_ssum_f[count])):
                count1=0
                print 're-aligning '
                print count
                print '\n'
                while(count1<8):
                    fpga.write_int('rxslide',2**count)
                    time.sleep(0.3)
                    fpga.write_int('rxslide',0)
                    count1=count1+1
            else:
                print (mag_usb_arr0[count]/mag_lsb_arr0[count])-(mag_usb_arr1[count]/mag_lsb_arr1[count])
            count=count+1


       print usb_mag_ssum[0]
       print usb_mag_ssum_f[0]
       print lsb_mag_ssum[0]
       print lsb_mag_ssum_f[0]
       print usb_mag_ssum[0]-lsb_mag_ssum[0]
       print usb_mag_ssum_f[0]-lsb_mag_ssum_f[0]
        

       print (mag_usb_arr0[0])
       print mag_usb_arr1[0]
       print 'done aligning bytes \n'


    elif (opts.auto_align_bytes == 2):
       print 'Checking IQ Alignment... \n'
       ####################################

       time.sleep(0.3)
       ####################################
       ####################################
       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,0)
       usb0_mag1, lsb0_mag1, usb1_mag1, lsb1_mag1 = treat_data_sbs(0,0)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag1, lsb2_mag1, usb3_mag1, lsb3_mag1 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag1, lsb4_mag1, usb5_mag1, lsb5_mag1 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag1, lsb6_mag1, usb7_mag1, lsb7_mag1 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag2, lsb0_mag2, usb1_mag2, lsb1_mag2 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag2, lsb2_mag2, usb3_mag2, lsb3_mag2 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag2, lsb4_mag2, usb5_mag2, lsb5_mag2 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag2, lsb6_mag2, usb7_mag2, lsb7_mag2 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag3, lsb0_mag3, usb1_mag3, lsb1_mag3 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag3, lsb2_mag3, usb3_mag3, lsb3_mag3 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag3, lsb4_mag3, usb5_mag3, lsb5_mag3 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag3, lsb6_mag3, usb7_mag3, lsb7_mag3 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag4, lsb0_mag4, usb1_mag4, lsb1_mag4 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag4, lsb2_mag4, usb3_mag4, lsb3_mag4 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag4, lsb4_mag4, usb5_mag4, lsb5_mag4 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag4, lsb6_mag4, usb7_mag4, lsb7_mag4 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag5, lsb0_mag5, usb1_mag5, lsb1_mag5 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag5, lsb2_mag5, usb3_mag5, lsb3_mag5 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag5, lsb4_mag5, usb5_mag5, lsb5_mag5 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag5, lsb6_mag5, usb7_mag5, lsb7_mag5 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag6, lsb0_mag6, usb1_mag6, lsb1_mag6 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag6, lsb2_mag6, usb3_mag6, lsb3_mag6 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag6, lsb4_mag6, usb5_mag6, lsb5_mag6 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag6, lsb6_mag6, usb7_mag6, lsb7_mag6 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag7, lsb0_mag7, usb1_mag7, lsb1_mag7 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag7, lsb2_mag7, usb3_mag7, lsb3_mag7 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag7, lsb4_mag7, usb5_mag7, lsb5_mag7 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag7, lsb6_mag7, usb7_mag7, lsb7_mag7 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag8, lsb0_mag8, usb1_mag8, lsb1_mag8 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag8, lsb2_mag8, usb3_mag8, lsb3_mag8 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag8, lsb4_mag8, usb5_mag8, lsb5_mag8 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag8, lsb6_mag8, usb7_mag8, lsb7_mag8 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag9, lsb0_mag9, usb1_mag9, lsb1_mag9 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag9, lsb2_mag9, usb3_mag9, lsb3_mag9 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag9, lsb4_mag9, usb5_mag9, lsb5_mag9 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag9, lsb6_mag9, usb7_mag9, lsb7_mag9 = treat_data_sbs(6,1)

       time.sleep(0.1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(0,1)
       usb0_mag10, lsb0_mag10, usb1_mag10, lsb1_mag10 = treat_data_sbs(0,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(2,1)
       usb2_mag10, lsb2_mag10, usb3_mag10, lsb3_mag10 = treat_data_sbs(2,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(4,1)
       usb4_mag10, lsb4_mag10, usb5_mag10, lsb5_mag10 = treat_data_sbs(4,1)

       usb_r,usb_i,lsb_r,lsb_i=get_data_sbs(6,1)
       usb6_mag10, lsb6_mag10, usb7_mag10, lsb7_mag10 = treat_data_sbs(6,1)

       time.sleep(0.1)
       
       usb0_mag=usb0_mag1
       lsb0_mag=lsb0_mag1
       usb1_mag=usb1_mag1
       lsb1_mag=lsb1_mag1
       usb2_mag=usb2_mag1
       lsb2_mag=lsb2_mag1
       usb3_mag=usb3_mag1
       lsb3_mag=lsb3_mag1
       usb4_mag=usb4_mag1
       lsb4_mag=lsb4_mag1
       usb5_mag=usb5_mag1
       lsb5_mag=lsb5_mag1
       usb6_mag=usb6_mag1
       lsb6_mag=lsb6_mag1
       usb7_mag=usb7_mag1
       lsb7_mag=lsb7_mag1

       ii=1
       #while ii<257:
       ########luke
       while ii<256:
            usb0_mag[ii] = usb0_mag1[ii]+usb0_mag2[ii]+usb0_mag3[ii]+usb0_mag4[ii]+usb0_mag5[ii]+usb0_mag6[ii]+usb0_mag7[ii]+usb0_mag8[ii]+usb0_mag9[ii]+usb0_mag10[ii]
            lsb0_mag[ii] = lsb0_mag1[ii]+lsb0_mag2[ii]+lsb0_mag3[ii]+lsb0_mag5[ii]+lsb0_mag5[ii]+lsb0_mag6[ii]+lsb0_mag7[ii]+lsb0_mag8[ii]+lsb0_mag9[ii]+lsb0_mag10[ii]
            usb1_mag[ii] = usb1_mag1[ii]+usb1_mag2[ii]+usb1_mag3[ii]+usb1_mag4[ii]+usb1_mag5[ii]+usb1_mag6[ii]+usb1_mag7[ii]+usb1_mag8[ii]+usb1_mag9[ii]+usb1_mag10[ii]
            lsb1_mag[ii] = lsb1_mag1[ii]+lsb1_mag2[ii]+lsb1_mag3[ii]+lsb1_mag5[ii]+lsb1_mag5[ii]+lsb1_mag6[ii]+lsb1_mag7[ii]+lsb1_mag8[ii]+lsb1_mag9[ii]+lsb1_mag10[ii]
            usb2_mag[ii] = usb2_mag1[ii]+usb2_mag2[ii]+usb2_mag3[ii]+usb2_mag4[ii]+usb2_mag5[ii]+usb2_mag6[ii]+usb2_mag7[ii]+usb2_mag8[ii]+usb2_mag9[ii]+usb2_mag10[ii]
            lsb2_mag[ii] = lsb2_mag1[ii]+lsb2_mag2[ii]+lsb2_mag3[ii]+lsb2_mag5[ii]+lsb2_mag5[ii]+lsb2_mag6[ii]+lsb2_mag7[ii]+lsb2_mag8[ii]+lsb2_mag9[ii]+lsb2_mag10[ii]
            usb3_mag[ii] = usb3_mag1[ii]+usb3_mag2[ii]+usb3_mag3[ii]+usb3_mag4[ii]+usb3_mag5[ii]+usb3_mag6[ii]+usb3_mag7[ii]+usb3_mag8[ii]+usb3_mag9[ii]+usb3_mag10[ii]
            lsb3_mag[ii] = lsb3_mag1[ii]+lsb3_mag2[ii]+lsb3_mag3[ii]+lsb3_mag5[ii]+lsb3_mag5[ii]+lsb3_mag6[ii]+lsb3_mag7[ii]+lsb3_mag8[ii]+lsb3_mag9[ii]+lsb3_mag10[ii]
            usb4_mag[ii] = usb4_mag1[ii]+usb4_mag2[ii]+usb4_mag3[ii]+usb4_mag4[ii]+usb4_mag5[ii]+usb4_mag6[ii]+usb4_mag7[ii]+usb4_mag8[ii]+usb4_mag9[ii]+usb4_mag10[ii]
            lsb4_mag[ii] = lsb4_mag1[ii]+lsb4_mag2[ii]+lsb4_mag3[ii]+lsb4_mag5[ii]+lsb4_mag5[ii]+lsb4_mag6[ii]+lsb4_mag7[ii]+lsb4_mag8[ii]+lsb4_mag9[ii]+lsb4_mag10[ii]
            usb5_mag[ii] = usb5_mag1[ii]+usb5_mag2[ii]+usb5_mag3[ii]+usb5_mag4[ii]+usb5_mag5[ii]+usb5_mag6[ii]+usb5_mag7[ii]+usb5_mag8[ii]+usb5_mag9[ii]+usb5_mag10[ii]
            lsb5_mag[ii] = lsb5_mag1[ii]+lsb5_mag2[ii]+lsb5_mag3[ii]+lsb5_mag5[ii]+lsb5_mag5[ii]+lsb5_mag6[ii]+lsb5_mag7[ii]+lsb5_mag8[ii]+lsb5_mag9[ii]+lsb5_mag10[ii]
            usb6_mag[ii] = usb6_mag1[ii]+usb6_mag2[ii]+usb6_mag3[ii]+usb6_mag4[ii]+usb6_mag5[ii]+usb6_mag6[ii]+usb6_mag7[ii]+usb6_mag8[ii]+usb6_mag9[ii]+usb6_mag10[ii]
            lsb6_mag[ii] = lsb6_mag1[ii]+lsb6_mag2[ii]+lsb6_mag3[ii]+lsb6_mag5[ii]+lsb6_mag5[ii]+lsb6_mag6[ii]+lsb6_mag7[ii]+lsb6_mag8[ii]+lsb6_mag9[ii]+lsb6_mag10[ii]
            usb7_mag[ii] = usb7_mag1[ii]+usb7_mag2[ii]+usb7_mag3[ii]+usb7_mag4[ii]+usb7_mag5[ii]+usb7_mag6[ii]+usb7_mag7[ii]+usb7_mag8[ii]+usb7_mag9[ii]+usb7_mag10[ii]
            lsb7_mag[ii] = lsb7_mag1[ii]+lsb7_mag2[ii]+lsb7_mag3[ii]+lsb7_mag5[ii]+lsb7_mag5[ii]+lsb7_mag6[ii]+lsb7_mag7[ii]+lsb7_mag8[ii]+lsb7_mag9[ii]+lsb7_mag10[ii]
            ii=ii+1


       print ('usb0_mag_sum')
       print usb0_mag[166]
       print ('lsb0_mag_sum')
       print lsb0_mag[166]

       print ('usb1_mag_sum')
       print usb1_mag[166]
       print ('lsb1_mag_sum')
       print lsb1_mag[166]

       print ('usb2_mag_sum')
       print usb2_mag[166]
       print ('lsb2_mag_sum')
       print lsb2_mag[166]

       print ('usb3_mag_sum')
       print usb3_mag[166]
       print ('lsb3_mag_sum')
       print lsb3_mag[166]

       print ('usb4_mag_sum')
       print usb4_mag[166]
       print ('lsb4_mag_sum')
       print lsb4_mag[166]

       print ('usb5_mag_sum')
       print usb5_mag[166]
       print ('lsb5_mag_sum')
       print lsb5_mag[166]

       print ('usb6_mag_sum')
       print usb6_mag[166]
       print ('lsb6_mag_sum')
       print lsb6_mag[166]

       print ('usb7_mag_sum')
       print usb7_mag[166]
       print ('lsb7_mag_sum')
       print lsb7_mag[166]

       if(usb0_mag[166]<(lsb0_mag[166]*10)):
            ##flyp dem bites on A0!!!
            print 'Channel 0 not aligned\n'
            if(usb0_mag[166]<(lsb0_mag[166]*3)):
                print 'setting threshold lower \n'
                fpga.write_int('rxslide',1)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',1)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',1)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',1)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',1)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',1)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',1)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',1)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)

       if(usb1_mag[166]<(lsb1_mag[166]*10)):
            ##flyp dem bites on A1!!!
            print 'Channel 1 not aligned\n'
            if(usb1_mag[166]<(lsb1_mag[166]*3)):
                print 'setting threshold lower \n'
                fpga.write_int('rxslide',2)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',2)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',2)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',2)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',2)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',2)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',2)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',2)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)

       if(usb2_mag[166]<(lsb2_mag[166]*10)):
            ##flyp dem bites on A2!!!
            print 'Channel 2 not aligned\n'
            if(usb2_mag[166]<(lsb2_mag[166]*3)):
                print 'setting threshold lower \n'
                fpga.write_int('rxslide',4)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',4)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',4)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',4)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',4)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',4)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',4)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',4)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)

       if(usb3_mag[166]<(lsb3_mag[166]*10)):
            ##flyp dem bites on A3!!!
            print 'Channel 3 not aligned\n'
            if(usb3_mag[166]<(lsb3_mag[166]*3)):
                print 'setting threshold lower \n'
                fpga.write_int('rxslide',8)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',8)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',8)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',8)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',8)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',8)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',8)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',8)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)

       if(usb4_mag[166]<(lsb4_mag[166]*10)):
            ##flyp dem bites on A4!!!
            print 'Channel 4 not aligned\n'
            if(usb4_mag[166]<(lsb4_mag[166]*3)):
                print 'setting threshold lower \n'
                fpga.write_int('rxslide',16)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',16)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',16)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',16)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',16)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',16)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',16)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',16)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)

       if(usb5_mag[166]<(lsb5_mag[166]*10)):
            ##flyp dem bites on A5!!!
            print 'Channel 5 not aligned\n'
            if(usb5_mag[166]<(lsb5_mag[166]*3)):
                print 'setting threshold lower \n'
                fpga.write_int('rxslide',32)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',32)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',32)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',32)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',32)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',32)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',32)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',32)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)

       if(usb6_mag[166]<(lsb6_mag[166]*10)):
            ##flyp dem bites on A6!!!
            print 'Channel 6 not aligned\n'
            if(usb6_mag[166]<(lsb6_mag[166]*3)):
                print 'setting threshold lower \n'
                fpga.write_int('rxslide',64)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',64)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',64)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',64)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',64)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',64)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',64)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',64)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)

       if(usb7_mag[166]<(lsb7_mag[166]*10)):
            ##flyp dem bites on A7!!!
            print 'Channel 7 not aligned\n'
            if(usb7_mag[166]<(lsb7_mag[166]*3)):
                print 'setting threshold lower \n'
                fpga.write_int('rxslide',164)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',164)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',164)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',164)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',164)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',164)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',164)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)
                time.sleep(0.1)
                fpga.write_int('rxslide',164)
                time.sleep(0.1)
                fpga.write_int('rxslide',0)

       print 'done checking byte alignment \n'
    #
    if (opts.swap_bytes_ch != -1):
       print 'Swapping bytes on specified channel/s'
      
       #while(hex(fpga.read_int('msb2_loc')) != 0x80):
       #   print 'in while'
       #   swap_byte(fpga,opts.swap_bytes_ch)      
       #print 'done'
 
    print 'Writing fft_shift ..... \n'
	fpga.write_int('scale_sch_we_reg',1)
    fpga.write_int('scale_sch_reg',opts.fft_shift)    #Don't know where this number comes from but its like this in the tut3 example as well 
    fpga.write_int('scale_sch_we_reg',0)
	print 'done \n'
    
    print 'Writing quantization gain .... \n'
    fpga.write_int('quant_gain',0x0000000a)
    print 'done \n'

##########################
#######################
#########luke

    if (opts.pack):
       #resets the GbE cores
       print 'Resetting the GbE cores on: %s' %(roach) 
       fpga.write_int('part2_gbe_rst_core',0)
       time.sleep(1)
       fpga.write_int('part2_gbe_rst_core',1)
       time.sleep(1)
       fpga.write_int('part2_gbe_rst_core',0)
       time.sleep(1)
       print 'done. \n'
       
      # Configure 10 GbE cores
       print 'Configuring 10 GbE interfaces on: %s.\n'%(roach) 
       fpga.config_10gbe_core('part2_gbe0', mac_base0, source_ip0, source_port, hpc_macs_list)
       fpga.config_10gbe_core('part2_gbe1', mac_base1, source_ip1, source_port, hpc_macs_list)
       fpga.config_10gbe_core('part2_gbe2', mac_base2, source_ip2, source_port, hpc_macs_list)
       fpga.config_10gbe_core('part2_gbe3', mac_base3, source_ip3, source_port, hpc_macs_list)
       print 'done. \n'
       print '------------------------'

       #set f_id
       print 'Write f engine fid... '  
       fpga.write_int('part2_f_id', opts.fid)
       time.sleep(0.5)
       print 'done. \n'
       
       #set x_id's
       print 'Write x engine id... '
       for i in range(20):
          fpga.write_int('part2_x_id'+str(i),i)
       time.sleep(0.5)
       print 'done. \n'
      
       #set destination port 
       print 'Writing destination port number: %d' %opts.dport
       fpga.write_int('part2_x_port', opts.dport)
       time.sleep(0.5)
       print 'done \n'
      
    #Set the destination IP addresses 
    if (opts.xid != -1):
       for i in range(20): 
          fpga.write_int('part2_x_ip'+str(i), blackhole)

       # The following 11 lines of code can be upgraded to case statements
       if  (opts.dcomp=='paf0' ):
          print 'Send subbands %d to paf0 and subsequent 4 xids to flag3' %opts.xid    
          fpga.write_int('part2_x_ip'+str(opts.xid)  ,paf0   )
          fpga.write_int('part2_x_ip'+str(opts.xid+1),flag3_0)
          fpga.write_int('part2_x_ip'+str(opts.xid+2),flag3_1)
          fpga.write_int('part2_x_ip'+str(opts.xid+3),flag3_2)
          fpga.write_int('part2_x_ip'+str(opts.xid+4),flag3_3)
       elif(opts.dcomp=='south'):
          print 'Send subbands %d to south' %opts.xid    
          fpga.write_int('part2_x_ip'+str(opts.xid),south)
       elif(opts.dcomp=='west' ):
          print 'Send subbands %d to west' %opts.xid    
          fpga.write_int('part2_x_ip'+str(opts.xid),west )
       elif(opts.dcomp=='tofu' ):
          print 'Send subbands %d to tofu' %opts.xid    
          fpga.write_int('part2_x_ip'+str(opts.xid),tofu )
       else :
          print("Unknown destination computer therefore sending packets to the BlAcKhOlE\n");
          fpga.write_int('part2_x_ip'+str(opts.xid),blackhole)

       print '\n done'


    if (opts.arm):
       print 'Sending sync pulse \n'
       fpga.write_int('ARM',1)
       fpga.write_int('ARM',0)
       print 'done ... \n'

    #if (opts.sbs_data):
    #   print 'Enabling output snapshots blocks \n'
    #   for k in range(4):
    #      fpga.write_int('lsb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
    #      fpga.write_int('lsb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,0)
    #      fpga.write_int('usb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
    #      fpga.write_int('usb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
# 
#       for k in range(4):
#          fpga.write_int('lsb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,1) 
#          fpga.write_int('lsb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,1)
#          fpga.write_int('usb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,1) 
#          fpga.write_int('usb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,1) ##

       #for k in range(4):
       #   fpga.write_int('lsb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
       #   fpga.write_int('lsb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,0)
       #   fpga.write_int('usb_re_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
       #   fpga.write_int('usb_im_'+str(k*2)+str(k*2+1)+'_ctrl',0,0) 
       
       #time.sleep(0.5)
       #get_data_sbs(0)
       #get_data_sbs(2)
       #get_data_sbs(4)
       #get_data_sbs(6)
       #print 'done \n'
    #luke added

#Luke commented out
    if (opts.time_data):
       print 'Reseting time domain brams'
       fpga.write_int('bram_we' ,0)
       fpga.write_int('bram_rst',1)
       fpga.write_int('bram_rst',0)
       fpga.write_int('bram_we' ,1)
       time.sleep(2)
       for i in range(8):
         get_data_iq(i)
       print 'done \n'


    if (opts.balance):
        #######################start balancing
        #atten_lvl = 9

        ##################
        blade_number=0
        ##################
        
        chan_num=0
        #set_attens1(9,blade_number)

        #rms_step_0 = get_i_q_dat()
        
        #print rms_step_0
        #print ('rms step 0 above \n')

        #if (rms_step_0<18 and rms_step_0>14):
        #    print('blade '+str(blade_number)+' channel '+str(chan_num))
        #    print('rms values good at start, atten = 9\n')        

        #if (rms_step_0<16):

        rms_arr_0=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        rms_arr_1=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        rms_arr_2=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        rms_arr_3=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        rms_arr_4=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        rms_arr_5=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        rms_arr_6=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        rms_arr_7=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        
        atten_lvl=0
        while (atten_lvl<16):
            #set_attens1(atten_lvl, blade_number)
            #nb.connect()
            nb.set_atten(0,atten_lvl)
            time.sleep(.2)
            rms_arr_0[atten_lvl]=16-abs(get_i_q_dat(0))
            #rms_arr_1[atten_lvl]=16-abs(get_i_q_dat())
            #rms_arr_2[atten_lvl]=16-abs(get_i_q_dat())
            #rms_arr_3[atten_lvl]=16-abs(get_i_q_dat())
            #rms_arr_4[atten_lvl]=16-abs(get_i_q_dat())
            #rms_arr_5[atten_lvl]=16-abs(get_i_q_dat())
            #rms_arr_6[atten_lvl]=16-abs(get_i_q_dat())
            #rms_arr_7[atten_lvl]=16-abs(get_i_q_dat())
            atten_lvl = atten_lvl+1

        #count1=0

        #while(count1<8):
        set_1atten(np.argmin(rms_arr_0),0,blade_number)
        print np.argmin(rms_arr_0)
        print('\n index of 0\n')
        print rms_arr_0
        
            
        

except KeyboardInterrupt:
  exit_clean()
except:
  exit_fail()

exit_clean()
