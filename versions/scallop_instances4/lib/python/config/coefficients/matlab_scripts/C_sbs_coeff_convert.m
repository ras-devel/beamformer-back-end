clc;
clear; 

plotData = 0;
saveData = 1;

fs = 155.52;
bw = fs/2;
chan = 256;
freq_res = bw/chan;
f_res = bw/chan:bw/chan:bw;
lsbre0_256 = zeros(256,1); 
lsbim0_256 = zeros(256,1);
usbre0_256 = zeros(256,1);
usbim0_256 = zeros(256,1);

lsbre1_256 = zeros(256,1); 
lsbim1_256 = zeros(256,1);
usbre1_256 = zeros(256,1);
usbim1_256 = zeros(256,1);

lsbre2_256 = zeros(256,1); 
lsbim2_256 = zeros(256,1);
usbre2_256 = zeros(256,1);
usbim2_256 = zeros(256,1);

lsbre3_256 = zeros(256,1); 
lsbim3_256 = zeros(256,1);
usbre3_256 = zeros(256,1);
usbim3_256 = zeros(256,1);

lsbre4_256 = zeros(256,1); 
lsbim4_256 = zeros(256,1);
usbre4_256 = zeros(256,1);
usbim4_256 = zeros(256,1);

lsbre5_256 = zeros(256,1); 
lsbim5_256 = zeros(256,1);
usbre5_256 = zeros(256,1);
usbim5_256 = zeros(256,1);

lsbre6_256 = zeros(256,1); 
lsbim6_256 = zeros(256,1);
usbre6_256 = zeros(256,1);
usbim6_256 = zeros(256,1);

lsbre7_256 = zeros(256,1); 
lsbim7_256 = zeros(256,1);
usbre7_256 = zeros(256,1);
usbim7_256 = zeros(256,1);


%j_sbs0 = importdata('A_SBS_0_512_point_Interpolated_Sideband_Coef.csv');
sbs0 = importdata('C_SBS_0_Sideband_Coef.csv');
sbs1 = importdata('C_SBS_1_Sideband_Coef.csv');
sbs2 = importdata('C_SBS_2_Sideband_Coef.csv');
sbs3 = importdata('C_SBS_3_Sideband_Coef.csv');
sbs4 = importdata('C_SBS_4_Sideband_Coef.csv');
sbs5 = importdata('C_SBS_5_Sideband_Coef.csv');
sbs6 = importdata('C_SBS_6_Sideband_Coef.csv');
sbs7 = importdata('C_SBS_7_Sideband_Coef.csv');

%import the data sets for LO 1.75 GHz

freq = sbs0.data(:,1);

lsbre0_78 = sbs0.data(:,22);
lsbim0_78 = sbs0.data(:,23);
usbre0_78 = sbs0.data(:,24);
usbim0_78 = sbs0.data(:,25);

%jlsbre0_256 = j_sbs0.data(:,10);
%jlsbim0_256 = j_sbs0.data(:,11);
%jusbre0_256 = j_sbs0.data(:,12);
%jusbim0_256 = j_sbs0.data(:,13);


lsbre1_78 = sbs1.data(:,22);
lsbim1_78 = sbs1.data(:,23);
usbre1_78 = sbs1.data(:,24);
usbim1_78 = sbs1.data(:,25);
                          
lsbre2_78 = sbs2.data(:,22);
lsbim2_78 = sbs2.data(:,23);
usbre2_78 = sbs2.data(:,24);
usbim2_78 = sbs2.data(:,25);
                          
lsbre3_78 = sbs3.data(:,22);
lsbim3_78 = sbs3.data(:,23);
usbre3_78 = sbs3.data(:,24);
usbim3_78 = sbs3.data(:,25);
                        
lsbre4_78 = sbs4.data(:,22);
lsbim4_78 = sbs4.data(:,23);
usbre4_78 = sbs4.data(:,24);
usbim4_78 = sbs4.data(:,25);
                          
lsbre5_78 = sbs5.data(:,22);
lsbim5_78 = sbs5.data(:,23);
usbre5_78 = sbs5.data(:,24);
usbim5_78 = sbs5.data(:,25);
                        
lsbre6_78 = sbs6.data(:,22);
lsbim6_78 = sbs6.data(:,23);
usbre6_78 = sbs6.data(:,24);
usbim6_78 = sbs6.data(:,25);
                          
lsbre7_78 = sbs7.data(:,22);
lsbim7_78 = sbs7.data(:,23);
usbre7_78 = sbs7.data(:,24);
usbim7_78 = sbs7.data(:,25);

%y=mx+c %% m = y2-y1/x2-x1 %% c = y - mx
m_lsbre0_78 = zeros(77,1);
m_lsbim0_78 = zeros(77,1);
m_usbre0_78 = zeros(77,1);
m_usbim0_78 = zeros(77,1);

c_lsbre0_78 = zeros(77,1);
c_lsbim0_78 = zeros(77,1);
c_usbre0_78 = zeros(77,1);
c_usbim0_78 = zeros(77,1);

m_lsbre1_78 = zeros(77,1);
m_lsbim1_78 = zeros(77,1);
m_usbre1_78 = zeros(77,1);
m_usbim1_78 = zeros(77,1);

c_lsbre1_78 = zeros(77,1);
c_lsbim1_78 = zeros(77,1);
c_usbre1_78 = zeros(77,1);
c_usbim1_78 = zeros(77,1);

m_lsbre2_78 = zeros(77,1);
m_lsbim2_78 = zeros(77,1);
m_usbre2_78 = zeros(77,1);
m_usbim2_78 = zeros(77,1);

c_lsbre2_78 = zeros(77,1);
c_lsbim2_78 = zeros(77,1);
c_usbre2_78 = zeros(77,1);
c_usbim2_78 = zeros(77,1);

m_lsbre3_78 = zeros(77,1);
m_lsbim3_78 = zeros(77,1);
m_usbre3_78 = zeros(77,1);
m_usbim3_78 = zeros(77,1);

c_lsbre3_78 = zeros(77,1);
c_lsbim3_78 = zeros(77,1);
c_usbre3_78 = zeros(77,1);
c_usbim3_78 = zeros(77,1);

m_lsbre4_78 = zeros(77,1);
m_lsbim4_78 = zeros(77,1);
m_usbre4_78 = zeros(77,1);
m_usbim4_78 = zeros(77,1);

c_lsbre4_78 = zeros(77,1);
c_lsbim4_78 = zeros(77,1);
c_usbre4_78 = zeros(77,1);
c_usbim4_78 = zeros(77,1);

m_lsbre5_78 = zeros(77,1);
m_lsbim5_78 = zeros(77,1);
m_usbre5_78 = zeros(77,1);
m_usbim5_78 = zeros(77,1);

c_lsbre5_78 = zeros(77,1);
c_lsbim5_78 = zeros(77,1);
c_usbre5_78 = zeros(77,1);
c_usbim5_78 = zeros(77,1);

m_lsbre6_78 = zeros(77,1);
m_lsbim6_78 = zeros(77,1);
m_usbre6_78 = zeros(77,1);
m_usbim6_78 = zeros(77,1);

c_lsbre6_78 = zeros(77,1);
c_lsbim6_78 = zeros(77,1);
c_usbre6_78 = zeros(77,1);
c_usbim6_78 = zeros(77,1);

m_lsbre7_78 = zeros(77,1);
m_lsbim7_78 = zeros(77,1);
m_usbre7_78 = zeros(77,1);
m_usbim7_78 = zeros(77,1);

c_lsbre7_78 = zeros(77,1);
c_lsbim7_78 = zeros(77,1);
c_usbre7_78 = zeros(77,1);
c_usbim7_78 = zeros(77,1);



for i=1:77
  m_lsbre0_78(i,1) = (lsbre0_78(i+1,1) - lsbre0_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_lsbim0_78(i,1) = (lsbim0_78(i+1,1) - lsbim0_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbre0_78(i,1) = (usbre0_78(i+1,1) - usbre0_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbim0_78(i,1) = (usbim0_78(i+1,1) - usbim0_78(i,1))/(freq(i+1,1) - freq(i,1));
  
  c_lsbre0_78(i,1) = ((lsbre0_78(i,1) + lsbre0_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbre0_78(i,1);
  c_lsbim0_78(i,1) = ((lsbim0_78(i,1) + lsbim0_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbim0_78(i,1);
  c_usbre0_78(i,1) = ((usbre0_78(i,1) + usbre0_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbre0_78(i,1);
  c_usbim0_78(i,1) = ((usbim0_78(i,1) + usbim0_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbim0_78(i,1); 

  m_lsbre1_78(i,1) = (lsbre1_78(i+1,1) - lsbre1_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_lsbim1_78(i,1) = (lsbim1_78(i+1,1) - lsbim1_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbre1_78(i,1) = (usbre1_78(i+1,1) - usbre1_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbim1_78(i,1) = (usbim1_78(i+1,1) - usbim1_78(i,1))/(freq(i+1,1) - freq(i,1));
  
  c_lsbre1_78(i,1) = ((lsbre1_78(i,1) + lsbre1_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbre1_78(i,1);
  c_lsbim1_78(i,1) = ((lsbim1_78(i,1) + lsbim1_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbim1_78(i,1);
  c_usbre1_78(i,1) = ((usbre1_78(i,1) + usbre1_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbre1_78(i,1);
  c_usbim1_78(i,1) = ((usbim1_78(i,1) + usbim1_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbim1_78(i,1); 

  m_lsbre2_78(i,1) = (lsbre2_78(i+1,1) - lsbre2_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_lsbim2_78(i,1) = (lsbim2_78(i+1,1) - lsbim2_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbre2_78(i,1) = (usbre2_78(i+1,1) - usbre2_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbim2_78(i,1) = (usbim2_78(i+1,1) - usbim2_78(i,1))/(freq(i+1,1) - freq(i,1));
  
  c_lsbre2_78(i,1) = ((lsbre2_78(i,1) + lsbre2_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbre2_78(i,1);
  c_lsbim2_78(i,1) = ((lsbim2_78(i,1) + lsbim2_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbim2_78(i,1);
  c_usbre2_78(i,1) = ((usbre2_78(i,1) + usbre2_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbre2_78(i,1);
  c_usbim2_78(i,1) = ((usbim2_78(i,1) + usbim2_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbim2_78(i,1); 

  m_lsbre3_78(i,1) = (lsbre3_78(i+1,1) - lsbre3_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_lsbim3_78(i,1) = (lsbim3_78(i+1,1) - lsbim3_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbre3_78(i,1) = (usbre3_78(i+1,1) - usbre3_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbim3_78(i,1) = (usbim3_78(i+1,1) - usbim3_78(i,1))/(freq(i+1,1) - freq(i,1));
  
  c_lsbre3_78(i,1) = ((lsbre3_78(i,1) + lsbre3_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbre3_78(i,1);
  c_lsbim3_78(i,1) = ((lsbim3_78(i,1) + lsbim3_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbim3_78(i,1);
  c_usbre3_78(i,1) = ((usbre3_78(i,1) + usbre3_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbre3_78(i,1);
  c_usbim3_78(i,1) = ((usbim3_78(i,1) + usbim3_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbim3_78(i,1); 

  m_lsbre4_78(i,1) = (lsbre4_78(i+1,1) - lsbre4_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_lsbim4_78(i,1) = (lsbim4_78(i+1,1) - lsbim4_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbre4_78(i,1) = (usbre4_78(i+1,1) - usbre4_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbim4_78(i,1) = (usbim4_78(i+1,1) - usbim4_78(i,1))/(freq(i+1,1) - freq(i,1));
  
  c_lsbre4_78(i,1) = ((lsbre4_78(i,1) + lsbre4_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbre4_78(i,1);
  c_lsbim4_78(i,1) = ((lsbim4_78(i,1) + lsbim4_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbim4_78(i,1);
  c_usbre4_78(i,1) = ((usbre4_78(i,1) + usbre4_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbre4_78(i,1);
  c_usbim4_78(i,1) = ((usbim4_78(i,1) + usbim4_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbim4_78(i,1); 

  m_lsbre5_78(i,1) = (lsbre5_78(i+1,1) - lsbre5_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_lsbim5_78(i,1) = (lsbim5_78(i+1,1) - lsbim5_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbre5_78(i,1) = (usbre5_78(i+1,1) - usbre5_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbim5_78(i,1) = (usbim5_78(i+1,1) - usbim5_78(i,1))/(freq(i+1,1) - freq(i,1));
  
  c_lsbre5_78(i,1) = ((lsbre5_78(i,1) + lsbre5_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbre5_78(i,1);
  c_lsbim5_78(i,1) = ((lsbim5_78(i,1) + lsbim5_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbim5_78(i,1);
  c_usbre5_78(i,1) = ((usbre5_78(i,1) + usbre5_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbre5_78(i,1);
  c_usbim5_78(i,1) = ((usbim5_78(i,1) + usbim5_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbim5_78(i,1); 

  m_lsbre6_78(i,1) = (lsbre6_78(i+1,1) - lsbre6_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_lsbim6_78(i,1) = (lsbim6_78(i+1,1) - lsbim6_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbre6_78(i,1) = (usbre6_78(i+1,1) - usbre6_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbim6_78(i,1) = (usbim6_78(i+1,1) - usbim6_78(i,1))/(freq(i+1,1) - freq(i,1));
  
  c_lsbre6_78(i,1) = ((lsbre6_78(i,1) + lsbre6_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbre6_78(i,1);
  c_lsbim6_78(i,1) = ((lsbim6_78(i,1) + lsbim6_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbim6_78(i,1);
  c_usbre6_78(i,1) = ((usbre6_78(i,1) + usbre6_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbre6_78(i,1);
  c_usbim6_78(i,1) = ((usbim6_78(i,1) + usbim6_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbim6_78(i,1); 

  m_lsbre7_78(i,1) = (lsbre7_78(i+1,1) - lsbre7_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_lsbim7_78(i,1) = (lsbim7_78(i+1,1) - lsbim7_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbre7_78(i,1) = (usbre7_78(i+1,1) - usbre7_78(i,1))/(freq(i+1,1) - freq(i,1));
  m_usbim7_78(i,1) = (usbim7_78(i+1,1) - usbim7_78(i,1))/(freq(i+1,1) - freq(i,1));
  
  c_lsbre7_78(i,1) = ((lsbre7_78(i,1) + lsbre7_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbre7_78(i,1);
  c_lsbim7_78(i,1) = ((lsbim7_78(i,1) + lsbim7_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_lsbim7_78(i,1);
  c_usbre7_78(i,1) = ((usbre7_78(i,1) + usbre7_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbre7_78(i,1);
  c_usbim7_78(i,1) = ((usbim7_78(i,1) + usbim7_78(i+1,1))/2) - ((freq(i+1,1) + freq(i,1))/2)*m_usbim7_78(i,1); 

end

m_lsbre0 = m_lsbre0_78(1,1);
c_lsbre0 = c_lsbre0_78(1,1);
m_lsbim0 = m_lsbim0_78(1,1);
c_lsbim0 = c_lsbim0_78(1,1);
m_usbre0 = m_usbre0_78(1,1);
c_usbre0 = c_usbre0_78(1,1);
m_usbim0 = m_usbim0_78(1,1);
c_usbim0 = c_usbim0_78(1,1);

m_lsbre1 = m_lsbre1_78(1,1);
c_lsbre1 = c_lsbre1_78(1,1);
m_lsbim1 = m_lsbim1_78(1,1);
c_lsbim1 = c_lsbim1_78(1,1);
m_usbre1 = m_usbre1_78(1,1);
c_usbre1 = c_usbre1_78(1,1);
m_usbim1 = m_usbim1_78(1,1);
c_usbim1 = c_usbim1_78(1,1);

m_lsbre2 = m_lsbre2_78(1,1);
c_lsbre2 = c_lsbre2_78(1,1);
m_lsbim2 = m_lsbim2_78(1,1);
c_lsbim2 = c_lsbim2_78(1,1);
m_usbre2 = m_usbre2_78(1,1);
c_usbre2 = c_usbre2_78(1,1);
m_usbim2 = m_usbim2_78(1,1);
c_usbim2 = c_usbim2_78(1,1);

m_lsbre3 = m_lsbre3_78(1,1);
c_lsbre3 = c_lsbre3_78(1,1);
m_lsbim3 = m_lsbim3_78(1,1);
c_lsbim3 = c_lsbim3_78(1,1);
m_usbre3 = m_usbre3_78(1,1);
c_usbre3 = c_usbre3_78(1,1);
m_usbim3 = m_usbim3_78(1,1);
c_usbim3 = c_usbim3_78(1,1);

m_lsbre4 = m_lsbre4_78(1,1);
c_lsbre4 = c_lsbre4_78(1,1);
m_lsbim4 = m_lsbim4_78(1,1);
c_lsbim4 = c_lsbim4_78(1,1);
m_usbre4 = m_usbre4_78(1,1);
c_usbre4 = c_usbre4_78(1,1);
m_usbim4 = m_usbim4_78(1,1);
c_usbim4 = c_usbim4_78(1,1);

m_lsbre5 = m_lsbre5_78(1,1);
c_lsbre5 = c_lsbre5_78(1,1);
m_lsbim5 = m_lsbim5_78(1,1);
c_lsbim5 = c_lsbim5_78(1,1);
m_usbre5 = m_usbre5_78(1,1);
c_usbre5 = c_usbre5_78(1,1);
m_usbim5 = m_usbim5_78(1,1);
c_usbim5 = c_usbim5_78(1,1);

m_lsbre6 = m_lsbre6_78(1,1);
c_lsbre6 = c_lsbre6_78(1,1);
m_lsbim6 = m_lsbim6_78(1,1);
c_lsbim6 = c_lsbim6_78(1,1);
m_usbre6 = m_usbre6_78(1,1);
c_usbre6 = c_usbre6_78(1,1);
m_usbim6 = m_usbim6_78(1,1);
c_usbim6 = c_usbim6_78(1,1);

m_lsbre7 = m_lsbre7_78(1,1);
c_lsbre7 = c_lsbre7_78(1,1);
m_lsbim7 = m_lsbim7_78(1,1);
c_lsbim7 = c_lsbim7_78(1,1);
m_usbre7 = m_usbre7_78(1,1);
c_usbre7 = c_usbre7_78(1,1);
m_usbim7 = m_usbim7_78(1,1);
c_usbim7 = c_usbim7_78(1,1);



%%%%%%%%%%%%%%%%%%

i = 1;
j = 1;


for i = 1:256
    if(j <= 76 && (freq(j+1,1) < f_res(1,i)))
        j = j + 1;
    end
            
    m_lsbre0 = m_lsbre0_78(j,1);
    c_lsbre0 = c_lsbre0_78(j,1);
    m_lsbim0 = m_lsbim0_78(j,1);
    c_lsbim0 = c_lsbim0_78(j,1);
    m_usbre0 = m_usbre0_78(j,1);
    c_usbre0 = c_usbre0_78(j,1);
    m_usbim0 = m_usbim0_78(j,1);
    c_usbim0 = c_usbim0_78(j,1);
          
    m_lsbre1 = m_lsbre1_78(j,1);
    c_lsbre1 = c_lsbre1_78(j,1);
    m_lsbim1 = m_lsbim1_78(j,1);
    c_lsbim1 = c_lsbim1_78(j,1);
    m_usbre1 = m_usbre1_78(j,1);
    c_usbre1 = c_usbre1_78(j,1);
    m_usbim1 = m_usbim1_78(j,1);
    c_usbim1 = c_usbim1_78(j,1);

    m_lsbre2 = m_lsbre2_78(j,1);
    c_lsbre2 = c_lsbre2_78(j,1);
    m_lsbim2 = m_lsbim2_78(j,1);
    c_lsbim2 = c_lsbim2_78(j,1);
    m_usbre2 = m_usbre2_78(j,1);
    c_usbre2 = c_usbre2_78(j,1);
    m_usbim2 = m_usbim2_78(j,1);
    c_usbim2 = c_usbim2_78(j,1);

    m_lsbre3 = m_lsbre3_78(j,1);
    c_lsbre3 = c_lsbre3_78(j,1);
    m_lsbim3 = m_lsbim3_78(j,1);
    c_lsbim3 = c_lsbim3_78(j,1);
    m_usbre3 = m_usbre3_78(j,1);
    c_usbre3 = c_usbre3_78(j,1);
    m_usbim3 = m_usbim3_78(j,1);
    c_usbim3 = c_usbim3_78(j,1);
    
    m_lsbre4 = m_lsbre4_78(j,1);
    c_lsbre4 = c_lsbre4_78(j,1);
    m_lsbim4 = m_lsbim4_78(j,1);
    c_lsbim4 = c_lsbim4_78(j,1);
    m_usbre4 = m_usbre4_78(j,1);
    c_usbre4 = c_usbre4_78(j,1);
    m_usbim4 = m_usbim4_78(j,1);
    c_usbim4 = c_usbim4_78(j,1);

    m_lsbre5 = m_lsbre5_78(j,1);
    c_lsbre5 = c_lsbre5_78(j,1);
    m_lsbim5 = m_lsbim5_78(j,1);
    c_lsbim5 = c_lsbim5_78(j,1);
    m_usbre5 = m_usbre5_78(j,1);
    c_usbre5 = c_usbre5_78(j,1);
    m_usbim5 = m_usbim5_78(j,1);
    c_usbim5 = c_usbim5_78(j,1);

    m_lsbre6 = m_lsbre6_78(j,1);
    c_lsbre6 = c_lsbre6_78(j,1);
    m_lsbim6 = m_lsbim6_78(j,1);
    c_lsbim6 = c_lsbim6_78(j,1);
    m_usbre6 = m_usbre6_78(j,1);
    c_usbre6 = c_usbre6_78(j,1);
    m_usbim6 = m_usbim6_78(j,1);
    c_usbim6 = c_usbim6_78(j,1);

    m_lsbre7 = m_lsbre7_78(j,1);
    c_lsbre7 = c_lsbre7_78(j,1);
    m_lsbim7 = m_lsbim7_78(j,1);
    c_lsbim7 = c_lsbim7_78(j,1);
    m_usbre7 = m_usbre7_78(j,1);
    c_usbre7 = c_usbre7_78(j,1);
    m_usbim7 = m_usbim7_78(j,1);
    c_usbim7 = c_usbim7_78(j,1);
 

     
    lsbre0_256(i,1) = m_lsbre0*f_res(1,i) + c_lsbre0; 
    lsbim0_256(i,1) = m_lsbim0*f_res(1,i) + c_lsbim0;
    usbre0_256(i,1) = m_usbre0*f_res(1,i) + c_usbre0; 
    usbim0_256(i,1) = m_usbim0*f_res(1,i) + c_usbim0;
    
    lsbre1_256(i,1) = m_lsbre1*f_res(1,i) + c_lsbre1; 
    lsbim1_256(i,1) = m_lsbim1*f_res(1,i) + c_lsbim1;
    usbre1_256(i,1) = m_usbre1*f_res(1,i) + c_usbre1; 
    usbim1_256(i,1) = m_usbim1*f_res(1,i) + c_usbim1;
    
    lsbre2_256(i,1) = m_lsbre2*f_res(1,i) + c_lsbre2; 
    lsbim2_256(i,1) = m_lsbim2*f_res(1,i) + c_lsbim2;
    usbre2_256(i,1) = m_usbre2*f_res(1,i) + c_usbre2; 
    usbim2_256(i,1) = m_usbim2*f_res(1,i) + c_usbim2;
  
    lsbre3_256(i,1) = m_lsbre3*f_res(1,i) + c_lsbre3; 
    lsbim3_256(i,1) = m_lsbim3*f_res(1,i) + c_lsbim3;
    usbre3_256(i,1) = m_usbre3*f_res(1,i) + c_usbre3; 
    usbim3_256(i,1) = m_usbim3*f_res(1,i) + c_usbim3;
   
    lsbre4_256(i,1) = m_lsbre4*f_res(1,i) + c_lsbre4; 
    lsbim4_256(i,1) = m_lsbim4*f_res(1,i) + c_lsbim4;
    usbre4_256(i,1) = m_usbre4*f_res(1,i) + c_usbre4; 
    usbim4_256(i,1) = m_usbim4*f_res(1,i) + c_usbim4;
  
    lsbre5_256(i,1) = m_lsbre5*f_res(1,i) + c_lsbre5; 
    lsbim5_256(i,1) = m_lsbim5*f_res(1,i) + c_lsbim5;
    usbre5_256(i,1) = m_usbre5*f_res(1,i) + c_usbre5; 
    usbim5_256(i,1) = m_usbim5*f_res(1,i) + c_usbim5;
 
    lsbre6_256(i,1) = m_lsbre6*f_res(1,i) + c_lsbre6; 
    lsbim6_256(i,1) = m_lsbim6*f_res(1,i) + c_lsbim6;
    usbre6_256(i,1) = m_usbre6*f_res(1,i) + c_usbre6; 
    usbim6_256(i,1) = m_usbim6*f_res(1,i) + c_usbim6;
  
    lsbre7_256(i,1) = m_lsbre7*f_res(1,i) + c_lsbre7; 
    lsbim7_256(i,1) = m_lsbim7*f_res(1,i) + c_lsbim7;
    usbre7_256(i,1) = m_usbre7*f_res(1,i) + c_usbre7; 
    usbim7_256(i,1) = m_usbim7*f_res(1,i) + c_usbim7;
 
    
end

if (plotData == 1)
    figure(1);
    subplot(2,2,1);
    plot(freq,lsbre0_78,'*',f_res,lsbre0_256,'o');
    grid();
    legend('Origanal','V');
    xlabel('Frequency [MHz]');
    ylabel('LSB real');
    title('Channel 0 coefficients');
    subplot(2,2,2);
    plot(freq,lsbim0_78,'*',f_res,lsbim0_256,'o');
    grid();
    legend('Origanal','V');
    xlabel('Frequency [MHz]');
    ylabel('LSB imaginary');
    title('Channel 0 coefficients');
    subplot(2,2,3);
    plot(freq,usbre0_78,'*',f_res,usbre0_256,'o');
    grid();
    legend('Origanal','V');
    xlabel('Frequency [MHz]');
    ylabel('USB real');
    title('Channel 0 coefficients');
    subplot(2,2,4);
    plot(freq,usbim0_78,'*',f_res,usbim0_256,'o');
    grid();
    legend('Origanal','V');
    xlabel('Frequency [MHz]');
    ylabel('USB imaginary');
    title('Channel 0 coefficients');

    figure(2);
    subplot(2,2,1);
    plot(freq,lsbre1_78,'*',f_res,lsbre1_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB real');
    title('Channel 1 coefficients');
    subplot(2,2,2);
    plot(freq,lsbim1_78,'*',f_res,lsbim1_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB imaginary');
    title('Channel 1 coefficients');
    subplot(2,2,3);
    plot(freq,usbre1_78,'*',f_res,usbre1_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB real');
    title('Channel 1 coefficients');
    subplot(2,2,4);
    plot(freq,usbim1_78,'*',f_res,usbim1_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB imaginary');
    title('Channel 1 coefficients');

    figure(3);
    subplot(2,2,1);
    plot(freq,lsbre2_78,'*',f_res,lsbre2_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB real');
    title('Channel 2 coefficients');
    subplot(2,2,2);
    plot(freq,lsbim2_78,'*',f_res,lsbim2_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB imaginary');
    title('Channel 2 coefficients');
    subplot(2,2,3);
    plot(freq,usbre2_78,'*',f_res,usbre2_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB real');
    title('Channel 2 coefficients');
    subplot(2,2,4);
    plot(freq,usbim2_78,'*',f_res,usbim2_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB imaginary');
    title('Channel 2 coefficients');

    figure(4);
    subplot(2,2,1);
    plot(freq,lsbre3_78,'*',f_res,lsbre3_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB real');
    title('Channel 3 coefficients');
    subplot(2,2,2);
    plot(freq,lsbim3_78,'*',f_res,lsbim3_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB imaginary');
    title('Channel 3 coefficients');
    subplot(2,2,3);
    plot(freq,usbre3_78,'*',f_res,usbre3_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB real');
    title('Channel 3 coefficients');
    subplot(2,2,4);
    plot(freq,usbim3_78,'*',f_res,usbim3_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB imaginary');
    title('Channel 3 coefficients');

    figure(5);
    subplot(2,2,1);
    plot(freq,lsbre4_78,'*',f_res,lsbre4_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB real');
    title('Channel 4 coefficients');
    subplot(2,2,2);
    plot(freq,lsbim4_78,'*',f_res,lsbim4_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB imaginary');
    title('Channel 4 coefficients');
    subplot(2,2,3);
    plot(freq,usbre4_78,'*',f_res,usbre4_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB real');
    title('Channel 4 coefficients');
    subplot(2,2,4);
    plot(freq,usbim4_78,'*',f_res,usbim4_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB imaginary');
    title('Channel 4 coefficients');






    figure(6);
    subplot(2,2,1);
    plot(freq,lsbre5_78,'*',f_res,lsbre5_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB real');
    title('Channel 5 coefficients');
    subplot(2,2,2);
    plot(freq,lsbim5_78,'*',f_res,lsbim5_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB imaginary');
    title('Channel 5 coefficients');
    subplot(2,2,3);
    plot(freq,usbre5_78,'*',f_res,usbre5_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB real');
    title('Channel 5 coefficients');
    subplot(2,2,4);
    plot(freq,usbim5_78,'*',f_res,usbim5_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB imaginary');
    title('Channel 5 coefficients');




    figure(7);
    subplot(2,2,1);
    plot(freq,lsbre6_78,'*',f_res,lsbre6_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB real');
    title('Channel 6 coefficients');
    subplot(2,2,2);
    plot(freq,lsbim6_78,'*',f_res,lsbim6_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB imaginary');
    title('Channel 6 coefficients');
    subplot(2,2,3);
    plot(freq,usbre6_78,'*',f_res,usbre6_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB real');
    title('Channel 6 coefficients');
    subplot(2,2,4);
    plot(freq,usbim6_78,'*',f_res,usbim6_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB imaginary');
    title('Channel 6 coefficients');



    figure(8);
    subplot(2,2,1);
    plot(freq,lsbre7_78,'*',f_res,lsbre7_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB real');
    title('Channel 7 coefficients');
    subplot(2,2,2);
    plot(freq,lsbim7_78,'*',f_res,lsbim7_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('LSB imaginary');
    title('Channel 7 coefficients');
    subplot(2,2,3);
    plot(freq,usbre7_78,'*',f_res,usbre7_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB real');
    title('Channel 7 coefficients');
    subplot(2,2,4);
    plot(freq,usbim7_78,'*',f_res,usbim7_256,'o');
    grid();
    xlabel('Frequency [MHz]');
    ylabel('USB imaginary');
    title('Channel 7 coefficients');
end



if (saveData == 1)
    lsbre01_data = [lsbre0_256 ; lsbre1_256];
    lsbre23_data = [lsbre2_256 ; lsbre3_256];
    lsbre45_data = [lsbre4_256 ; lsbre5_256];
    lsbre67_data = [lsbre6_256 ; lsbre7_256];
    
    lsbim01_data = [lsbim0_256 ; lsbim1_256];
    lsbim23_data = [lsbim2_256 ; lsbim3_256];
    lsbim45_data = [lsbim4_256 ; lsbim5_256];
    lsbim67_data = [lsbim6_256 ; lsbim7_256];
    
    usbre01_data = [usbre0_256 ; usbre1_256];
    usbre23_data = [usbre2_256 ; usbre3_256];
    usbre45_data = [usbre4_256 ; usbre5_256];
    usbre67_data = [usbre6_256 ; usbre7_256];
    
    usbim01_data = [usbim0_256 ; usbim1_256];
    usbim23_data = [usbim2_256 ; usbim3_256];
    usbim45_data = [usbim4_256 ; usbim5_256];
    usbim67_data = [usbim6_256 ; usbim7_256];
    
 
    
    lsbre01 = fopen('C_512_lsbre01_1750.csv','w');
    lsbim01 = fopen('C_512_lsbim01_1750.csv','w');
    usbre01 = fopen('C_512_usbre01_1750.csv','w');
    usbim01 = fopen('C_512_usbim01_1750.csv','w');

    lsbre23 = fopen('C_512_lsbre23_1750.csv','w');
    lsbim23 = fopen('C_512_lsbim23_1750.csv','w');
    usbre23 = fopen('C_512_usbre23_1750.csv','w');
    usbim23 = fopen('C_512_usbim23_1750.csv','w');   

    lsbre45 = fopen('C_512_lsbre45_1750.csv','w');
    lsbim45 = fopen('C_512_lsbim45_1750.csv','w');
    usbre45 = fopen('C_512_usbre45_1750.csv','w');
    usbim45 = fopen('C_512_usbim45_1750.csv','w');

    lsbre67 = fopen('C_512_lsbre67_1750.csv','w');
    lsbim67 = fopen('C_512_lsbim67_1750.csv','w');
    usbre67 = fopen('C_512_usbre67_1750.csv','w');
    usbim67 = fopen('C_512_usbim67_1750.csv','w');
  
 
    for i=1:512
        fprintf(lsbre01,'%d\n',lsbre01_data(i,1));
        fprintf(lsbim01,'%d\n',lsbim01_data(i,1));
        fprintf(usbre01,'%d\n',usbre01_data(i,1));
        fprintf(usbim01,'%d\n',usbim01_data(i,1));
        
        fprintf(lsbre23,'%d\n',lsbre23_data(i,1));
        fprintf(lsbim23,'%d\n',lsbim23_data(i,1));
        fprintf(usbre23,'%d\n',usbre23_data(i,1));
        fprintf(usbim23,'%d\n',usbim23_data(i,1));
        
        fprintf(lsbre45,'%d\n',lsbre45_data(i,1));
        fprintf(lsbim45,'%d\n',lsbim45_data(i,1));
        fprintf(usbre45,'%d\n',usbre45_data(i,1));
        fprintf(usbim45,'%d\n',usbim45_data(i,1)); 
        
        fprintf(lsbre67,'%d\n',lsbre67_data(i,1));
        fprintf(lsbim67,'%d\n',lsbim67_data(i,1));
        fprintf(usbre67,'%d\n',usbre67_data(i,1));
        fprintf(usbim67,'%d\n',usbim67_data(i,1));
        
    end

    fclose(lsbre01);
    fclose(lsbim01);
    fclose(usbre01);
    fclose(usbim01);
    fclose(lsbre23);
    fclose(lsbim23);
    fclose(usbre23);
    fclose(usbim23);
    fclose(lsbre45);
    fclose(lsbim45);
    fclose(usbre45);
    fclose(usbim45);
    fclose(lsbre67);
    fclose(lsbim67);
    fclose(usbre67);
    fclose(usbim67);
    
end










